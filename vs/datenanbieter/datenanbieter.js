import $ from "jquery";
import can from "can";

import store from 'vs/resources/store';

/*
	Zuständig für die Beschaffung, Verwaltung und Auslieferung der Daten
*/
export const AmoDatenanbieter = can.Control.extend("Amo.Datenanbieter", {
	defaults: {
		//serverPfad: 'http://localhost/AMO-Steal/amo/',
		//serverPfad: 'http://visu-l.drjochum.de/amovs/',
		//serverPfad: 'http://drjochum.de/amo/'
		serverPfad: 'https://www.tecnisvisionsimulator.com/',
		//serverPfad: 'http://vision.abbottmedicaloptics.com/',
		webApp: false,
		desktopApp: false,
		ausgewaehltesLandDefault: "usa"
	}
}, {
	init: function( element , options ) {
		steal.dev.log( 'Datenanbieter amoDaten' + amoData.amoStatus.version );
		var that = this; //Referenz auf Controller

		this.derBodyEl = $('body');
		//this.acceptButtonEl = $('#acceptButton');
		this.acceptBlockEl = $('#acceptBlock');
		this.countrySelectEl = $('#countrySelect');
		this.countrySelectAnzeigeEl = $('.countrySelectContainer span.amoButton');

		this.pageInit = true;
		this.appInit = false;
		this.fertigStartSeite = false;
		//this.bereitEreignis = false;

		this.imagePfad = 'amo/';
			//!steal-remove-start
				this.imagePfad = '';
			//!steal-remove-end	

		
		//Zwischenspeicher für die Config-Daten (entweder vom Server oder von appMobi-Cookie)
		this.amoConfigData = '';

		//Anpassungen für die verschiedenen App-Versionen
		if( webApp === true )
		{
//Serverpfad für die Desktop-App muss gleich sein wie bei ioS-App 
// !!!!!!!!!       ------------------         !!!!!!!!!!!!!!
			//this.options.serverPfad = 'http://localhost/amo/';
			this.options.serverPfad = derServerPfad;
			//this.options.serverPfad = 'http://drjochum.de/amo2/';
			this.options.webApp = true;

			if ( desktopApp === true )
			{
				this.options.desktopApp = true;

				this.pruefenAufDatenImCache = this.pruefenAufDatenImCacheDesktopApp;
				this.speichernAmoDatenImCache = this.speichernAmoDatenImCacheDesktopApp;
				this.pruefenVersionVomServer = this.pruefenVersionVomServerDesktop;
			}
			else
			{

				this.pruefenAufDatenImCache = this.pruefenAufDatenImCacheWebApp;
				this.speichernAmoDatenImCache = this.speichernAmoDatenImCacheWebApp;
				this.pruefenVersionVomServer = this.pruefenVersionOhneServer;
			}
 
			this.pruefenAufStatusImCache = this.pruefenAufStatusImCacheWebApp;

			this.speichernAmoStatusImCache = this.speichernAmoStatusImCacheWebApp;


			//Einmalig das Datum setzen beim Laden der WebApp
			this.auslesenDatum();

		}
		else
		{
			this.options.webApp = false;

			this.pruefenAufDatenImCache = this.pruefenAufDatenImCacheAppMobi;
			this.speichernAmoDatenImCache = this.speichernAmoDatenImCacheAppMobi;
			//this.pruefenVersionVomServer = this.pruefenVersionOhneServer;

			this.pruefenAufStatusImCache = this.pruefenAufStatusImCacheAppMobi;

			this.speichernAmoStatusImCache = this.speichernAmoStatusImCacheAppMobi;

			/*this.pruefenAufDatenImCache = this.pruefenAufDatenImCacheAppMobi;
			this.pruefenAufStatusImCache = this.pruefenAufStatusImCacheAppMobi;

			this.speichernAmoDatenImCache = this.speichernAmoDatenImCacheAppMobi;
			this.speichernAmoStatusImCache = this.speichernAmoStatusImCacheAppMobi;*/

			this.pruefenVersionVomServer = this.pruefenVersionVomServerAjax;
		}

		//Daten anlegen und verfügbar machen
		//noch statisch
		this.anlegenDaten();

		//prüfen, ob gespeicherte Daten vorliegen (geschiet über 'bereit'-Event)

		if( this.options.webApp === true )
		{
			this.amoStatus.attr("upDate", this.lesbaresDatumHeute );
		}

		//Daten für die aktuelle Sprachversion laden
		//this.festlegenLaenderVersion( 'deutschland' );
		//steal.dev.log('init usa');
		this.aktualisierenLaenderVersion( this.options.ausgewaehltesLandDefault );
	},
	ausgebenDaten: function( datenName ) {
		return this[datenName];
	},
	eingebenDaten: function( datenName, eingabeWert ){
		this[datenName] = eingabeWert;
	},
	festlegenLaenderVersion: function( dasLand ) {
		this.amoDatenAktuell['laenderVersion'] = this.laenderVersionen[dasLand];
		this.amoDatenAktuell['sprachVariante'] = this.sprachVarianten[ this.laenderVersionen[dasLand].sprache ];
	},
	aktualisierenLaenderVersion: function( dasLand ) {
		//wenn es das Land nicht mehr gibt, nimm usa
		if ( this.laenderVersionen[dasLand] !== undefined )
		{
			this.amoDatenAktuell.attr('laenderVersion', this.laenderVersionen[dasLand]);

			steal.dev.log('aktualisierenLaenderVersion: ' + this.laenderVersionen[dasLand].ref);
			//this.amoStatus.ausgewaehltesLand = dasLand;
			this.amoStatus.attr("ausgewaehltesLand", dasLand);
			
			steal.dev.log('aktualisierenLaenderVersion ' +this.amoStatus.ausgewaehltesLand + dasLand );
		}
		else
		{
			this.amoDatenAktuell.attr('laenderVersion', this.laenderVersionen[ this.options.ausgewaehltesLandDefault ]);
			steal.dev.log('aktualisierenLaenderVersion2:' + this.options.ausgewaehltesLandDefault);
			//this.amoStatus.ausgewaehltesLand = 'usa';
			//this.amoStatus.attr("ausgewaehltesLand", dasLand);
			this.amoStatus.attr("ausgewaehltesLand", this.options.ausgewaehltesLandDefault );
			 
		}
		steal.dev.log("aktualisierenLaenderVersion 3 ");
		//das überschreibt eventuell unabsichtlich den alten cache
		//this.speichernAmoStatusImCache();

//		this.amoDatenAktuell.attr('sprachVariante', this.sprachVarianten[ this.laenderVersionen[ this.amoDatenAktuell.laenderVersion.ref ].sprache ]);
		this.amoDatenAktuell.attr('sprachVariante', this.sprachVarianten[ this.amoDatenAktuell.attr('laenderVersion').attr('sprache') ]);
	steal.dev.log("aktualisierenLaenderVersion 3b ");
		//this.amoDatenAktuell.attr('isiBlock', this.isiBloecke[ this.laenderVersionen[dasLand].isiBlock ] );
		this.amoDatenAktuell.attr('counter', Math.random() );
		steal.dev.log("aktualisierenLaenderVersion 4 " + this.amoDatenAktuell.counter);
		//Country-Select reinitialisieren
		this.auswaehlenLandSelectMenue();

		//Symfony überprüfung für dieses Land
		this.checkSymfony();
	},
	checkSymfony: function() {
		//if ( this.amoDatenAktuell.laenderVersion.products.length > 4 )
		if ( this.amoDatenAktuell.laenderVersion.products.indexOf(1) > 0 || this.amoDatenAktuell.laenderVersion.products.indexOf(3) > 0  )
		{this.derBodyEl.addClass("mitMonofocal");}
		else{this.derBodyEl.removeClass("mitMonofocal");}

		if ( this.amoDatenAktuell.laenderVersion.products.indexOf(2) > 0 || this.amoDatenAktuell.laenderVersion.products.indexOf(4) > 0  )
		{this.derBodyEl.addClass("mitMultifocal");}
		else{this.derBodyEl.removeClass("mitMultifocal");}

		if ( this.amoDatenAktuell.laenderVersion.products.indexOf(7) > 0 || this.amoDatenAktuell.laenderVersion.products.indexOf(8) > 0  )
		{this.derBodyEl.addClass("mitSymfony");}
		else{this.derBodyEl.removeClass("mitSymfony");}

		if ( this.amoDatenAktuell.laenderVersion.products.indexOf(9) > 0 || this.amoDatenAktuell.laenderVersion.products.indexOf(10) > 0  )
		{this.derBodyEl.addClass("mitSynergy");}
		else{this.derBodyEl.removeClass("mitSynergy");}

		if ( this.amoDatenAktuell.laenderVersion.products.indexOf(11) > 0 || this.amoDatenAktuell.laenderVersion.products.indexOf(12) > 0  )
		{this.derBodyEl.addClass("mitSonata");}
		else{this.derBodyEl.removeClass("mitSonata");}

		if ( this.amoDatenAktuell.laenderVersion.products.indexOf(13) > 0 || this.amoDatenAktuell.laenderVersion.products.indexOf(14) > 0  )
		{this.derBodyEl.addClass("mitEyehance");}
		else{this.derBodyEl.removeClass("mitEyehance");}
	},
	auswaehlenLandSelectMenue: function() {
		//Im Select-Menue das ausgewählte Land anzeigen
		this.countrySelectEl.val( this.amoStatus.ausgewaehltesLand );

		this.countrySelectAnzeigeEl.html( this.amoDatenAktuell['laenderVersion'].auswahl );

		//das Auffrischen ist nur mit jQueryMobile nötig
		/*if ( this.fertigStartSeite === true )
		{
			this.countrySelectEl.selectmenu( "refresh", true );
		}*/

		//steal.dev.log( 'auswaehlenLandSelectMenue' + this.amoStatus.ausgewaehltesLand );
	},
	' landgewaehlt': function( el, event, land ) {
		//steal.dev.log('landgewaehlt ' + land);

		this.aktualisierenLaenderVersion( land );
		//steal.dev.log( this.amoDatenAktuell.sprachVariante.isiLink);

		//Das Land für das nächste Mal speichern
		//das passiert bereits in this.aktualisierenLaenderVersion( land );
		//this.amoStatus.ausgewaehltesLand = land;
		this.speichernAmoStatusImCache();

	},
	' szenenwechsel': function( el, event, szenenWahl ) {
		//steal.dev.log('szenegewaehlt ' + szenenWahl);

		if( szenenWahl === 2 )
		{
			this.amoStatus.attr({
				dieSzene: 2, 
				szenenOrdner: 'sim2'
			}, false );
		}
		else if ( szenenWahl === 1 )
		{
			this.amoStatus.attr({
				dieSzene: 1, 
				szenenOrdner: 'sim1'
			}, false );
		}
		else
		{
			this.amoStatus.attr({
				dieSzene: 0, 
				szenenOrdner: 'sim'
			}, false );
		}

/*		if( szenenWahl === 2 )
		{
			this.amoStatus.dieSzene = 2;
			this.amoStatus.szenenOrdner = 'sim2';
		}
		else if ( szenenWahl === 1 )
		{
			this.amoStatus.dieSzene = 1;
			this.amoStatus.szenenOrdner = 'sim1';
		}
		else
		{
			this.amoStatus.dieSzene = 0;
			this.amoStatus.szenenOrdner = 'sim';
		}
*/

		//Die Szene für das nächste Mal speichern
		//steal.dev.log( this.amoStatus.dieSzene );
		this.speichernAmoStatusImCache();

	},
	' bereit': function( el, event) {
		bereitsFertig = true;

		console.log("bereit datenanbieter - appStateCounter" + appStateCounter);

		if ( appStateCounter >= 3 )
		{
			this.appInit = true; //Testweise
			var self = this, schleifenZaehler = 0;
			steal.dev.log("bereit appFertig" + appFertig);
			/*do
			{
				//steal.dev.log('warten auf appFertig');
				schleifenZaehler += 1;
			} while ( !appFertig && schleifenZaehler < 40000 )*/

			steal.dev.log('bereit appStateCounter ' + appStateCounter);
			//alert('bereit appStateCounter ' + appStateCounter);

			//var warten1 = setTimeout( function(){
				this.pruefenAufDatenImCache();

				//Wenn gespeichert, das Land laden und setzen
				this.pruefenAufStatusImCache();
			//}, 1000 );


this.aktualisierenAmoDaten();


			steal.dev.log('bereit funktion this.amoStatus.ausgewaehltesLand: ' + this.amoStatus.ausgewaehltesLand);
			//Das im Cache gespeicherte Land laden, wenn nicht ausgewaehltesLandDefault -> usa
			if ( this.amoStatus.ausgewaehltesLand !== this.options.ausgewaehltesLandDefault )
			{
				this.aktualisierenLaenderVersion( this.amoStatus.ausgewaehltesLand );
			}

			if ( this.options.webApp !== true || this.options.desktopApp === true )
			{	
				//in der Web-App werden erst alle Bilder geladen und dann ausgeblendet
				this.ausblendenScreen();

				if ( this.options.webApp !== true )
				{
					//navigator.splashscreen.hide();
				}

				var warten = setTimeout( function(){
						self.pruefenVersionVomServer();
				}, 1000 );

				this.vorausladenBilder();
				this.vorausladenSimulatorBilder();
			}

		}
		else
		{
			console.log('noch nicht ganz fertig ' + appStateCounter );
		}

		//this.appInit = true;
	},
	' bildergeladen': function( el, event ) {
		steal.dev.log("bildergeladen appInit" + this.appInit);
		if( this.appInit === false )
		{
			this.derBodyEl.trigger('bereit');
			
		}

		this.ausblendenScreen();
		this.pruefenVersionVomServer();
	},
	'#triggerUpdate click': function( el, event) {
		this.pruefenVersionVomServer();
	},
	'#triggerCache click': function( el, event) {
		this.pruefenAufDatenImCache();
	},
	'#updateKringel click': function( el, event) {
		//Status auf ladend setzen
		this.amoStatus.attr("status", "syncLoading");

		//Lade-Kringel einblenden
		this.derBodyEl.toggleClass('loadingFromServer', true);

		//this.acceptButtonEl.button('disable');
		this.umschaltenAcceptButton( true );

		this.ladenConfigVomServer('updateKringel');
	},
	ausblendenScreen: function() {
		steal.dev.log("ausblendenScreen");
		$('#pagecontainer').toggleClass( 'initScreen', false );

	},
	vorausladenBilder: function() {

		if ( document.images ) {
			this.img1 = new Image();
				this.img1.src = this.imagePfad + "images/gph_simulator_haken_on.png";
			this.img2 = new Image();
				this.img2.src = this.imagePfad + "images/Logo_TFamily_VStitle_o.png";
			this.img3 = new Image();
				this.img3.src = this.imagePfad + "images/btn_anatomy_slider_touch.png";
			this.img4 = new Image();
				this.img4.src = this.imagePfad + "images/btn_simulator_slider_touch.png";
		}
	},
	vorausladenSimulatorBilder: function() {

		if ( document.images ) {
			this.loadingDivEl = $('<div></div>');
			this.loadingDivEl.append( this.simImages["hyperopia"] );
			this.loadingDivEl.append( this.simImages["myopia"] );
			this.loadingDivEl.append( this.simImages["astigmatism"] );
			this.loadingDivEl.append( this.simImages["presbyopia"] );
			this.loadingDivEl.append( this.simImages["presbyopia"] );
			this.loadingDivEl.append( this.simImages["cataract"] );
			this.loadingDivEl.append( this.simImages["cataractsurgery"] );

				/*
					case 'normalvision':
					hyperopia
					case 'myopia':
					case 'astigmatism':
					case 'presbyopia':
					case 'cataract':
					case 'cataractsurgery':
				*/
		}
	},
	pruefenAufDatenImCache: function() {
		//Diese Funktion sollte nie aufgerufen werden
		//"pruefenAufDatenImCache" referenziert entweder: pruefenAufDatenImCacheWebApp oder pruefenAufDatenImCacheAppMobi
		//steal.dev.log('pruefenAufDatenImCache');
	},
	pruefenAufDatenImCacheDesktopApp: function() {
		if( store.enabled ) 
		{
		   console.log("localStorage Node-Webkit verfügbar");

			this.amoDataCache = store.get('amoCache' );

		   if( this.amoDataCache !== undefined )
		   {
		   		console.log("Daten im Cache vorhanden");
		   		this.amoConfigData = this.amoDataCache;
				//this.aktualisierenStatusVomCache();
		   }
		   else
		   {
		   	 console.log("keine Daten im Cache vorhanden");
		   }
		} 
		else 
		{
		   //kein Speicher vorhanden
		}
	},
	pruefenAufDatenImCacheWebApp: function() {
		//Für die Web-App werden keine Daten gespeichert, sondern nur vom Server geladen

	},
	pruefenAufDatenImCacheAppMobi: function() {
		if( store.enabled ) 
		{
		   console.log("localStorage App-Storage verfügbar");

			this.amoDataCache = store.get('amoCache' );

		   if( this.amoDataCache !== undefined )
		   {
		   		console.log("Daten im Cache vorhanden");
		   		this.amoConfigData = this.amoDataCache;
				//this.aktualisierenStatusVomCache();
		   }
		   else
		   {
		   	 console.log("keine Daten im Cache vorhanden");
		   }
		} 
		else 
		{
		   //kein Speicher vorhanden
		}
	},
	pruefenAufStatusImCache: function() {
		//steal.dev.log('pruefenAufStatusImCache');
	},
	pruefenAufStatusImCacheWebApp: function() {
		console.log('pruefenAufStatusImCacheWebApp');

		if( store.enabled ) 
		{
		   steal.dev.log("localStorage verfügbar");

			this.amoStatusVomCacheData = store.get('amoStatusCache' );

		   if( this.amoStatusVomCacheData !== undefined )
		   {
				console.log('LS ' + this.amoStatusVomCacheData.ausgewaehltesLand );
				this.aktualisierenStatusVomCache();
		   }
		} 
		else 
		{
		   //kein Speicher vorhanden
		}
	},
	pruefenAufStatusImCacheAppMobi: function() {
		console.log('pruefenAufStatusImCacheIntelApp');

		if( store.enabled ) 
		{
		   steal.dev.log("localStorage verfügbar");

			this.amoStatusVomCacheData = store.get('amoStatusCache' );

		   if( this.amoStatusVomCacheData !== undefined )
		   {
				console.log('LS ' + this.amoStatusVomCacheData.ausgewaehltesLand );
				this.aktualisierenStatusVomCache();
		   }
		} 
		else 
		{
		   //kein Speicher vorhanden
		}
	},
	aktualisierenStatusVomCache: function() {
	//can.batch.start;
console.log("aktualisierenVomCache");

		if ( this.options.webApp !== true || this.options.desktopApp === true ) {
			//Update-Datum aus dem Cache
			this.amoStatus.attr("upDate", this.amoStatusVomCacheData.upDate );
console.log("status: " + this.amoStatus.attr("upDate"));
			//nur in der iPad-App die Version aus dem Cache holen
			//Cache-Version in amoStatus übertragen
			this.amoStatus.attr("version", this.amoStatusVomCacheData.version );
			//this.amoStatus.version = this.amoStatusVomCacheData.version;
		}

		if ( this.options.desktopApp === true )
		{
			//gespeicherte Passwortkontrolle laden
			this.amoStatus.attr("passwortKontrolle", this.amoStatusVomCacheData.passwortKontrolle );
		}

		//das Land aus dem Cach lesen
		this.amoStatus.attr("ausgewaehltesLand", this.amoStatusVomCacheData.ausgewaehltesLand );

		//keinen Ladestatus mehr anzeigen - Hier nötig????????????
		this.amoStatus.attr("status", "syncConnected" );

	//can.batch.stop;

		//Szenen-Wahl aus dem Cache laden
		this.derBodyEl.trigger('szenenwechsel', this.amoStatusVomCacheData.dieSzene);
			//this.amoStatus.dieSzene = this.amoStatusVomCacheData.dieSzene;
			//this.amoStatus.szenenOrdner = this.amoStatusVomCacheData.szenenOrdner;

		steal.dev.log('appMobi Status-Cache vorhanden ' + this.amoStatus.ausgewaehltesLand );
	},
	pruefenVersionVomServer: function() {
		steal.dev.log('pruefenVersionVomServer sollte nie angezeigt werden');
	},
	pruefenVersionVomServerDesktop: function() {
		//DesktopApp
		var self = this;
steal.dev.log("pruefenVersionVomServerDesktop");
		this.jqxhrVersionData = $.ajax( {
			dataType: "json",
			type: "GET",
			crossDomain: true,
			cache: false,
  			url:  this.options.serverPfad + "tvs_vs.json" 
		})
		.done(function( amoSVData ) { 
			
			self.amoSVDataAmoVersion = amoSVData.amoVersion;

			self.abgleichenVersion();
			
		})
		.fail(function( jqxhr, textStatus, error ) { 
			steal.dev.log("error pruefenVersionOhneServer " + textStatus + ' - ' + error); 
		})
		.always(function() { steal.dev.log('Complete Version-Anfrage'); 
		});
	},
	pruefenVersionOhneServer: function() {
		//WebApp
		var self = this;
steal.dev.log("pruefenVersionOhneServer");
		// Assign handlers immediately after making the request,
		// and remember the jqxhr object for this request
		//this.jqxhrVersionData = $.getJSON( this.options.serverPfad + "tvs_version.json", function() {
		this.jqxhrVersionData = $.ajax( {
			dataType: "json",
			type: "GET",
			crossDomain: true,
			cache: false,
  			url:  this.options.serverPfad + "tvs_vs.json" 
		})
		.done(function( amoSVData ) { 
			steal.dev.log( "Success Versions-Abfrage " + amoSVData.amoVersion + ' ' + self.amoStatus.version );
			self.amoSVDataAmoVersion = amoSVData.amoVersion;

			if ( Number( self.amoStatus.version ) !== Number ( self.amoSVDataAmoVersion ) )
			{
				steal.dev.log("Version unterschiedlich");
				//wenn neuere Version auf dem Server, Meldung einblenden
				$('#updateWarnung').css('display', 'block');
				self.aktualisierenStatus();
				self.speichernAmoStatusImCache();

			}
			
		})
		.fail(function( jqxhr, textStatus, error ) { 
			//steal.dev.log("error pruefenVersionOhneServer " + textStatus + ' - ' + error); 
		})
		.always(function() { //steal.dev.log('Complete Version-Anfrage'); 
		});
	},
	pruefenVersionVomServerAjax: function() {
		//iOS-App
		var self = this;

		// Assign handlers immediately after making the request,
		// and remember the jqxhr object for this request
		this.jqxhrVersionData = $.ajax( {
			dataType: "json",
			cache: false,
			crossDomain: true,
			type: "GET",
  			url:  self.options.serverPfad + "tvs_vs.json" 
		})
		.done(function( amoSVData ) { 
			steal.dev.log( "Success Versions-Abfrage " + amoSVData.amoVersion );
			self.amoSVDataAmoVersion = amoSVData.amoVersion;
			console.log("pruefenVersionVomServerAjax ---2 " + self.options.serverPfad);
			self.abgleichenVersion();
			steal.dev.log("pruefenVersionVomServerAjax ---3");
		})
		.fail(function( jqxhr, textStatus, error ) { 
			console.log("error Version-Anfrage " + textStatus + " " + error); 
		})
		.always(function() { //steal.dev.log('Complete Version-Anfrage'); 
		});
		 
	},
	auslesenDatum: function() { //das heutige Datum auslesen und speichern
		this.datumHeute = new Date();
		this.lesbaresDatumHeute = ( this.datumHeute.getMonth() + 1 ) + '/' + this.datumHeute.getDate()  + '/' + this.datumHeute.getFullYear();
	},
	abgleichenVersion: function() {
		console.log("abgleichenVersion---");
		this.auslesenDatum();
		steal.dev.log("abgleichenVersion---1");
		this.amoStatus.attr("upDate", this.lesbaresDatumHeute );
		steal.dev.log("abgleichenVersion---2");

		//steal.dev.log('letztes Update ' + this.lesbaresDatumHeute );

		//Wenn Server-Version abweicht - Daten vom Server laden
		if ( Number(this.amoStatus.version) === Number(this.amoSVDataAmoVersion) )
		{
			//'last Update' auf das neue Datum setzten
			console.log("Versionsabgleich identisch Datum" + this.lesbaresDatumHeute + this.amoSVDataAmoVersion + this.amoStatus.version);
		}
		else
		{
			steal.dev.log("Versionsabgleich UNTERSCHIED Datum" + this.lesbaresDatumHeute + ' serverVersion ' + this.amoSVDataAmoVersion + ' amoStatus.version ' + this.amoStatus.version);

			//Status auf ladend setzen
			this.amoStatus.attr("status", "syncLoading"); //DRJOCHUM
		console.log("abgleichenVersion---3");

			//Lade-Kringel einblenden
			this.derBodyEl.toggleClass('loadingFromServer', true);
		steal.dev.log("abgleichenVersion---4");

			//this.acceptButtonEl.button('disable');
			this.umschaltenAcceptButton( true );

		steal.dev.log("abgleichenVersion---5");
			this.ladenConfigVomServer();
		steal.dev.log("abgleichenVersion---6");
		}
	},
	ladenConfigVomServer: function( ausgeloestVon ) {
		var self = this;

		this.jqxhrVersionData = $.ajax( {
			dataType: "json",
			cache: false,
			crossDomain: true,
			type: "GET",
  			url:  self.options.serverPfad + "tvs_data.json" 
		})
		.done(function( amoSConfigData ) { 
			console.log('Erfolg Config vom Server amoSConfigData.amoStatus.version ' + amoSConfigData.amoStatus.version + ' Sprache ' + amoSConfigData.sprachVarianten.english.welcomeMenuH2);
			self.amoConfigData = amoSConfigData;
			self.aktualisierenAmoDaten();
			//steal.dev.log('ladenConfigVomServer ' + self.amoStatus.ausgewaehltesLand);
			if ( self.amoStatus.ausgewaehltesLand !== self.options.ausgewaehltesLandDefault )
			{
				if ( self.amoConfigData.laenderVersionen[ self.amoStatus.ausgewaehltesLand ] !== undefined )
				{
					self.aktualisierenLaenderVersion( self.amoStatus.ausgewaehltesLand );
				}
				else
				{
					self.aktualisierenLaenderVersion( self.options.ausgewaehltesLandDefault );
				}
			}
			else
			{
				self.aktualisierenLaenderVersion( self.options.ausgewaehltesLandDefault );
			}

			self.aktualisierenStatus();

			if ( self.options.webApp === false || self.options.desktopApp === true )
			{
				self.speichernAmoDatenImCache();
				self.speichernAmoStatusImCache();
			}
		})
		.fail(function( jqxhr, textStatus, error) {
			console.log("Error1 " + textStatus + error);/**/ 
			//steal.dev.log('Fehler laden vom Server Config ' + ausgeloestVon);

			if ( ausgeloestVon === 'updateKringel' )
			{
				//Fehler irgendwie anders anzeigen???
				self.amoStatus.attr("status", "syncNotConnected" );

				var warten1 = setTimeout( function(){
					self.amoStatus.attr("status", "syncConnected" );
				}, 3000 );
			}
			else
			{
				//keinen Ladestatus mehr anzeigen 
				self.amoStatus.attr("status", "syncConnected" );
			}

			//Lade-Kringel wieder ausblenden
			self.derBodyEl.toggleClass('loadingFromServer', false);
			self.umschaltenAcceptButton( false );
		})
		.always(function() { /*alert("complete");*/ });

		//alert("die Daten wurden gerade aktualisiert");
		 
	},
	speichernAmoDatenImCache: function() {
		//steal.dev.log('speichernAmoDatenImCache');
	},
	speichernAmoDatenImCacheDesktopApp: function() {
		steal.dev.log('speichernAmoDatenImCache Desktop');
		if( store.enabled ) 
		{	
			steal.dev.log('Config speichern Desktop-App');
			//var testObjekt = JSON.stringify( this.amoConfigData );  
			//steal.dev.log(testObjekt);
			store.set('amoCache', this.amoConfigData );
		} 
		else 
		{
		   //Speichern unmöglich
		}

	},
	speichernAmoDatenImCacheWebApp: function() {
		//in der WebApp werden die Config-Daten nicht im Cache gespeichert


	},
	speichernAmoDatenImCacheAppMobi: function() {
		if( store.enabled ) 
		{	
			console.log('Config speichern Intel-App');
			//var testObjekt = JSON.stringify( this.amoConfigData );  
			//steal.dev.log(testObjekt);
			store.set('amoCache', this.amoConfigData );
		} 
		else 
		{
		   //Speichern unmöglich
		   console.log("Speichern unmöglich");
		}
	},
	speichernAmoStatusImCache: function() {
		//steal.dev.log('speichernAmoStatusImCache');
	},
	speichernAmoStatusImCacheWebApp: function() {
		if( store.enabled ) 
		{
			//var testObjekt = JSON.stringify( this.amoStatus );  
			//this.amoStatus.serialize();??
		   //steal.dev.log("localStorage is available " + this.amoStatus.ausgewaehltesLand + " " + testObjekt);
			store.set('amoStatusCache', this.amoStatus );
		} 
		else 
		{
		   //Speichern unmöglich
		}
	},
	speichernAmoStatusImCacheAppMobi: function() {
		if( store.enabled ) 
		{
			//var testObjekt = JSON.stringify( this.amoStatus );  
			//this.amoStatus.serialize();??
		   //steal.dev.log("localStorage is available " + this.amoStatus.ausgewaehltesLand + " " + testObjekt);
			store.set('amoStatusCache', this.amoStatus );
		} 
		else 
		{
		   //Speichern unmöglich
		}
	},
	filternlaenderVersionen: function() {
		var self = this;
		if ( this.options.desktopApp === true )
		{
			steal.dev.log('Länderversionen filtern');
			//this.laenderversionen zusammensetzen
			this.laenderVersionen = {};

			$.each( sprachenListe, function( index, wert ) {
				
				self.laenderVersionen[ wert ] = self.amoConfigData.laenderVersionen[ wert ];

				//Routing number anpassen für Desktop-Version
				self.laenderVersionen[ wert ].routingNumber = self.laenderVersionen[ wert ].routingNumberOff;
			});
		}
		else
		{
			this.laenderVersionen = this.amoConfigData.laenderVersionen;
		}
	},
	aktualisierenAmoDaten: function() {
		steal.dev.log('aktualisierenAmoDaten');
//can.batch.start;
		this.filternlaenderVersionen();

		//this.laenderVersionen = this.amoConfigData.laenderVersionen;
		this.sprachVarianten = this.amoConfigData.sprachVarianten;

		//Versionsnummer aktualisieren
		this.amoDatenAktuell.attr('amoStatus', this.amoConfigData.amoStatus );

		//LLL Prüfen, ob das Land aus amoDatenAktuell.LaenderVersion überhaut vorhanden ist
		//steht schon in "aktualisierenLaenderVersion" -- ist das jetzt doppelt??
		if ( this.amoConfigData.laenderVersionen[ this.amoDatenAktuell.laenderVersion.ref ] !== undefined )
		{		
			//Daten in Observable aufnehmen und alle Inhalte aktualisieren
			steal.dev.log('aktualisierenAmoDaten aufgerufen ' + this.amoDatenAktuell.laenderVersion.ref);
			//this.aktualisierenLaenderVersion( this.amoDatenAktuell.laenderVersion.ref );
			this.aktualisierenLaenderVersion( this.amoStatus.ausgewaehltesLand );
		}
		else
		{
			steal.dev.log('aktualisierenAmoDaten aufgerufen 22 ' + this.amoDatenAktuell.laenderVersion.ref);
			this.aktualisierenLaenderVersion( this.options.ausgewaehltesLandDefault );
		}

		this.derBodyEl.trigger('neuedaten',{ laenderVersionen: this.laenderVersionen });
		steal.dev.log('aktualisierenAmoDaten aufgerufen 33 ');
//can.batch.stop;
	},
	aktualisierenStatus: function() {
		steal.dev.log("aktualisierenStatus---");
	can.batch.start;

		//Update-Datum festhalten und Anzeigen
		//JD this.amoStatus.attr("upDate", this.lesbaresDatumHeute );
		steal.dev.log("aktualisierenStatus--1");

		//Version speichern
		this.amoStatus.attr("version", this.amoConfigData.amoStatus.version );

		steal.dev.log("aktualisierenStatus--2 " + this.amoStatus.status);
		//keinen Ladestatus mehr anzeigen - hier nötig ????????
		this.amoStatus.attr("status","syncConnected");
		//this.amoDatenAktuell.attr("amoConnectionStatusLang", this.amoDatenAktuell.sprachVariante[""])
		
		steal.dev.log("aktualisierenStatus--2b ");
	can.batch.stop;

		steal.dev.log("aktualisierenStatus--2c " + this.amoStatus.status);
		this.derBodyEl.toggleClass('loadingFromServer', false);
		steal.dev.log("aktualisierenStatus--3");
		this.umschaltenAcceptButton( false ); // ????????
		steal.dev.log("aktualisierenStatus--4");
		//steal.dev.log('aktualisierenStatus beendet - version: ' + this.amoConfigData.amoStatus.version);
	},	
	umschaltenAcceptButton: function( einaus ) {
		this.acceptBlockEl.toggleClass('disabled', einaus );
	},
	'#startseite pageinit': function(el, ev) {
			steal.dev.log('Page init Startseite');

			this.fertigStartSeite = true;
		},
	' pagechange': function( el, ev, info ) {
		//steal.dev.log('seite ' + info.toPage.data('seite'));

		//if ( info.toPage.data('seite') === 'startseite' && this.amoConfigData.amoStatus.upDate !== this.lesbaresDatumHeute && this.lesbaresDatumHeute !== undefined )
		if ( info.toPage.data('seite') === 'startseite' )
		{
			//steal.dev.log('Page change Startseite');

			//nicht bei Initialisierung ausführen
			if ( this.pageInit === true )
			{
				this.pageInit = false;
			}
			else
			{
				//steal.dev.log('Page change - upDate: ' + this.amoConfigData.amoStatus.upDate + ' lesbaresDatumHeute: ' + this.lesbaresDatumHeute);
				this.pruefenVersionVomServer();
			}
		}
	},
	'{seitenCompute} change': function( frage, ev, value ) {
			var self = this;

			if ( value === '#startseite' )
			{
					this.pruefenVersionVomServer();
				
			}
	},
	anpassenAmoStatus: function( statusText ) {
		// body...
	},
	anlegenDaten: function() {
		var self = this;

		if ( this.options.desktopApp === true )
		{
			this.options.ausgewaehltesLandDefault = sprachenListe[0];
		}
	//steal.dev.log("anlegenDaten");
		//die lokalen Initial-Daten aus amoData.js in den Zwischenspeicher holen
		this.amoConfigData = amoData;

		this.amoStatus = new can.Map({
			version: amoData.amoStatus.version,
			upDate: amoData.amoStatus.upDate,
			ausgewaehltesLand: this.options.ausgewaehltesLandDefault,
			status: "syncConnected", 
			dieSzene: 0,
			szenenOrdner: 'sim',
			zeitStempel: 1,
			passwortKontrolle: true
		});

		//Wenn Passwort eingegeben wurde, Info speichern
		this.amoStatus.bind("passwortKontrolle", function( ev, neuWert, altWert ){
			if ( neuWert === false )
			{
				self.speichernAmoStatusImCache();
			}
		});

		this.amoStatus.bind("ausgewaehltesLand", function( ev, neuWert, altWert ){
			
			self.derBodyEl.removeClass( "land-" + altWert );

			self.derBodyEl.addClass( "land-" + neuWert );
			
		});

		this.amoDatenAktuell = new can.Map({
			counter: 1,
			amoStatus: amoData.amoStatus,
			amoConnectionStatusLang: '',
			laenderVersion: {ref: 'nichts'},
			sprachVariante: {ref: 'nichts'}
		});

		this.laenderVersionen = amoData.laenderVersionen;

		this.sprachVarianten = amoData.sprachVarianten;

		this.simulatorBilder = [
			[ //ohne Astigmatismus
				['tvs0_d_normal.jpg','tvs0_d_presby.jpg','tvs0_d_catara.jpg','tvs0_d_stdmon.jpg','tvs0_d_tcmono.jpg','tvs0_n_stdmon.jpg','tvs0_n_tcmono.jpg','tvs0_n_tcmono.jpg'],
				['tvs0_d_normal.jpg','tvs0_d_presby.jpg','tvs0_d_catara.jpg','tvs0_d_tcmono.jpg','tvs0_d_tcmult.jpg','tvs0_n_tcmono.jpg','tvs0_n_tcmul1.jpg','tvs0_n_tcmul2.jpg'],
				['tvs0_d_normal.jpg','tvs0_d_presby.jpg','tvs0_d_catara.jpg','tvs0_d_tcmono.jpg','tvs0_d_tctorc.jpg','tvs0_n_tcmono.jpg','tvs0_n_tctorc.jpg','tvs0_n_tctorc.jpg'],
				['tvs0_d_normal.jpg','tvs0_d_presby.jpg','tvs0_d_catara.jpg','tvs0_d_tcmono.jpg','tvs0_d_tcmfto.jpg','tvs0_n_tcmono.jpg','tvs0_n_tcmft1.jpg','tvs0_n_tcmft2.jpg'],
				['tvs0_d_normal.jpg','tvs0_d_presby.jpg','tvs0_d_catara.jpg','tvs0_d_tcmono.jpg','tvs0_d_symfon.jpg','tvs0_n_tcmono.jpg','tvs0_n_symfon.jpg','tvs0_n_symfon.jpg'],
				['tvs0_d_normal.jpg','tvs0_d_presby.jpg','tvs0_d_catara.jpg','tvs0_d_tcmono.jpg','tvs0_d_symfto.jpg','tvs0_n_tcmono.jpg','tvs0_n_symfto.jpg','tvs0_n_symfto.jpg'],

				['tvs0_d_normal.jpg','tvs0_d_presby.jpg','tvs0_d_catara.jpg','tvs0_d_tcmono.jpg','tvs0_d_synerg.jpg','tvs0_n_tcmono.jpg','tvs0_n_synerg.jpg','tvs0_n_synerg.jpg'],
				['tvs0_d_normal.jpg','tvs0_d_presby.jpg','tvs0_d_catara.jpg','tvs0_d_tcmono.jpg','tvs0_d_syneto.jpg','tvs0_n_tcmono.jpg','tvs0_n_syneto.jpg','tvs0_n_syneto.jpg'],
				['tvs0_d_normal.jpg','tvs0_d_presby.jpg','tvs0_d_catara.jpg','tvs0_d_tcmono.jpg','tvs0_d_sonata.jpg','tvs0_n_tcmono.jpg','tvs0_n_sonata.jpg','tvs0_n_sonata.jpg'],
				['tvs0_d_normal.jpg','tvs0_d_presby.jpg','tvs0_d_catara.jpg','tvs0_d_tcmono.jpg','tvs0_d_sonato.jpg','tvs0_n_tcmono.jpg','tvs0_n_sonato.jpg','tvs0_n_sonato.jpg'],
				['tvs0_d_normal.jpg','tvs0_d_presby.jpg','tvs0_d_catara.jpg','tvs0_d_tcmono.jpg','tvs0_d_eyhanc.jpg','tvs0_n_tcmono.jpg','tvs0_n_eyhanc.jpg','tvs0_n_eyhanc.jpg'],
				['tvs0_d_normal.jpg','tvs0_d_presby.jpg','tvs0_d_catara.jpg','tvs0_d_tcmono.jpg','tvs0_d_eyhcto.jpg','tvs0_n_tcmono.jpg','tvs0_n_eyhcto.jpg','tvs0_n_eyhcto.jpg']
			],[ //mit Astigmatismus Stufe 1
				['tvs1_d_normal.jpg','tvs1_d_presby.jpg','tvs1_d_catara.jpg','tvs1_d_stdmon.jpg','tvs1_d_tcmono.jpg','tvs1_n_stdmon.jpg','tvs1_n_tcmono.jpg','tvs1_n_tcmono.jpg'],
				['tvs1_d_normal.jpg','tvs1_d_presby.jpg','tvs1_d_catara.jpg','tvs1_d_tcmono.jpg','tvs1_d_tcmult.jpg','tvs1_n_tcmono.jpg','tvs1_n_tcmul1.jpg','tvs1_n_tcmul2.jpg'],
				['tvs1_d_normal.jpg','tvs1_d_presby.jpg','tvs1_d_catara.jpg','tvs1_d_tcmono.jpg','tvs0_d_tctorc.jpg','tvs1_n_tcmono.jpg','tvs0_n_tctorc.jpg','tvs0_n_tctorc.jpg'],
				['tvs1_d_normal.jpg','tvs1_d_presby.jpg','tvs1_d_catara.jpg','tvs1_d_tcmono.jpg','tvs0_d_tcmfto.jpg','tvs1_n_tcmono.jpg','tvs0_n_tcmft1.jpg','tvs0_n_tcmft2.jpg'],
				['tvs1_d_normal.jpg','tvs1_d_presby.jpg','tvs1_d_catara.jpg','tvs1_d_tcmono.jpg','tvs1_d_symfon.jpg','tvs1_n_tcmono.jpg','tvs1_n_symfon.jpg','tvs1_n_symfon.jpg'],
				['tvs1_d_normal.jpg','tvs1_d_presby.jpg','tvs1_d_catara.jpg','tvs1_d_tcmono.jpg','tvs0_d_symfto.jpg','tvs1_n_tcmono.jpg','tvs0_n_symfto.jpg','tvs0_n_symfto.jpg'],

				['tvs1_d_normal.jpg','tvs1_d_presby.jpg','tvs1_d_catara.jpg','tvs1_d_tcmono.jpg','tvs1_d_synerg.jpg','tvs1_n_tcmono.jpg','tvs1_n_synerg.jpg','tvs1_n_synerg.jpg'],
				['tvs1_d_normal.jpg','tvs1_d_presby.jpg','tvs1_d_catara.jpg','tvs1_d_tcmono.jpg','tvs0_d_syneto.jpg','tvs1_n_tcmono.jpg','tvs0_n_syneto.jpg','tvs0_n_syneto.jpg'],
				['tvs1_d_normal.jpg','tvs1_d_presby.jpg','tvs1_d_catara.jpg','tvs1_d_tcmono.jpg','tvs1_d_sonata.jpg','tvs1_n_tcmono.jpg','tvs1_n_sonata.jpg','tvs1_n_sonata.jpg'],
				['tvs1_d_normal.jpg','tvs1_d_presby.jpg','tvs1_d_catara.jpg','tvs1_d_tcmono.jpg','tvs0_d_sonato.jpg','tvs1_n_tcmono.jpg','tvs0_n_sonato.jpg','tvs0_n_sonato.jpg'],
				['tvs1_d_normal.jpg','tvs1_d_presby.jpg','tvs1_d_catara.jpg','tvs1_d_tcmono.jpg','tvs1_d_eyhanc.jpg','tvs1_n_tcmono.jpg','tvs1_n_eyhanc.jpg','tvs1_n_eyhanc.jpg'],
				['tvs1_d_normal.jpg','tvs1_d_presby.jpg','tvs1_d_catara.jpg','tvs1_d_tcmono.jpg','tvs0_d_eyhcto.jpg','tvs1_n_tcmono.jpg','tvs0_n_eyhcto.jpg','tvs0_n_eyhcto.jpg']
			],[ //mit Astigmatismus Stufe 2
				['tvs2_d_normal.jpg','tvs2_d_presby.jpg','tvs2_d_catara.jpg','tvs2_d_stdmon.jpg','tvs2_d_tcmono.jpg','tvs2_n_stdmon.jpg','tvs2_n_tcmono.jpg','tvs2_n_tcmono.jpg'],
				['tvs2_d_normal.jpg','tvs2_d_presby.jpg','tvs2_d_catara.jpg','tvs2_d_tcmono.jpg','tvs2_d_tcmult.jpg','tvs2_n_tcmono.jpg','tvs2_n_tcmul1.jpg','tvs2_n_tcmul2.jpg'],
				['tvs2_d_normal.jpg','tvs2_d_presby.jpg','tvs2_d_catara.jpg','tvs2_d_tcmono.jpg','tvs0_d_tctorc.jpg','tvs2_n_tcmono.jpg','tvs0_n_tctorc.jpg','tvs0_n_tctorc.jpg'],
				['tvs2_d_normal.jpg','tvs2_d_presby.jpg','tvs2_d_catara.jpg','tvs2_d_tcmono.jpg','tvs0_d_tcmfto.jpg','tvs2_n_tcmono.jpg','tvs0_n_tcmft1.jpg','tvs0_n_tcmft2.jpg'],
				['tvs2_d_normal.jpg','tvs2_d_presby.jpg','tvs2_d_catara.jpg','tvs2_d_tcmono.jpg','tvs2_d_symfon.jpg','tvs2_n_tcmono.jpg','tvs2_n_symfon.jpg','tvs2_n_symfon.jpg'],
				['tvs2_d_normal.jpg','tvs2_d_presby.jpg','tvs2_d_catara.jpg','tvs2_d_tcmono.jpg','tvs0_d_symfto.jpg','tvs2_n_tcmono.jpg','tvs0_n_symfto.jpg','tvs0_n_symfto.jpg'],

				['tvs2_d_normal.jpg','tvs2_d_presby.jpg','tvs2_d_catara.jpg','tvs2_d_tcmono.jpg','tvs2_d_synerg.jpg','tvs2_n_tcmono.jpg','tvs2_n_synerg.jpg','tvs2_n_synerg.jpg'],
				['tvs2_d_normal.jpg','tvs2_d_presby.jpg','tvs2_d_catara.jpg','tvs2_d_tcmono.jpg','tvs0_d_syneto.jpg','tvs2_n_tcmono.jpg','tvs0_n_syneto.jpg','tvs0_n_syneto.jpg'],
				['tvs2_d_normal.jpg','tvs2_d_presby.jpg','tvs2_d_catara.jpg','tvs2_d_tcmono.jpg','tvs2_d_sonata.jpg','tvs2_n_tcmono.jpg','tvs2_n_sonata.jpg','tvs2_n_sonata.jpg'],
				['tvs2_d_normal.jpg','tvs2_d_presby.jpg','tvs2_d_catara.jpg','tvs2_d_tcmono.jpg','tvs0_d_sonato.jpg','tvs2_n_tcmono.jpg','tvs0_n_sonato.jpg','tvs0_n_sonato.jpg'],
				['tvs2_d_normal.jpg','tvs2_d_presby.jpg','tvs2_d_catara.jpg','tvs2_d_tcmono.jpg','tvs2_d_eyhanc.jpg','tvs2_n_tcmono.jpg','tvs2_n_eyhanc.jpg','tvs2_n_eyhanc.jpg'],
				['tvs2_d_normal.jpg','tvs2_d_presby.jpg','tvs2_d_catara.jpg','tvs2_d_tcmono.jpg','tvs0_d_eyhcto.jpg','tvs2_n_tcmono.jpg','tvs0_n_eyhcto.jpg','tvs0_n_eyhcto.jpg']
			],[ //mit Astigmatismus Stufe 3
				['tvs3_d_normal.jpg','tvs3_d_presby.jpg','tvs3_d_catara.jpg','tvs3_d_stdmon.jpg','tvs3_d_tcmono.jpg','tvs3_n_stdmon.jpg','tvs3_n_tcmono.jpg','tvs3_n_tcmono.jpg'],
				['tvs3_d_normal.jpg','tvs3_d_presby.jpg','tvs3_d_catara.jpg','tvs3_d_tcmono.jpg','tvs3_d_tcmult.jpg','tvs3_n_tcmono.jpg','tvs3_n_tcmul1.jpg','tvs3_n_tcmul2.jpg'],
				['tvs3_d_normal.jpg','tvs3_d_presby.jpg','tvs3_d_catara.jpg','tvs3_d_tcmono.jpg','tvs0_d_tctorc.jpg','tvs3_n_tcmono.jpg','tvs0_n_tctorc.jpg','tvs0_n_tctorc.jpg'],
				['tvs3_d_normal.jpg','tvs3_d_presby.jpg','tvs3_d_catara.jpg','tvs3_d_tcmono.jpg','tvs0_d_tcmfto.jpg','tvs3_n_tcmono.jpg','tvs0_n_tcmft1.jpg','tvs0_n_tcmft2.jpg'],
				['tvs3_d_normal.jpg','tvs3_d_presby.jpg','tvs3_d_catara.jpg','tvs3_d_tcmono.jpg','tvs3_d_symfon.jpg','tvs3_n_tcmono.jpg','tvs3_n_symfon.jpg','tvs3_n_symfon.jpg'],
				['tvs3_d_normal.jpg','tvs3_d_presby.jpg','tvs3_d_catara.jpg','tvs3_d_tcmono.jpg','tvs0_d_symfto.jpg','tvs3_n_tcmono.jpg','tvs0_n_symfto.jpg','tvs0_n_symfto.jpg'],

				['tvs3_d_normal.jpg','tvs3_d_presby.jpg','tvs3_d_catara.jpg','tvs3_d_tcmono.jpg','tvs3_d_synerg.jpg','tvs3_n_tcmono.jpg','tvs3_n_synerg.jpg','tvs3_n_synerg.jpg'],
				['tvs3_d_normal.jpg','tvs3_d_presby.jpg','tvs3_d_catara.jpg','tvs3_d_tcmono.jpg','tvs0_d_syneto.jpg','tvs3_n_tcmono.jpg','tvs0_n_syneto.jpg','tvs0_n_syneto.jpg'],
				['tvs3_d_normal.jpg','tvs3_d_presby.jpg','tvs3_d_catara.jpg','tvs3_d_tcmono.jpg','tvs3_d_sonata.jpg','tvs3_n_tcmono.jpg','tvs3_n_sonata.jpg','tvs3_n_sonata.jpg'],
				['tvs3_d_normal.jpg','tvs3_d_presby.jpg','tvs3_d_catara.jpg','tvs3_d_tcmono.jpg','tvs0_d_sonato.jpg','tvs3_n_tcmono.jpg','tvs0_n_sonato.jpg','tvs0_n_sonato.jpg'],
				['tvs3_d_normal.jpg','tvs3_d_presby.jpg','tvs3_d_catara.jpg','tvs3_d_tcmono.jpg','tvs3_d_eyhanc.jpg','tvs3_n_tcmono.jpg','tvs3_n_eyhanc.jpg','tvs3_n_eyhanc.jpg'],
				['tvs3_d_normal.jpg','tvs3_d_presby.jpg','tvs3_d_catara.jpg','tvs3_d_tcmono.jpg','tvs0_d_eyhcto.jpg','tvs3_n_tcmono.jpg','tvs0_n_eyhcto.jpg','tvs0_n_eyhcto.jpg']
			],[ //mit Astigmatismus Stufe 4
				['tvs4_d_normal.jpg','tvs4_d_presby.jpg','tvs4_d_catara.jpg','tvs4_d_stdmon.jpg','tvs4_d_tcmono.jpg','tvs4_n_stdmon.jpg','tvs4_n_tcmono.jpg','tvs4_n_tcmono.jpg'],
				['tvs4_d_normal.jpg','tvs4_d_presby.jpg','tvs4_d_catara.jpg','tvs4_d_tcmono.jpg','tvs4_d_tcmult.jpg','tvs4_n_tcmono.jpg','tvs4_n_tcmul1.jpg','tvs4_n_tcmul2.jpg'],
				['tvs4_d_normal.jpg','tvs4_d_presby.jpg','tvs4_d_catara.jpg','tvs4_d_tcmono.jpg','tvs0_d_tctorc.jpg','tvs4_n_tcmono.jpg','tvs0_n_tctorc.jpg','tvs0_n_tctorc.jpg'],
				['tvs4_d_normal.jpg','tvs4_d_presby.jpg','tvs4_d_catara.jpg','tvs4_d_tcmono.jpg','tvs0_d_tcmfto.jpg','tvs4_n_tcmono.jpg','tvs0_n_tcmft1.jpg','tvs0_n_tcmft2.jpg'],
				['tvs4_d_normal.jpg','tvs4_d_presby.jpg','tvs4_d_catara.jpg','tvs4_d_tcmono.jpg','tvs4_d_symfon.jpg','tvs4_n_tcmono.jpg','tvs4_n_symfon.jpg','tvs4_n_symfon.jpg'],
				['tvs4_d_normal.jpg','tvs4_d_presby.jpg','tvs4_d_catara.jpg','tvs4_d_tcmono.jpg','tvs0_d_symfto.jpg','tvs4_n_tcmono.jpg','tvs0_n_symfto.jpg','tvs0_n_symfto.jpg'],

				['tvs4_d_normal.jpg','tvs4_d_presby.jpg','tvs4_d_catara.jpg','tvs4_d_tcmono.jpg','tvs4_d_synerg.jpg','tvs4_n_tcmono.jpg','tvs4_n_synerg.jpg','tvs4_n_synerg.jpg'],
				['tvs4_d_normal.jpg','tvs4_d_presby.jpg','tvs4_d_catara.jpg','tvs4_d_tcmono.jpg','tvs0_d_syneto.jpg','tvs4_n_tcmono.jpg','tvs0_n_syneto.jpg','tvs0_n_syneto.jpg'],
				['tvs4_d_normal.jpg','tvs4_d_presby.jpg','tvs4_d_catara.jpg','tvs4_d_tcmono.jpg','tvs4_d_sonata.jpg','tvs4_n_tcmono.jpg','tvs4_n_sonata.jpg','tvs4_n_sonata.jpg'],
				['tvs4_d_normal.jpg','tvs4_d_presby.jpg','tvs4_d_catara.jpg','tvs4_d_tcmono.jpg','tvs0_d_sonato.jpg','tvs4_n_tcmono.jpg','tvs0_n_sonato.jpg','tvs0_n_sonato.jpg'],
				['tvs4_d_normal.jpg','tvs4_d_presby.jpg','tvs4_d_catara.jpg','tvs4_d_tcmono.jpg','tvs4_d_eyhanc.jpg','tvs4_n_tcmono.jpg','tvs4_n_eyhanc.jpg','tvs4_n_eyhanc.jpg'],
				['tvs4_d_normal.jpg','tvs4_d_presby.jpg','tvs4_d_catara.jpg','tvs4_d_tcmono.jpg','tvs0_d_eyhcto.jpg','tvs4_n_tcmono.jpg','tvs0_n_eyhcto.jpg','tvs0_n_eyhcto.jpg']
			],[ //mit Astigmatismus Stufe 5
				['tvs5_d_normal.jpg','tvs5_d_presby.jpg','tvs5_d_catara.jpg','tvs5_d_stdmon.jpg','tvs5_d_tcmono.jpg','tvs5_n_stdmon.jpg','tvs5_n_tcmono.jpg','tvs5_n_tcmono.jpg'],
				['tvs5_d_normal.jpg','tvs5_d_presby.jpg','tvs5_d_catara.jpg','tvs5_d_tcmono.jpg','tvs5_d_tcmult.jpg','tvs5_n_tcmono.jpg','tvs5_n_tcmul1.jpg','tvs5_n_tcmul2.jpg'],
				['tvs5_d_normal.jpg','tvs5_d_presby.jpg','tvs5_d_catara.jpg','tvs5_d_tcmono.jpg','tvs0_d_tctorc.jpg','tvs5_n_tcmono.jpg','tvs0_n_tctorc.jpg','tvs0_n_tctorc.jpg'],
				['tvs5_d_normal.jpg','tvs5_d_presby.jpg','tvs5_d_catara.jpg','tvs5_d_tcmono.jpg','tvs0_d_tcmfto.jpg','tvs5_n_tcmono.jpg','tvs0_n_tcmft1.jpg','tvs0_n_tcmft2.jpg'],
				['tvs5_d_normal.jpg','tvs5_d_presby.jpg','tvs5_d_catara.jpg','tvs5_d_tcmono.jpg','tvs5_d_symfon.jpg','tvs5_n_tcmono.jpg','tvs5_n_symfon.jpg','tvs5_n_symfon.jpg'],
				['tvs5_d_normal.jpg','tvs5_d_presby.jpg','tvs5_d_catara.jpg','tvs5_d_tcmono.jpg','tvs0_d_symfto.jpg','tvs5_n_tcmono.jpg','tvs0_n_symfto.jpg','tvs0_n_symfto.jpg'],

				['tvs5_d_normal.jpg','tvs5_d_presby.jpg','tvs5_d_catara.jpg','tvs5_d_tcmono.jpg','tvs5_d_synerg.jpg','tvs5_n_tcmono.jpg','tvs5_n_synerg.jpg','tvs5_n_synerg.jpg'],
				['tvs5_d_normal.jpg','tvs5_d_presby.jpg','tvs5_d_catara.jpg','tvs5_d_tcmono.jpg','tvs0_d_syneto.jpg','tvs5_n_tcmono.jpg','tvs0_n_syneto.jpg','tvs0_n_syneto.jpg'],
				['tvs5_d_normal.jpg','tvs5_d_presby.jpg','tvs5_d_catara.jpg','tvs5_d_tcmono.jpg','tvs5_d_sonata.jpg','tvs5_n_tcmono.jpg','tvs5_n_sonata.jpg','tvs5_n_sonata.jpg'],
				['tvs5_d_normal.jpg','tvs5_d_presby.jpg','tvs5_d_catara.jpg','tvs5_d_tcmono.jpg','tvs0_d_sonato.jpg','tvs5_n_tcmono.jpg','tvs0_n_sonato.jpg','tvs0_n_sonato.jpg'],
				['tvs5_d_normal.jpg','tvs5_d_presby.jpg','tvs5_d_catara.jpg','tvs5_d_tcmono.jpg','tvs5_d_eyhanc.jpg','tvs5_n_tcmono.jpg','tvs5_n_eyhanc.jpg','tvs5_n_eyhanc.jpg'],
				['tvs5_d_normal.jpg','tvs5_d_presby.jpg','tvs5_d_catara.jpg','tvs5_d_tcmono.jpg','tvs0_d_eyhcto.jpg','tvs5_n_tcmono.jpg','tvs0_n_eyhcto.jpg','tvs0_n_eyhcto.jpg']
			]

		];

		this.simImages = {
			"normalvision": "<img id='simVision1' src='amo/simulations/sim_norm_jpg/sim_norm_1.jpg' width='712' height='285' alt='' style='visibility:visible;'><img id='simVision2' src='amo/simulations/sim_norm_jpg/sim_norm_2.jpg' width='712' height='285' alt=''><img id='simVision3' src='amo/simulations/sim_norm_jpg/sim_norm_3.jpg' width='712' height='285' alt=''><img id='simVision4' src='amo/simulations/sim_norm_jpg/sim_norm_4.jpg' width='712' height='285' alt=''><img id='simVision5' src='amo/simulations/sim_norm_jpg/sim_norm_5.jpg' width='712' height='285' alt=''><img id='simVision6' src='amo/simulations/sim_norm_jpg/sim_norm_6.jpg' width='712' height='285' alt=''><img id='simVision7' src='amo/simulations/sim_norm_jpg/sim_norm_7.jpg' width='712' height='285' alt=''><img id='simVision8' src='amo/simulations/sim_norm_jpg/sim_norm_8.jpg' width='712' height='285' alt=''><img id='simVision9' src='amo/simulations/sim_norm_jpg/sim_norm_9.jpg' width='712' height='285' alt=''><img id='simVision10' src='amo/simulations/sim_norm_jpg/sim_norm_10.jpg' width='712' height='285' alt=''><img id='simVision11' src='amo/simulations/sim_norm_jpg/sim_norm_11.jpg' width='712' height='285' alt=''><img id='simVision12' src='amo/simulations/sim_norm_jpg/sim_norm_12.jpg' width='712' height='285' alt=''><img id='simVision13' src='amo/simulations/sim_norm_jpg/sim_norm_13.jpg' width='712' height='285' alt=''><img id='simVision14' src='amo/simulations/sim_norm_jpg/sim_norm_14.jpg' width='712' height='285' alt=''><img id='simVision15' src='amo/simulations/sim_norm_jpg/sim_norm_15.jpg' width='712' height='285' alt=''><img id='simVision16' src='amo/simulations/sim_norm_jpg/sim_norm_16.jpg' width='712' height='285' alt=''><img id='simVision17' src='amo/simulations/sim_norm_jpg/sim_norm_17.jpg' width='712' height='285' alt=''><img id='simVision18' src='amo/simulations/sim_norm_jpg/sim_norm_18.jpg' width='712' height='285' alt=''><img id='simVision19' src='amo/simulations/sim_norm_jpg/sim_norm_19.jpg' width='712' height='285' alt=''><img id='simVision20' src='amo/simulations/sim_norm_jpg/sim_norm_20.jpg' width='712' height='285' alt=''><img id='simVision21' src='amo/simulations/sim_norm_jpg/sim_norm_21.jpg' width='712' height='285' alt=''><img id='simVision22' src='amo/simulations/sim_norm_jpg/sim_norm_22.jpg' width='712' height='285' alt=''><img id='simVision23' src='amo/simulations/sim_norm_jpg/sim_norm_23.jpg' width='712' height='285' alt=''><img id='simVision24' src='amo/simulations/sim_norm_jpg/sim_norm_24.jpg' width='712' height='285' alt=''><img id='simVision25' src='amo/simulations/sim_norm_jpg/sim_norm_25.jpg' width='712' height='285' alt=''><img id='simVision26' src='amo/simulations/sim_norm_jpg/sim_norm_26.jpg' width='712' height='285' alt=''><img id='simVision27' src='amo/simulations/sim_norm_jpg/sim_norm_27.jpg' width='712' height='285' alt=''><img id='simVision28' src='amo/simulations/sim_norm_jpg/sim_norm_28.jpg' width='712' height='285' alt=''><img id='simVision29' src='amo/simulations/sim_norm_jpg/sim_norm_29.jpg' width='712' height='285' alt=''><img id='simVision30' src='amo/simulations/sim_norm_jpg/sim_norm_30.jpg' width='712' height='285' alt=''><img id='simVision31' src='amo/simulations/sim_norm_jpg/sim_norm_31.jpg' width='712' height='285' alt=''><img id='simVision32' src='amo/simulations/sim_norm_jpg/sim_norm_32.jpg' width='712' height='285' alt=''><img id='simVision33' src='amo/simulations/sim_norm_jpg/sim_norm_33.jpg' width='712' height='285' alt=''><img id='simVision34' src='amo/simulations/sim_norm_jpg/sim_norm_34.jpg' width='712' height='285' alt=''><img id='simVision35' src='amo/simulations/sim_norm_jpg/sim_norm_35.jpg' width='712' height='285' alt=''><img id='simVision36' src='amo/simulations/sim_norm_jpg/sim_norm_36.jpg' width='712' height='285' alt=''><img id='simVision37' src='amo/simulations/sim_norm_jpg/sim_norm_37.jpg' width='712' height='285' alt=''><img id='simVision38' src='amo/simulations/sim_norm_jpg/sim_norm_38.jpg' width='712' height='285' alt=''><img id='simVision39' src='amo/simulations/sim_norm_jpg/sim_norm_39.jpg' width='712' height='285' alt=''><img id='simVision40' src='amo/simulations/sim_norm_jpg/sim_norm_40.jpg' width='712' height='285' alt=''><img id='simVision41' src='amo/simulations/sim_norm_jpg/sim_norm_41.jpg' width='712' height='285' alt=''><img id='simVision42' src='amo/simulations/sim_norm_jpg/sim_norm_42.jpg' width='712' height='285' alt=''><img id='simVision43' src='amo/simulations/sim_norm_jpg/sim_norm_43.jpg' width='712' height='285' alt=''><img id='simVision44' src='amo/simulations/sim_norm_jpg/sim_norm_44.jpg' width='712' height='285' alt=''><img id='simVision45' src='amo/simulations/sim_norm_jpg/sim_norm_45.jpg' width='712' height='285' alt=''><img id='simVision46' src='amo/simulations/sim_norm_jpg/sim_norm_46.jpg' width='712' height='285' alt=''><img id='simVision47' src='amo/simulations/sim_norm_jpg/sim_norm_47.jpg' width='712' height='285' alt=''><img id='simVision48' src='amo/simulations/sim_norm_jpg/sim_norm_48.jpg' width='712' height='285' alt=''><img id='simVision49' src='amo/simulations/sim_norm_jpg/sim_norm_49.jpg' width='712' height='285' alt=''><img id='simVision50' src='amo/simulations/sim_norm_jpg/sim_norm_50.jpg' width='712' height='285' alt=''>",
			"hyperopia": "<img id='simVision1' src='amo/simulations/sim_fars_jpg/sim_fars_1.jpg' width='712' height='285' alt='' style='visibility:visible;'><img id='simVision2' src='amo/simulations/sim_fars_jpg/sim_fars_2.jpg' width='712' height='285' alt=''><img id='simVision3' src='amo/simulations/sim_fars_jpg/sim_fars_3.jpg' width='712' height='285' alt=''><img id='simVision4' src='amo/simulations/sim_fars_jpg/sim_fars_4.jpg' width='712' height='285' alt=''><img id='simVision5' src='amo/simulations/sim_fars_jpg/sim_fars_5.jpg' width='712' height='285' alt=''><img id='simVision6' src='amo/simulations/sim_fars_jpg/sim_fars_6.jpg' width='712' height='285' alt=''><img id='simVision7' src='amo/simulations/sim_fars_jpg/sim_fars_7.jpg' width='712' height='285' alt=''><img id='simVision8' src='amo/simulations/sim_fars_jpg/sim_fars_8.jpg' width='712' height='285' alt=''><img id='simVision9' src='amo/simulations/sim_fars_jpg/sim_fars_9.jpg' width='712' height='285' alt=''><img id='simVision10' src='amo/simulations/sim_fars_jpg/sim_fars_10.jpg' width='712' height='285' alt=''><img id='simVision11' src='amo/simulations/sim_fars_jpg/sim_fars_11.jpg' width='712' height='285' alt=''><img id='simVision12' src='amo/simulations/sim_fars_jpg/sim_fars_12.jpg' width='712' height='285' alt=''><img id='simVision13' src='amo/simulations/sim_fars_jpg/sim_fars_13.jpg' width='712' height='285' alt=''><img id='simVision14' src='amo/simulations/sim_fars_jpg/sim_fars_14.jpg' width='712' height='285' alt=''><img id='simVision15' src='amo/simulations/sim_fars_jpg/sim_fars_15.jpg' width='712' height='285' alt=''><img id='simVision16' src='amo/simulations/sim_fars_jpg/sim_fars_16.jpg' width='712' height='285' alt=''><img id='simVision17' src='amo/simulations/sim_fars_jpg/sim_fars_17.jpg' width='712' height='285' alt=''><img id='simVision18' src='amo/simulations/sim_fars_jpg/sim_fars_18.jpg' width='712' height='285' alt=''><img id='simVision19' src='amo/simulations/sim_fars_jpg/sim_fars_19.jpg' width='712' height='285' alt=''><img id='simVision20' src='amo/simulations/sim_fars_jpg/sim_fars_20.jpg' width='712' height='285' alt=''><img id='simVision21' src='amo/simulations/sim_fars_jpg/sim_fars_21.jpg' width='712' height='285' alt=''><img id='simVision22' src='amo/simulations/sim_fars_jpg/sim_fars_22.jpg' width='712' height='285' alt=''><img id='simVision23' src='amo/simulations/sim_fars_jpg/sim_fars_23.jpg' width='712' height='285' alt=''><img id='simVision24' src='amo/simulations/sim_fars_jpg/sim_fars_24.jpg' width='712' height='285' alt=''><img id='simVision25' src='amo/simulations/sim_fars_jpg/sim_fars_25.jpg' width='712' height='285' alt=''><img id='simVision26' src='amo/simulations/sim_fars_jpg/sim_fars_26.jpg' width='712' height='285' alt=''><img id='simVision27' src='amo/simulations/sim_fars_jpg/sim_fars_27.jpg' width='712' height='285' alt=''><img id='simVision28' src='amo/simulations/sim_fars_jpg/sim_fars_28.jpg' width='712' height='285' alt=''><img id='simVision29' src='amo/simulations/sim_fars_jpg/sim_fars_29.jpg' width='712' height='285' alt=''><img id='simVision30' src='amo/simulations/sim_fars_jpg/sim_fars_30.jpg' width='712' height='285' alt=''><img id='simVision31' src='amo/simulations/sim_fars_jpg/sim_fars_31.jpg' width='712' height='285' alt=''><img id='simVision32' src='amo/simulations/sim_fars_jpg/sim_fars_32.jpg' width='712' height='285' alt=''><img id='simVision33' src='amo/simulations/sim_fars_jpg/sim_fars_33.jpg' width='712' height='285' alt=''><img id='simVision34' src='amo/simulations/sim_fars_jpg/sim_fars_34.jpg' width='712' height='285' alt=''><img id='simVision35' src='amo/simulations/sim_fars_jpg/sim_fars_35.jpg' width='712' height='285' alt=''><img id='simVision36' src='amo/simulations/sim_fars_jpg/sim_fars_36.jpg' width='712' height='285' alt=''><img id='simVision37' src='amo/simulations/sim_fars_jpg/sim_fars_37.jpg' width='712' height='285' alt=''><img id='simVision38' src='amo/simulations/sim_fars_jpg/sim_fars_38.jpg' width='712' height='285' alt=''><img id='simVision39' src='amo/simulations/sim_fars_jpg/sim_fars_39.jpg' width='712' height='285' alt=''><img id='simVision40' src='amo/simulations/sim_fars_jpg/sim_fars_40.jpg' width='712' height='285' alt=''><img id='simVision41' src='amo/simulations/sim_fars_jpg/sim_fars_41.jpg' width='712' height='285' alt=''><img id='simVision42' src='amo/simulations/sim_fars_jpg/sim_fars_42.jpg' width='712' height='285' alt=''><img id='simVision43' src='amo/simulations/sim_fars_jpg/sim_fars_43.jpg' width='712' height='285' alt=''><img id='simVision44' src='amo/simulations/sim_fars_jpg/sim_fars_44.jpg' width='712' height='285' alt=''><img id='simVision45' src='amo/simulations/sim_fars_jpg/sim_fars_45.jpg' width='712' height='285' alt=''><img id='simVision46' src='amo/simulations/sim_fars_jpg/sim_fars_46.jpg' width='712' height='285' alt=''><img id='simVision47' src='amo/simulations/sim_fars_jpg/sim_fars_47.jpg' width='712' height='285' alt=''><img id='simVision48' src='amo/simulations/sim_fars_jpg/sim_fars_48.jpg' width='712' height='285' alt=''><img id='simVision49' src='amo/simulations/sim_fars_jpg/sim_fars_49.jpg' width='712' height='285' alt=''><img id='simVision50' src='amo/simulations/sim_fars_jpg/sim_fars_50.jpg' width='712' height='285' alt=''>",
			"myopia": "<img id='simVision1' src='amo/simulations/sim_near_jpg/sim_near_1.jpg' width='712' height='285' alt='' style='visibility:visible;'><img id='simVision2' src='amo/simulations/sim_near_jpg/sim_near_2.jpg' width='712' height='285' alt=''><img id='simVision3' src='amo/simulations/sim_near_jpg/sim_near_3.jpg' width='712' height='285' alt=''><img id='simVision4' src='amo/simulations/sim_near_jpg/sim_near_4.jpg' width='712' height='285' alt=''><img id='simVision5' src='amo/simulations/sim_near_jpg/sim_near_5.jpg' width='712' height='285' alt=''><img id='simVision6' src='amo/simulations/sim_near_jpg/sim_near_6.jpg' width='712' height='285' alt=''><img id='simVision7' src='amo/simulations/sim_near_jpg/sim_near_7.jpg' width='712' height='285' alt=''><img id='simVision8' src='amo/simulations/sim_near_jpg/sim_near_8.jpg' width='712' height='285' alt=''><img id='simVision9' src='amo/simulations/sim_near_jpg/sim_near_9.jpg' width='712' height='285' alt=''><img id='simVision10' src='amo/simulations/sim_near_jpg/sim_near_10.jpg' width='712' height='285' alt=''><img id='simVision11' src='amo/simulations/sim_near_jpg/sim_near_11.jpg' width='712' height='285' alt=''><img id='simVision12' src='amo/simulations/sim_near_jpg/sim_near_12.jpg' width='712' height='285' alt=''><img id='simVision13' src='amo/simulations/sim_near_jpg/sim_near_13.jpg' width='712' height='285' alt=''><img id='simVision14' src='amo/simulations/sim_near_jpg/sim_near_14.jpg' width='712' height='285' alt=''><img id='simVision15' src='amo/simulations/sim_near_jpg/sim_near_15.jpg' width='712' height='285' alt=''><img id='simVision16' src='amo/simulations/sim_near_jpg/sim_near_16.jpg' width='712' height='285' alt=''><img id='simVision17' src='amo/simulations/sim_near_jpg/sim_near_17.jpg' width='712' height='285' alt=''><img id='simVision18' src='amo/simulations/sim_near_jpg/sim_near_18.jpg' width='712' height='285' alt=''><img id='simVision19' src='amo/simulations/sim_near_jpg/sim_near_19.jpg' width='712' height='285' alt=''><img id='simVision20' src='amo/simulations/sim_near_jpg/sim_near_20.jpg' width='712' height='285' alt=''><img id='simVision21' src='amo/simulations/sim_near_jpg/sim_near_21.jpg' width='712' height='285' alt=''><img id='simVision22' src='amo/simulations/sim_near_jpg/sim_near_22.jpg' width='712' height='285' alt=''><img id='simVision23' src='amo/simulations/sim_near_jpg/sim_near_23.jpg' width='712' height='285' alt=''><img id='simVision24' src='amo/simulations/sim_near_jpg/sim_near_24.jpg' width='712' height='285' alt=''><img id='simVision25' src='amo/simulations/sim_near_jpg/sim_near_25.jpg' width='712' height='285' alt=''><img id='simVision26' src='amo/simulations/sim_near_jpg/sim_near_26.jpg' width='712' height='285' alt=''><img id='simVision27' src='amo/simulations/sim_near_jpg/sim_near_27.jpg' width='712' height='285' alt=''><img id='simVision28' src='amo/simulations/sim_near_jpg/sim_near_28.jpg' width='712' height='285' alt=''><img id='simVision29' src='amo/simulations/sim_near_jpg/sim_near_29.jpg' width='712' height='285' alt=''><img id='simVision30' src='amo/simulations/sim_near_jpg/sim_near_30.jpg' width='712' height='285' alt=''><img id='simVision31' src='amo/simulations/sim_near_jpg/sim_near_31.jpg' width='712' height='285' alt=''><img id='simVision32' src='amo/simulations/sim_near_jpg/sim_near_32.jpg' width='712' height='285' alt=''><img id='simVision33' src='amo/simulations/sim_near_jpg/sim_near_33.jpg' width='712' height='285' alt=''><img id='simVision34' src='amo/simulations/sim_near_jpg/sim_near_34.jpg' width='712' height='285' alt=''><img id='simVision35' src='amo/simulations/sim_near_jpg/sim_near_35.jpg' width='712' height='285' alt=''><img id='simVision36' src='amo/simulations/sim_near_jpg/sim_near_36.jpg' width='712' height='285' alt=''><img id='simVision37' src='amo/simulations/sim_near_jpg/sim_near_37.jpg' width='712' height='285' alt=''><img id='simVision38' src='amo/simulations/sim_near_jpg/sim_near_38.jpg' width='712' height='285' alt=''><img id='simVision39' src='amo/simulations/sim_near_jpg/sim_near_39.jpg' width='712' height='285' alt=''><img id='simVision40' src='amo/simulations/sim_near_jpg/sim_near_40.jpg' width='712' height='285' alt=''><img id='simVision41' src='amo/simulations/sim_near_jpg/sim_near_41.jpg' width='712' height='285' alt=''><img id='simVision42' src='amo/simulations/sim_near_jpg/sim_near_42.jpg' width='712' height='285' alt=''><img id='simVision43' src='amo/simulations/sim_near_jpg/sim_near_43.jpg' width='712' height='285' alt=''><img id='simVision44' src='amo/simulations/sim_near_jpg/sim_near_44.jpg' width='712' height='285' alt=''><img id='simVision45' src='amo/simulations/sim_near_jpg/sim_near_45.jpg' width='712' height='285' alt=''><img id='simVision46' src='amo/simulations/sim_near_jpg/sim_near_46.jpg' width='712' height='285' alt=''><img id='simVision47' src='amo/simulations/sim_near_jpg/sim_near_47.jpg' width='712' height='285' alt=''><img id='simVision48' src='amo/simulations/sim_near_jpg/sim_near_48.jpg' width='712' height='285' alt=''><img id='simVision49' src='amo/simulations/sim_near_jpg/sim_near_49.jpg' width='712' height='285' alt=''><img id='simVision50' src='amo/simulations/sim_near_jpg/sim_near_50.jpg' width='712' height='285' alt=''>",
			"astigmatism":"<img id='simVision1' src='amo/simulations/sim_astg_jpg/sim_astg_1.jpg' width='712' height='285' alt='' style='visibility:visible;'><img id='simVision2' src='amo/simulations/sim_astg_jpg/sim_astg_2.jpg' width='712' height='285' alt=''><img id='simVision3' src='amo/simulations/sim_astg_jpg/sim_astg_3.jpg' width='712' height='285' alt=''><img id='simVision4' src='amo/simulations/sim_astg_jpg/sim_astg_4.jpg' width='712' height='285' alt=''><img id='simVision5' src='amo/simulations/sim_astg_jpg/sim_astg_5.jpg' width='712' height='285' alt=''><img id='simVision6' src='amo/simulations/sim_astg_jpg/sim_astg_6.jpg' width='712' height='285' alt=''><img id='simVision7' src='amo/simulations/sim_astg_jpg/sim_astg_7.jpg' width='712' height='285' alt=''><img id='simVision8' src='amo/simulations/sim_astg_jpg/sim_astg_8.jpg' width='712' height='285' alt=''><img id='simVision9' src='amo/simulations/sim_astg_jpg/sim_astg_9.jpg' width='712' height='285' alt=''><img id='simVision10' src='amo/simulations/sim_astg_jpg/sim_astg_10.jpg' width='712' height='285' alt=''><img id='simVision11' src='amo/simulations/sim_astg_jpg/sim_astg_11.jpg' width='712' height='285' alt=''><img id='simVision12' src='amo/simulations/sim_astg_jpg/sim_astg_12.jpg' width='712' height='285' alt=''><img id='simVision13' src='amo/simulations/sim_astg_jpg/sim_astg_13.jpg' width='712' height='285' alt=''><img id='simVision14' src='amo/simulations/sim_astg_jpg/sim_astg_14.jpg' width='712' height='285' alt=''><img id='simVision15' src='amo/simulations/sim_astg_jpg/sim_astg_15.jpg' width='712' height='285' alt=''><img id='simVision16' src='amo/simulations/sim_astg_jpg/sim_astg_16.jpg' width='712' height='285' alt=''><img id='simVision17' src='amo/simulations/sim_astg_jpg/sim_astg_17.jpg' width='712' height='285' alt=''><img id='simVision18' src='amo/simulations/sim_astg_jpg/sim_astg_18.jpg' width='712' height='285' alt=''><img id='simVision19' src='amo/simulations/sim_astg_jpg/sim_astg_19.jpg' width='712' height='285' alt=''><img id='simVision20' src='amo/simulations/sim_astg_jpg/sim_astg_20.jpg' width='712' height='285' alt=''><img id='simVision21' src='amo/simulations/sim_astg_jpg/sim_astg_21.jpg' width='712' height='285' alt=''><img id='simVision22' src='amo/simulations/sim_astg_jpg/sim_astg_22.jpg' width='712' height='285' alt=''><img id='simVision23' src='amo/simulations/sim_astg_jpg/sim_astg_23.jpg' width='712' height='285' alt=''><img id='simVision24' src='amo/simulations/sim_astg_jpg/sim_astg_24.jpg' width='712' height='285' alt=''><img id='simVision25' src='amo/simulations/sim_astg_jpg/sim_astg_25.jpg' width='712' height='285' alt=''><img id='simVision26' src='amo/simulations/sim_astg_jpg/sim_astg_26.jpg' width='712' height='285' alt=''><img id='simVision27' src='amo/simulations/sim_astg_jpg/sim_astg_27.jpg' width='712' height='285' alt=''><img id='simVision28' src='amo/simulations/sim_astg_jpg/sim_astg_28.jpg' width='712' height='285' alt=''><img id='simVision29' src='amo/simulations/sim_astg_jpg/sim_astg_29.jpg' width='712' height='285' alt=''><img id='simVision30' src='amo/simulations/sim_astg_jpg/sim_astg_30.jpg' width='712' height='285' alt=''><img id='simVision31' src='amo/simulations/sim_astg_jpg/sim_astg_31.jpg' width='712' height='285' alt=''><img id='simVision32' src='amo/simulations/sim_astg_jpg/sim_astg_32.jpg' width='712' height='285' alt=''><img id='simVision33' src='amo/simulations/sim_astg_jpg/sim_astg_33.jpg' width='712' height='285' alt=''><img id='simVision34' src='amo/simulations/sim_astg_jpg/sim_astg_34.jpg' width='712' height='285' alt=''><img id='simVision35' src='amo/simulations/sim_astg_jpg/sim_astg_35.jpg' width='712' height='285' alt=''><img id='simVision36' src='amo/simulations/sim_astg_jpg/sim_astg_36.jpg' width='712' height='285' alt=''><img id='simVision37' src='amo/simulations/sim_astg_jpg/sim_astg_37.jpg' width='712' height='285' alt=''><img id='simVision38' src='amo/simulations/sim_astg_jpg/sim_astg_38.jpg' width='712' height='285' alt=''><img id='simVision39' src='amo/simulations/sim_astg_jpg/sim_astg_39.jpg' width='712' height='285' alt=''><img id='simVision40' src='amo/simulations/sim_astg_jpg/sim_astg_40.jpg' width='712' height='285' alt=''><img id='simVision41' src='amo/simulations/sim_astg_jpg/sim_astg_41.jpg' width='712' height='285' alt=''><img id='simVision42' src='amo/simulations/sim_astg_jpg/sim_astg_42.jpg' width='712' height='285' alt=''><img id='simVision43' src='amo/simulations/sim_astg_jpg/sim_astg_43.jpg' width='712' height='285' alt=''><img id='simVision44' src='amo/simulations/sim_astg_jpg/sim_astg_44.jpg' width='712' height='285' alt=''><img id='simVision45' src='amo/simulations/sim_astg_jpg/sim_astg_45.jpg' width='712' height='285' alt=''><img id='simVision46' src='amo/simulations/sim_astg_jpg/sim_astg_46.jpg' width='712' height='285' alt=''><img id='simVision47' src='amo/simulations/sim_astg_jpg/sim_astg_47.jpg' width='712' height='285' alt=''><img id='simVision48' src='amo/simulations/sim_astg_jpg/sim_astg_48.jpg' width='712' height='285' alt=''><img id='simVision49' src='amo/simulations/sim_astg_jpg/sim_astg_49.jpg' width='712' height='285' alt=''><img id='simVision50' src='amo/simulations/sim_astg_jpg/sim_astg_50.jpg' width='712' height='285' alt=''>",
			"presbyopia": "<img id='simVision1' src='amo/simulations/sim_pres_jpg/sim_pres_1.jpg' width='712' height='285' alt='' style='visibility:visible;'><img id='simVision2' src='amo/simulations/sim_pres_jpg/sim_pres_2.jpg' width='712' height='285' alt=''><img id='simVision3' src='amo/simulations/sim_pres_jpg/sim_pres_3.jpg' width='712' height='285' alt=''><img id='simVision4' src='amo/simulations/sim_pres_jpg/sim_pres_4.jpg' width='712' height='285' alt=''><img id='simVision5' src='amo/simulations/sim_pres_jpg/sim_pres_5.jpg' width='712' height='285' alt=''><img id='simVision6' src='amo/simulations/sim_pres_jpg/sim_pres_6.jpg' width='712' height='285' alt=''><img id='simVision7' src='amo/simulations/sim_pres_jpg/sim_pres_7.jpg' width='712' height='285' alt=''><img id='simVision8' src='amo/simulations/sim_pres_jpg/sim_pres_8.jpg' width='712' height='285' alt=''><img id='simVision9' src='amo/simulations/sim_pres_jpg/sim_pres_9.jpg' width='712' height='285' alt=''><img id='simVision10' src='amo/simulations/sim_pres_jpg/sim_pres_10.jpg' width='712' height='285' alt=''><img id='simVision11' src='amo/simulations/sim_pres_jpg/sim_pres_11.jpg' width='712' height='285' alt=''><img id='simVision12' src='amo/simulations/sim_pres_jpg/sim_pres_12.jpg' width='712' height='285' alt=''><img id='simVision13' src='amo/simulations/sim_pres_jpg/sim_pres_13.jpg' width='712' height='285' alt=''><img id='simVision14' src='amo/simulations/sim_pres_jpg/sim_pres_14.jpg' width='712' height='285' alt=''><img id='simVision15' src='amo/simulations/sim_pres_jpg/sim_pres_15.jpg' width='712' height='285' alt=''><img id='simVision16' src='amo/simulations/sim_pres_jpg/sim_pres_16.jpg' width='712' height='285' alt=''><img id='simVision17' src='amo/simulations/sim_pres_jpg/sim_pres_17.jpg' width='712' height='285' alt=''><img id='simVision18' src='amo/simulations/sim_pres_jpg/sim_pres_18.jpg' width='712' height='285' alt=''><img id='simVision19' src='amo/simulations/sim_pres_jpg/sim_pres_19.jpg' width='712' height='285' alt=''><img id='simVision20' src='amo/simulations/sim_pres_jpg/sim_pres_20.jpg' width='712' height='285' alt=''><img id='simVision21' src='amo/simulations/sim_pres_jpg/sim_pres_21.jpg' width='712' height='285' alt=''><img id='simVision22' src='amo/simulations/sim_pres_jpg/sim_pres_22.jpg' width='712' height='285' alt=''><img id='simVision23' src='amo/simulations/sim_pres_jpg/sim_pres_23.jpg' width='712' height='285' alt=''><img id='simVision24' src='amo/simulations/sim_pres_jpg/sim_pres_24.jpg' width='712' height='285' alt=''><img id='simVision25' src='amo/simulations/sim_pres_jpg/sim_pres_25.jpg' width='712' height='285' alt=''><img id='simVision26' src='amo/simulations/sim_pres_jpg/sim_pres_26.jpg' width='712' height='285' alt=''><img id='simVision27' src='amo/simulations/sim_pres_jpg/sim_pres_27.jpg' width='712' height='285' alt=''><img id='simVision28' src='amo/simulations/sim_pres_jpg/sim_pres_28.jpg' width='712' height='285' alt=''><img id='simVision29' src='amo/simulations/sim_pres_jpg/sim_pres_29.jpg' width='712' height='285' alt=''><img id='simVision30' src='amo/simulations/sim_pres_jpg/sim_pres_30.jpg' width='712' height='285' alt=''><img id='simVision31' src='amo/simulations/sim_pres_jpg/sim_pres_31.jpg' width='712' height='285' alt=''><img id='simVision32' src='amo/simulations/sim_pres_jpg/sim_pres_32.jpg' width='712' height='285' alt=''><img id='simVision33' src='amo/simulations/sim_pres_jpg/sim_pres_33.jpg' width='712' height='285' alt=''><img id='simVision34' src='amo/simulations/sim_pres_jpg/sim_pres_34.jpg' width='712' height='285' alt=''><img id='simVision35' src='amo/simulations/sim_pres_jpg/sim_pres_35.jpg' width='712' height='285' alt=''><img id='simVision36' src='amo/simulations/sim_pres_jpg/sim_pres_36.jpg' width='712' height='285' alt=''><img id='simVision37' src='amo/simulations/sim_pres_jpg/sim_pres_37.jpg' width='712' height='285' alt=''><img id='simVision38' src='amo/simulations/sim_pres_jpg/sim_pres_38.jpg' width='712' height='285' alt=''><img id='simVision39' src='amo/simulations/sim_pres_jpg/sim_pres_39.jpg' width='712' height='285' alt=''><img id='simVision40' src='amo/simulations/sim_pres_jpg/sim_pres_40.jpg' width='712' height='285' alt=''><img id='simVision41' src='amo/simulations/sim_pres_jpg/sim_pres_41.jpg' width='712' height='285' alt=''><img id='simVision42' src='amo/simulations/sim_pres_jpg/sim_pres_42.jpg' width='712' height='285' alt=''><img id='simVision43' src='amo/simulations/sim_pres_jpg/sim_pres_43.jpg' width='712' height='285' alt=''><img id='simVision44' src='amo/simulations/sim_pres_jpg/sim_pres_44.jpg' width='712' height='285' alt=''><img id='simVision45' src='amo/simulations/sim_pres_jpg/sim_pres_45.jpg' width='712' height='285' alt=''><img id='simVision46' src='amo/simulations/sim_pres_jpg/sim_pres_46.jpg' width='712' height='285' alt=''><img id='simVision47' src='amo/simulations/sim_pres_jpg/sim_pres_47.jpg' width='712' height='285' alt=''><img id='simVision48' src='amo/simulations/sim_pres_jpg/sim_pres_48.jpg' width='712' height='285' alt=''><img id='simVision49' src='amo/simulations/sim_pres_jpg/sim_pres_49.jpg' width='712' height='285' alt=''><img id='simVision50' src='amo/simulations/sim_pres_jpg/sim_pres_50.jpg' width='712' height='285' alt=''>",
			"cataract": "<img id='simVision1' src='amo/simulations/sim_cata_jpg/sim_cata_1.jpg' width='712' height='285' alt='' style='visibility:visible;'><img id='simVision2' src='amo/simulations/sim_cata_jpg/sim_cata_2.jpg' width='712' height='285' alt=''><img id='simVision3' src='amo/simulations/sim_cata_jpg/sim_cata_3.jpg' width='712' height='285' alt=''><img id='simVision4' src='amo/simulations/sim_cata_jpg/sim_cata_4.jpg' width='712' height='285' alt=''><img id='simVision5' src='amo/simulations/sim_cata_jpg/sim_cata_5.jpg' width='712' height='285' alt=''><img id='simVision6' src='amo/simulations/sim_cata_jpg/sim_cata_6.jpg' width='712' height='285' alt=''><img id='simVision7' src='amo/simulations/sim_cata_jpg/sim_cata_7.jpg' width='712' height='285' alt=''><img id='simVision8' src='amo/simulations/sim_cata_jpg/sim_cata_8.jpg' width='712' height='285' alt=''><img id='simVision9' src='amo/simulations/sim_cata_jpg/sim_cata_9.jpg' width='712' height='285' alt=''><img id='simVision10' src='amo/simulations/sim_cata_jpg/sim_cata_10.jpg' width='712' height='285' alt=''><img id='simVision11' src='amo/simulations/sim_cata_jpg/sim_cata_11.jpg' width='712' height='285' alt=''><img id='simVision12' src='amo/simulations/sim_cata_jpg/sim_cata_12.jpg' width='712' height='285' alt=''><img id='simVision13' src='amo/simulations/sim_cata_jpg/sim_cata_13.jpg' width='712' height='285' alt=''><img id='simVision14' src='amo/simulations/sim_cata_jpg/sim_cata_14.jpg' width='712' height='285' alt=''><img id='simVision15' src='amo/simulations/sim_cata_jpg/sim_cata_15.jpg' width='712' height='285' alt=''><img id='simVision16' src='amo/simulations/sim_cata_jpg/sim_cata_16.jpg' width='712' height='285' alt=''><img id='simVision17' src='amo/simulations/sim_cata_jpg/sim_cata_17.jpg' width='712' height='285' alt=''><img id='simVision18' src='amo/simulations/sim_cata_jpg/sim_cata_18.jpg' width='712' height='285' alt=''><img id='simVision19' src='amo/simulations/sim_cata_jpg/sim_cata_19.jpg' width='712' height='285' alt=''><img id='simVision20' src='amo/simulations/sim_cata_jpg/sim_cata_20.jpg' width='712' height='285' alt=''><img id='simVision21' src='amo/simulations/sim_cata_jpg/sim_cata_21.jpg' width='712' height='285' alt=''><img id='simVision22' src='amo/simulations/sim_cata_jpg/sim_cata_22.jpg' width='712' height='285' alt=''><img id='simVision23' src='amo/simulations/sim_cata_jpg/sim_cata_23.jpg' width='712' height='285' alt=''><img id='simVision24' src='amo/simulations/sim_cata_jpg/sim_cata_24.jpg' width='712' height='285' alt=''><img id='simVision25' src='amo/simulations/sim_cata_jpg/sim_cata_25.jpg' width='712' height='285' alt=''><img id='simVision26' src='amo/simulations/sim_cata_jpg/sim_cata_26.jpg' width='712' height='285' alt=''><img id='simVision27' src='amo/simulations/sim_cata_jpg/sim_cata_27.jpg' width='712' height='285' alt=''><img id='simVision28' src='amo/simulations/sim_cata_jpg/sim_cata_28.jpg' width='712' height='285' alt=''><img id='simVision29' src='amo/simulations/sim_cata_jpg/sim_cata_29.jpg' width='712' height='285' alt=''><img id='simVision30' src='amo/simulations/sim_cata_jpg/sim_cata_30.jpg' width='712' height='285' alt=''><img id='simVision31' src='amo/simulations/sim_cata_jpg/sim_cata_31.jpg' width='712' height='285' alt=''><img id='simVision32' src='amo/simulations/sim_cata_jpg/sim_cata_32.jpg' width='712' height='285' alt=''><img id='simVision33' src='amo/simulations/sim_cata_jpg/sim_cata_33.jpg' width='712' height='285' alt=''><img id='simVision34' src='amo/simulations/sim_cata_jpg/sim_cata_34.jpg' width='712' height='285' alt=''><img id='simVision35' src='amo/simulations/sim_cata_jpg/sim_cata_35.jpg' width='712' height='285' alt=''><img id='simVision36' src='amo/simulations/sim_cata_jpg/sim_cata_36.jpg' width='712' height='285' alt=''><img id='simVision37' src='amo/simulations/sim_cata_jpg/sim_cata_37.jpg' width='712' height='285' alt=''><img id='simVision38' src='amo/simulations/sim_cata_jpg/sim_cata_38.jpg' width='712' height='285' alt=''><img id='simVision39' src='amo/simulations/sim_cata_jpg/sim_cata_39.jpg' width='712' height='285' alt=''><img id='simVision40' src='amo/simulations/sim_cata_jpg/sim_cata_40.jpg' width='712' height='285' alt=''><img id='simVision41' src='amo/simulations/sim_cata_jpg/sim_cata_41.jpg' width='712' height='285' alt=''><img id='simVision42' src='amo/simulations/sim_cata_jpg/sim_cata_42.jpg' width='712' height='285' alt=''><img id='simVision43' src='amo/simulations/sim_cata_jpg/sim_cata_43.jpg' width='712' height='285' alt=''><img id='simVision44' src='amo/simulations/sim_cata_jpg/sim_cata_44.jpg' width='712' height='285' alt=''><img id='simVision45' src='amo/simulations/sim_cata_jpg/sim_cata_45.jpg' width='712' height='285' alt=''><img id='simVision46' src='amo/simulations/sim_cata_jpg/sim_cata_46.jpg' width='712' height='285' alt=''><img id='simVision47' src='amo/simulations/sim_cata_jpg/sim_cata_47.jpg' width='712' height='285' alt=''><img id='simVision48' src='amo/simulations/sim_cata_jpg/sim_cata_48.jpg' width='712' height='285' alt=''><img id='simVision49' src='amo/simulations/sim_cata_jpg/sim_cata_49.jpg' width='712' height='285' alt=''><img id='simVision50' src='amo/simulations/sim_cata_jpg/sim_cata_50.jpg' width='712' height='285' alt=''>",
			"cataractsurgery": "<img id='simVision1' src='amo/simulations/sim_surg_jpg/sim_surg_1.jpg' width='712' height='285' alt='' style='visibility:visible;'><img id='simVision2' src='amo/simulations/sim_surg_jpg/sim_surg_2.jpg' width='712' height='285' alt=''><img id='simVision3' src='amo/simulations/sim_surg_jpg/sim_surg_3.jpg' width='712' height='285' alt=''><img id='simVision4' src='amo/simulations/sim_surg_jpg/sim_surg_4.jpg' width='712' height='285' alt=''><img id='simVision5' src='amo/simulations/sim_surg_jpg/sim_surg_5.jpg' width='712' height='285' alt=''><img id='simVision6' src='amo/simulations/sim_surg_jpg/sim_surg_6.jpg' width='712' height='285' alt=''><img id='simVision7' src='amo/simulations/sim_surg_jpg/sim_surg_7.jpg' width='712' height='285' alt=''><img id='simVision8' src='amo/simulations/sim_surg_jpg/sim_surg_8.jpg' width='712' height='285' alt=''><img id='simVision9' src='amo/simulations/sim_surg_jpg/sim_surg_9.jpg' width='712' height='285' alt=''><img id='simVision10' src='amo/simulations/sim_surg_jpg/sim_surg_10.jpg' width='712' height='285' alt=''><img id='simVision11' src='amo/simulations/sim_surg_jpg/sim_surg_11.jpg' width='712' height='285' alt=''><img id='simVision12' src='amo/simulations/sim_surg_jpg/sim_surg_12.jpg' width='712' height='285' alt=''><img id='simVision13' src='amo/simulations/sim_surg_jpg/sim_surg_13.jpg' width='712' height='285' alt=''><img id='simVision14' src='amo/simulations/sim_surg_jpg/sim_surg_14.jpg' width='712' height='285' alt=''><img id='simVision15' src='amo/simulations/sim_surg_jpg/sim_surg_15.jpg' width='712' height='285' alt=''><img id='simVision16' src='amo/simulations/sim_surg_jpg/sim_surg_16.jpg' width='712' height='285' alt=''><img id='simVision17' src='amo/simulations/sim_surg_jpg/sim_surg_17.jpg' width='712' height='285' alt=''><img id='simVision18' src='amo/simulations/sim_surg_jpg/sim_surg_18.jpg' width='712' height='285' alt=''><img id='simVision19' src='amo/simulations/sim_surg_jpg/sim_surg_19.jpg' width='712' height='285' alt=''><img id='simVision20' src='amo/simulations/sim_surg_jpg/sim_surg_20.jpg' width='712' height='285' alt=''><img id='simVision21' src='amo/simulations/sim_surg_jpg/sim_surg_21.jpg' width='712' height='285' alt=''><img id='simVision22' src='amo/simulations/sim_surg_jpg/sim_surg_22.jpg' width='712' height='285' alt=''><img id='simVision23' src='amo/simulations/sim_surg_jpg/sim_surg_23.jpg' width='712' height='285' alt=''><img id='simVision24' src='amo/simulations/sim_surg_jpg/sim_surg_24.jpg' width='712' height='285' alt=''><img id='simVision25' src='amo/simulations/sim_surg_jpg/sim_surg_25.jpg' width='712' height='285' alt=''><img id='simVision26' src='amo/simulations/sim_surg_jpg/sim_surg_26.jpg' width='712' height='285' alt=''><img id='simVision27' src='amo/simulations/sim_surg_jpg/sim_surg_27.jpg' width='712' height='285' alt=''><img id='simVision28' src='amo/simulations/sim_surg_jpg/sim_surg_28.jpg' width='712' height='285' alt=''><img id='simVision29' src='amo/simulations/sim_surg_jpg/sim_surg_29.jpg' width='712' height='285' alt=''><img id='simVision30' src='amo/simulations/sim_surg_jpg/sim_surg_30.jpg' width='712' height='285' alt=''><img id='simVision31' src='amo/simulations/sim_surg_jpg/sim_surg_31.jpg' width='712' height='285' alt=''><img id='simVision32' src='amo/simulations/sim_surg_jpg/sim_surg_32.jpg' width='712' height='285' alt=''><img id='simVision33' src='amo/simulations/sim_surg_jpg/sim_surg_33.jpg' width='712' height='285' alt=''><img id='simVision34' src='amo/simulations/sim_surg_jpg/sim_surg_34.jpg' width='712' height='285' alt=''><img id='simVision35' src='amo/simulations/sim_surg_jpg/sim_surg_35.jpg' width='712' height='285' alt=''><img id='simVision36' src='amo/simulations/sim_surg_jpg/sim_surg_36.jpg' width='712' height='285' alt=''><img id='simVision37' src='amo/simulations/sim_surg_jpg/sim_surg_37.jpg' width='712' height='285' alt=''><img id='simVision38' src='amo/simulations/sim_surg_jpg/sim_surg_38.jpg' width='712' height='285' alt=''><img id='simVision39' src='amo/simulations/sim_surg_jpg/sim_surg_39.jpg' width='712' height='285' alt=''><img id='simVision40' src='amo/simulations/sim_surg_jpg/sim_surg_40.jpg' width='712' height='285' alt=''><img id='simVision41' src='amo/simulations/sim_surg_jpg/sim_surg_41.jpg' width='712' height='285' alt=''><img id='simVision42' src='amo/simulations/sim_surg_jpg/sim_surg_42.jpg' width='712' height='285' alt=''><img id='simVision43' src='amo/simulations/sim_surg_jpg/sim_surg_43.jpg' width='712' height='285' alt=''><img id='simVision44' src='amo/simulations/sim_surg_jpg/sim_surg_44.jpg' width='712' height='285' alt=''><img id='simVision45' src='amo/simulations/sim_surg_jpg/sim_surg_45.jpg' width='712' height='285' alt=''><img id='simVision46' src='amo/simulations/sim_surg_jpg/sim_surg_46.jpg' width='712' height='285' alt=''><img id='simVision47' src='amo/simulations/sim_surg_jpg/sim_surg_47.jpg' width='712' height='285' alt=''><img id='simVision48' src='amo/simulations/sim_surg_jpg/sim_surg_48.jpg' width='712' height='285' alt=''><img id='simVision49' src='amo/simulations/sim_surg_jpg/sim_surg_49.jpg' width='712' height='285' alt=''><img id='simVision50' src='amo/simulations/sim_surg_jpg/sim_surg_50.jpg' width='712' height='285' alt=''>",
			"monolens": "<img id='simLens1' src='amo/simulations/sim_mono_jpg/sim_mono_1.jpg' width='712' height='285' alt='' style='visibility:visible;'><img id='simLens2' src='amo/simulations/sim_mono_jpg/sim_mono_2.jpg' width='712' height='285' alt=''><img id='simLens3' src='amo/simulations/sim_mono_jpg/sim_mono_3.jpg' width='712' height='285' alt=''><img id='simLens4' src='amo/simulations/sim_mono_jpg/sim_mono_4.jpg' width='712' height='285' alt=''><img id='simLens5' src='amo/simulations/sim_mono_jpg/sim_mono_5.jpg' width='712' height='285' alt=''><img id='simLens6' src='amo/simulations/sim_mono_jpg/sim_mono_6.jpg' width='712' height='285' alt=''><img id='simLens7' src='amo/simulations/sim_mono_jpg/sim_mono_7.jpg' width='712' height='285' alt=''><img id='simLens8' src='amo/simulations/sim_mono_jpg/sim_mono_8.jpg' width='712' height='285' alt=''><img id='simLens9' src='amo/simulations/sim_mono_jpg/sim_mono_9.jpg' width='712' height='285' alt=''><img id='simLens10' src='amo/simulations/sim_mono_jpg/sim_mono_10.jpg' width='712' height='285' alt=''><img id='simLens11' src='amo/simulations/sim_mono_jpg/sim_mono_11.jpg' width='712' height='285' alt=''><img id='simLens12' src='amo/simulations/sim_mono_jpg/sim_mono_12.jpg' width='712' height='285' alt=''><img id='simLens13' src='amo/simulations/sim_mono_jpg/sim_mono_13.jpg' width='712' height='285' alt=''><img id='simLens14' src='amo/simulations/sim_mono_jpg/sim_mono_14.jpg' width='712' height='285' alt=''><img id='simLens15' src='amo/simulations/sim_mono_jpg/sim_mono_15.jpg' width='712' height='285' alt=''><img id='simLens16' src='amo/simulations/sim_mono_jpg/sim_mono_16.jpg' width='712' height='285' alt=''><img id='simLens17' src='amo/simulations/sim_mono_jpg/sim_mono_17.jpg' width='712' height='285' alt=''><img id='simLens18' src='amo/simulations/sim_mono_jpg/sim_mono_18.jpg' width='712' height='285' alt=''><img id='simLens19' src='amo/simulations/sim_mono_jpg/sim_mono_19.jpg' width='712' height='285' alt=''><img id='simLens20' src='amo/simulations/sim_mono_jpg/sim_mono_20.jpg' width='712' height='285' alt=''><img id='simLens21' src='amo/simulations/sim_mono_jpg/sim_mono_21.jpg' width='712' height='285' alt=''><img id='simLens22' src='amo/simulations/sim_mono_jpg/sim_mono_22.jpg' width='712' height='285' alt=''><img id='simLens23' src='amo/simulations/sim_mono_jpg/sim_mono_23.jpg' width='712' height='285' alt=''><img id='simLens24' src='amo/simulations/sim_mono_jpg/sim_mono_24.jpg' width='712' height='285' alt=''><img id='simLens25' src='amo/simulations/sim_mono_jpg/sim_mono_25.jpg' width='712' height='285' alt=''><img id='simLens26' src='amo/simulations/sim_mono_jpg/sim_mono_26.jpg' width='712' height='285' alt=''><img id='simLens27' src='amo/simulations/sim_mono_jpg/sim_mono_27.jpg' width='712' height='285' alt=''><img id='simLens28' src='amo/simulations/sim_mono_jpg/sim_mono_28.jpg' width='712' height='285' alt=''><img id='simLens29' src='amo/simulations/sim_mono_jpg/sim_mono_29.jpg' width='712' height='285' alt=''><img id='simLens30' src='amo/simulations/sim_mono_jpg/sim_mono_30.jpg' width='712' height='285' alt=''><img id='simLens31' src='amo/simulations/sim_mono_jpg/sim_mono_31.jpg' width='712' height='285' alt=''><img id='simLens32' src='amo/simulations/sim_mono_jpg/sim_mono_32.jpg' width='712' height='285' alt=''><img id='simLens33' src='amo/simulations/sim_mono_jpg/sim_mono_33.jpg' width='712' height='285' alt=''><img id='simLens34' src='amo/simulations/sim_mono_jpg/sim_mono_34.jpg' width='712' height='285' alt=''><img id='simLens35' src='amo/simulations/sim_mono_jpg/sim_mono_35.jpg' width='712' height='285' alt=''><img id='simLens36' src='amo/simulations/sim_mono_jpg/sim_mono_36.jpg' width='712' height='285' alt=''><img id='simLens37' src='amo/simulations/sim_mono_jpg/sim_mono_37.jpg' width='712' height='285' alt=''><img id='simLens38' src='amo/simulations/sim_mono_jpg/sim_mono_38.jpg' width='712' height='285' alt=''><img id='simLens39' src='amo/simulations/sim_mono_jpg/sim_mono_39.jpg' width='712' height='285' alt=''><img id='simLens40' src='amo/simulations/sim_mono_jpg/sim_mono_40.jpg' width='712' height='285' alt=''><img id='simLens41' src='amo/simulations/sim_mono_jpg/sim_mono_41.jpg' width='712' height='285' alt=''><img id='simLens42' src='amo/simulations/sim_mono_jpg/sim_mono_42.jpg' width='712' height='285' alt=''><img id='simLens43' src='amo/simulations/sim_mono_jpg/sim_mono_43.jpg' width='712' height='285' alt=''><img id='simLens44' src='amo/simulations/sim_mono_jpg/sim_mono_44.jpg' width='712' height='285' alt=''><img id='simLens45' src='amo/simulations/sim_mono_jpg/sim_mono_45.jpg' width='712' height='285' alt=''><img id='simLens46' src='amo/simulations/sim_mono_jpg/sim_mono_46.jpg' width='712' height='285' alt=''><img id='simLens47' src='amo/simulations/sim_mono_jpg/sim_mono_47.jpg' width='712' height='285' alt=''><img id='simLens48' src='amo/simulations/sim_mono_jpg/sim_mono_48.jpg' width='712' height='285' alt=''><img id='simLens49' src='amo/simulations/sim_mono_jpg/sim_mono_49.jpg' width='712' height='285' alt=''><img id='simLens50' src='amo/simulations/sim_mono_jpg/sim_mono_50.jpg' width='712' height='285' alt=''>",
			"multilens": "<img id='simLens1' src='amo/simulations/sim_mult_jpg/sim_mult_1.jpg' width='712' height='285' alt='' style='visibility:visible;'><img id='simLens2' src='amo/simulations/sim_mult_jpg/sim_mult_2.jpg' width='712' height='285' alt=''><img id='simLens3' src='amo/simulations/sim_mult_jpg/sim_mult_3.jpg' width='712' height='285' alt=''><img id='simLens4' src='amo/simulations/sim_mult_jpg/sim_mult_4.jpg' width='712' height='285' alt=''><img id='simLens5' src='amo/simulations/sim_mult_jpg/sim_mult_5.jpg' width='712' height='285' alt=''><img id='simLens6' src='amo/simulations/sim_mult_jpg/sim_mult_6.jpg' width='712' height='285' alt=''><img id='simLens7' src='amo/simulations/sim_mult_jpg/sim_mult_7.jpg' width='712' height='285' alt=''><img id='simLens8' src='amo/simulations/sim_mult_jpg/sim_mult_8.jpg' width='712' height='285' alt=''><img id='simLens9' src='amo/simulations/sim_mult_jpg/sim_mult_9.jpg' width='712' height='285' alt=''><img id='simLens10' src='amo/simulations/sim_mult_jpg/sim_mult_10.jpg' width='712' height='285' alt=''><img id='simLens11' src='amo/simulations/sim_mult_jpg/sim_mult_11.jpg' width='712' height='285' alt=''><img id='simLens12' src='amo/simulations/sim_mult_jpg/sim_mult_12.jpg' width='712' height='285' alt=''><img id='simLens13' src='amo/simulations/sim_mult_jpg/sim_mult_13.jpg' width='712' height='285' alt=''><img id='simLens14' src='amo/simulations/sim_mult_jpg/sim_mult_14.jpg' width='712' height='285' alt=''><img id='simLens15' src='amo/simulations/sim_mult_jpg/sim_mult_15.jpg' width='712' height='285' alt=''><img id='simLens16' src='amo/simulations/sim_mult_jpg/sim_mult_16.jpg' width='712' height='285' alt=''><img id='simLens17' src='amo/simulations/sim_mult_jpg/sim_mult_17.jpg' width='712' height='285' alt=''><img id='simLens18' src='amo/simulations/sim_mult_jpg/sim_mult_18.jpg' width='712' height='285' alt=''><img id='simLens19' src='amo/simulations/sim_mult_jpg/sim_mult_19.jpg' width='712' height='285' alt=''><img id='simLens20' src='amo/simulations/sim_mult_jpg/sim_mult_20.jpg' width='712' height='285' alt=''><img id='simLens21' src='amo/simulations/sim_mult_jpg/sim_mult_21.jpg' width='712' height='285' alt=''><img id='simLens22' src='amo/simulations/sim_mult_jpg/sim_mult_22.jpg' width='712' height='285' alt=''><img id='simLens23' src='amo/simulations/sim_mult_jpg/sim_mult_23.jpg' width='712' height='285' alt=''><img id='simLens24' src='amo/simulations/sim_mult_jpg/sim_mult_24.jpg' width='712' height='285' alt=''><img id='simLens25' src='amo/simulations/sim_mult_jpg/sim_mult_25.jpg' width='712' height='285' alt=''><img id='simLens26' src='amo/simulations/sim_mult_jpg/sim_mult_26.jpg' width='712' height='285' alt=''><img id='simLens27' src='amo/simulations/sim_mult_jpg/sim_mult_27.jpg' width='712' height='285' alt=''><img id='simLens28' src='amo/simulations/sim_mult_jpg/sim_mult_28.jpg' width='712' height='285' alt=''><img id='simLens29' src='amo/simulations/sim_mult_jpg/sim_mult_29.jpg' width='712' height='285' alt=''><img id='simLens30' src='amo/simulations/sim_mult_jpg/sim_mult_30.jpg' width='712' height='285' alt=''><img id='simLens31' src='amo/simulations/sim_mult_jpg/sim_mult_31.jpg' width='712' height='285' alt=''><img id='simLens32' src='amo/simulations/sim_mult_jpg/sim_mult_32.jpg' width='712' height='285' alt=''><img id='simLens33' src='amo/simulations/sim_mult_jpg/sim_mult_33.jpg' width='712' height='285' alt=''><img id='simLens34' src='amo/simulations/sim_mult_jpg/sim_mult_34.jpg' width='712' height='285' alt=''><img id='simLens35' src='amo/simulations/sim_mult_jpg/sim_mult_35.jpg' width='712' height='285' alt=''><img id='simLens36' src='amo/simulations/sim_mult_jpg/sim_mult_36.jpg' width='712' height='285' alt=''><img id='simLens37' src='amo/simulations/sim_mult_jpg/sim_mult_37.jpg' width='712' height='285' alt=''><img id='simLens38' src='amo/simulations/sim_mult_jpg/sim_mult_38.jpg' width='712' height='285' alt=''><img id='simLens39' src='amo/simulations/sim_mult_jpg/sim_mult_39.jpg' width='712' height='285' alt=''><img id='simLens40' src='amo/simulations/sim_mult_jpg/sim_mult_40.jpg' width='712' height='285' alt=''><img id='simLens41' src='amo/simulations/sim_mult_jpg/sim_mult_41.jpg' width='712' height='285' alt=''><img id='simLens42' src='amo/simulations/sim_mult_jpg/sim_mult_42.jpg' width='712' height='285' alt=''><img id='simLens43' src='amo/simulations/sim_mult_jpg/sim_mult_43.jpg' width='712' height='285' alt=''><img id='simLens44' src='amo/simulations/sim_mult_jpg/sim_mult_44.jpg' width='712' height='285' alt=''><img id='simLens45' src='amo/simulations/sim_mult_jpg/sim_mult_45.jpg' width='712' height='285' alt=''><img id='simLens46' src='amo/simulations/sim_mult_jpg/sim_mult_46.jpg' width='712' height='285' alt=''><img id='simLens47' src='amo/simulations/sim_mult_jpg/sim_mult_47.jpg' width='712' height='285' alt=''><img id='simLens48' src='amo/simulations/sim_mult_jpg/sim_mult_48.jpg' width='712' height='285' alt=''><img id='simLens49' src='amo/simulations/sim_mult_jpg/sim_mult_49.jpg' width='712' height='285' alt=''><img id='simLens50' src='amo/simulations/sim_mult_jpg/sim_mult_50.jpg' width='712' height='285' alt=''>",
			"symfony": "<img id='simLens1' src='amo/simulations/sim_symf_jpg/sim_symf_1.jpg' width='712' height='285' alt='' style='visibility:visible;'><img id='simLens2' src='amo/simulations/sim_symf_jpg/sim_symf_2.jpg' width='712' height='285' alt=''><img id='simLens3' src='amo/simulations/sim_symf_jpg/sim_symf_3.jpg' width='712' height='285' alt=''><img id='simLens4' src='amo/simulations/sim_symf_jpg/sim_symf_4.jpg' width='712' height='285' alt=''><img id='simLens5' src='amo/simulations/sim_symf_jpg/sim_symf_5.jpg' width='712' height='285' alt=''><img id='simLens6' src='amo/simulations/sim_symf_jpg/sim_symf_6.jpg' width='712' height='285' alt=''><img id='simLens7' src='amo/simulations/sim_symf_jpg/sim_symf_7.jpg' width='712' height='285' alt=''><img id='simLens8' src='amo/simulations/sim_symf_jpg/sim_symf_8.jpg' width='712' height='285' alt=''><img id='simLens9' src='amo/simulations/sim_symf_jpg/sim_symf_9.jpg' width='712' height='285' alt=''><img id='simLens10' src='amo/simulations/sim_symf_jpg/sim_symf_10.jpg' width='712' height='285' alt=''><img id='simLens11' src='amo/simulations/sim_symf_jpg/sim_symf_11.jpg' width='712' height='285' alt=''><img id='simLens12' src='amo/simulations/sim_symf_jpg/sim_symf_12.jpg' width='712' height='285' alt=''><img id='simLens13' src='amo/simulations/sim_symf_jpg/sim_symf_13.jpg' width='712' height='285' alt=''><img id='simLens14' src='amo/simulations/sim_symf_jpg/sim_symf_14.jpg' width='712' height='285' alt=''><img id='simLens15' src='amo/simulations/sim_symf_jpg/sim_symf_15.jpg' width='712' height='285' alt=''><img id='simLens16' src='amo/simulations/sim_symf_jpg/sim_symf_16.jpg' width='712' height='285' alt=''><img id='simLens17' src='amo/simulations/sim_symf_jpg/sim_symf_17.jpg' width='712' height='285' alt=''><img id='simLens18' src='amo/simulations/sim_symf_jpg/sim_symf_18.jpg' width='712' height='285' alt=''><img id='simLens19' src='amo/simulations/sim_symf_jpg/sim_symf_19.jpg' width='712' height='285' alt=''><img id='simLens20' src='amo/simulations/sim_symf_jpg/sim_symf_20.jpg' width='712' height='285' alt=''><img id='simLens21' src='amo/simulations/sim_symf_jpg/sim_symf_21.jpg' width='712' height='285' alt=''><img id='simLens22' src='amo/simulations/sim_symf_jpg/sim_symf_22.jpg' width='712' height='285' alt=''><img id='simLens23' src='amo/simulations/sim_symf_jpg/sim_symf_23.jpg' width='712' height='285' alt=''><img id='simLens24' src='amo/simulations/sim_symf_jpg/sim_symf_24.jpg' width='712' height='285' alt=''><img id='simLens25' src='amo/simulations/sim_symf_jpg/sim_symf_25.jpg' width='712' height='285' alt=''><img id='simLens26' src='amo/simulations/sim_symf_jpg/sim_symf_26.jpg' width='712' height='285' alt=''><img id='simLens27' src='amo/simulations/sim_symf_jpg/sim_symf_27.jpg' width='712' height='285' alt=''><img id='simLens28' src='amo/simulations/sim_symf_jpg/sim_symf_28.jpg' width='712' height='285' alt=''><img id='simLens29' src='amo/simulations/sim_symf_jpg/sim_symf_29.jpg' width='712' height='285' alt=''><img id='simLens30' src='amo/simulations/sim_symf_jpg/sim_symf_30.jpg' width='712' height='285' alt=''><img id='simLens31' src='amo/simulations/sim_symf_jpg/sim_symf_31.jpg' width='712' height='285' alt=''><img id='simLens32' src='amo/simulations/sim_symf_jpg/sim_symf_32.jpg' width='712' height='285' alt=''><img id='simLens33' src='amo/simulations/sim_symf_jpg/sim_symf_33.jpg' width='712' height='285' alt=''><img id='simLens34' src='amo/simulations/sim_symf_jpg/sim_symf_34.jpg' width='712' height='285' alt=''><img id='simLens35' src='amo/simulations/sim_symf_jpg/sim_symf_35.jpg' width='712' height='285' alt=''><img id='simLens36' src='amo/simulations/sim_symf_jpg/sim_symf_36.jpg' width='712' height='285' alt=''><img id='simLens37' src='amo/simulations/sim_symf_jpg/sim_symf_37.jpg' width='712' height='285' alt=''><img id='simLens38' src='amo/simulations/sim_symf_jpg/sim_symf_38.jpg' width='712' height='285' alt=''><img id='simLens39' src='amo/simulations/sim_symf_jpg/sim_symf_39.jpg' width='712' height='285' alt=''><img id='simLens40' src='amo/simulations/sim_symf_jpg/sim_symf_40.jpg' width='712' height='285' alt=''><img id='simLens41' src='amo/simulations/sim_symf_jpg/sim_symf_41.jpg' width='712' height='285' alt=''><img id='simLens42' src='amo/simulations/sim_symf_jpg/sim_symf_42.jpg' width='712' height='285' alt=''><img id='simLens43' src='amo/simulations/sim_symf_jpg/sim_symf_43.jpg' width='712' height='285' alt=''><img id='simLens44' src='amo/simulations/sim_symf_jpg/sim_symf_44.jpg' width='712' height='285' alt=''><img id='simLens45' src='amo/simulations/sim_symf_jpg/sim_symf_45.jpg' width='712' height='285' alt=''><img id='simLens46' src='amo/simulations/sim_symf_jpg/sim_symf_46.jpg' width='712' height='285' alt=''><img id='simLens47' src='amo/simulations/sim_symf_jpg/sim_symf_47.jpg' width='712' height='285' alt=''><img id='simLens48' src='amo/simulations/sim_symf_jpg/sim_symf_48.jpg' width='712' height='285' alt=''><img id='simLens49' src='amo/simulations/sim_symf_jpg/sim_symf_49.jpg' width='712' height='285' alt=''><img id='simLens50' src='amo/simulations/sim_symf_jpg/sim_symf_50.jpg' width='712' height='285' alt=''>",
			"synergy": "<img id='simLens1' src='amo/simulations/sim_syne_jpg/sim_syne_1.jpg' width='712' height='285' alt='' style='visibility:visible;'><img id='simLens2' src='amo/simulations/sim_syne_jpg/sim_syne_2.jpg' width='712' height='285' alt=''><img id='simLens3' src='amo/simulations/sim_syne_jpg/sim_syne_3.jpg' width='712' height='285' alt=''><img id='simLens4' src='amo/simulations/sim_syne_jpg/sim_syne_4.jpg' width='712' height='285' alt=''><img id='simLens5' src='amo/simulations/sim_syne_jpg/sim_syne_5.jpg' width='712' height='285' alt=''><img id='simLens6' src='amo/simulations/sim_syne_jpg/sim_syne_6.jpg' width='712' height='285' alt=''><img id='simLens7' src='amo/simulations/sim_syne_jpg/sim_syne_7.jpg' width='712' height='285' alt=''><img id='simLens8' src='amo/simulations/sim_syne_jpg/sim_syne_8.jpg' width='712' height='285' alt=''><img id='simLens9' src='amo/simulations/sim_syne_jpg/sim_syne_9.jpg' width='712' height='285' alt=''><img id='simLens10' src='amo/simulations/sim_syne_jpg/sim_syne_10.jpg' width='712' height='285' alt=''><img id='simLens11' src='amo/simulations/sim_syne_jpg/sim_syne_11.jpg' width='712' height='285' alt=''><img id='simLens12' src='amo/simulations/sim_syne_jpg/sim_syne_12.jpg' width='712' height='285' alt=''><img id='simLens13' src='amo/simulations/sim_syne_jpg/sim_syne_13.jpg' width='712' height='285' alt=''><img id='simLens14' src='amo/simulations/sim_syne_jpg/sim_syne_14.jpg' width='712' height='285' alt=''><img id='simLens15' src='amo/simulations/sim_syne_jpg/sim_syne_15.jpg' width='712' height='285' alt=''><img id='simLens16' src='amo/simulations/sim_syne_jpg/sim_syne_16.jpg' width='712' height='285' alt=''><img id='simLens17' src='amo/simulations/sim_syne_jpg/sim_syne_17.jpg' width='712' height='285' alt=''><img id='simLens18' src='amo/simulations/sim_syne_jpg/sim_syne_18.jpg' width='712' height='285' alt=''><img id='simLens19' src='amo/simulations/sim_syne_jpg/sim_syne_19.jpg' width='712' height='285' alt=''><img id='simLens20' src='amo/simulations/sim_syne_jpg/sim_syne_20.jpg' width='712' height='285' alt=''><img id='simLens21' src='amo/simulations/sim_syne_jpg/sim_syne_21.jpg' width='712' height='285' alt=''><img id='simLens22' src='amo/simulations/sim_syne_jpg/sim_syne_22.jpg' width='712' height='285' alt=''><img id='simLens23' src='amo/simulations/sim_syne_jpg/sim_syne_23.jpg' width='712' height='285' alt=''><img id='simLens24' src='amo/simulations/sim_syne_jpg/sim_syne_24.jpg' width='712' height='285' alt=''><img id='simLens25' src='amo/simulations/sim_syne_jpg/sim_syne_25.jpg' width='712' height='285' alt=''><img id='simLens26' src='amo/simulations/sim_syne_jpg/sim_syne_26.jpg' width='712' height='285' alt=''><img id='simLens27' src='amo/simulations/sim_syne_jpg/sim_syne_27.jpg' width='712' height='285' alt=''><img id='simLens28' src='amo/simulations/sim_syne_jpg/sim_syne_28.jpg' width='712' height='285' alt=''><img id='simLens29' src='amo/simulations/sim_syne_jpg/sim_syne_29.jpg' width='712' height='285' alt=''><img id='simLens30' src='amo/simulations/sim_syne_jpg/sim_syne_30.jpg' width='712' height='285' alt=''><img id='simLens31' src='amo/simulations/sim_syne_jpg/sim_syne_31.jpg' width='712' height='285' alt=''><img id='simLens32' src='amo/simulations/sim_syne_jpg/sim_syne_32.jpg' width='712' height='285' alt=''><img id='simLens33' src='amo/simulations/sim_syne_jpg/sim_syne_33.jpg' width='712' height='285' alt=''><img id='simLens34' src='amo/simulations/sim_syne_jpg/sim_syne_34.jpg' width='712' height='285' alt=''><img id='simLens35' src='amo/simulations/sim_syne_jpg/sim_syne_35.jpg' width='712' height='285' alt=''><img id='simLens36' src='amo/simulations/sim_syne_jpg/sim_syne_36.jpg' width='712' height='285' alt=''><img id='simLens37' src='amo/simulations/sim_syne_jpg/sim_syne_37.jpg' width='712' height='285' alt=''><img id='simLens38' src='amo/simulations/sim_syne_jpg/sim_syne_38.jpg' width='712' height='285' alt=''><img id='simLens39' src='amo/simulations/sim_syne_jpg/sim_syne_39.jpg' width='712' height='285' alt=''><img id='simLens40' src='amo/simulations/sim_syne_jpg/sim_syne_40.jpg' width='712' height='285' alt=''><img id='simLens41' src='amo/simulations/sim_syne_jpg/sim_syne_41.jpg' width='712' height='285' alt=''><img id='simLens42' src='amo/simulations/sim_syne_jpg/sim_syne_42.jpg' width='712' height='285' alt=''><img id='simLens43' src='amo/simulations/sim_syne_jpg/sim_syne_43.jpg' width='712' height='285' alt=''><img id='simLens44' src='amo/simulations/sim_syne_jpg/sim_syne_44.jpg' width='712' height='285' alt=''><img id='simLens45' src='amo/simulations/sim_syne_jpg/sim_syne_45.jpg' width='712' height='285' alt=''><img id='simLens46' src='amo/simulations/sim_syne_jpg/sim_syne_46.jpg' width='712' height='285' alt=''><img id='simLens47' src='amo/simulations/sim_syne_jpg/sim_syne_47.jpg' width='712' height='285' alt=''><img id='simLens48' src='amo/simulations/sim_syne_jpg/sim_syne_48.jpg' width='712' height='285' alt=''><img id='simLens49' src='amo/simulations/sim_syne_jpg/sim_syne_49.jpg' width='712' height='285' alt=''><img id='simLens50' src='amo/simulations/sim_syne_jpg/sim_syne_50.jpg' width='712' height='285' alt=''>",
			"sonata": "<img id='simLens1' src='amo/simulations/sim_sona_jpg/sim_sona_1.jpg' width='712' height='285' alt='' style='visibility:visible;'><img id='simLens2' src='amo/simulations/sim_sona_jpg/sim_sona_2.jpg' width='712' height='285' alt=''><img id='simLens3' src='amo/simulations/sim_sona_jpg/sim_sona_3.jpg' width='712' height='285' alt=''><img id='simLens4' src='amo/simulations/sim_sona_jpg/sim_sona_4.jpg' width='712' height='285' alt=''><img id='simLens5' src='amo/simulations/sim_sona_jpg/sim_sona_5.jpg' width='712' height='285' alt=''><img id='simLens6' src='amo/simulations/sim_sona_jpg/sim_sona_6.jpg' width='712' height='285' alt=''><img id='simLens7' src='amo/simulations/sim_sona_jpg/sim_sona_7.jpg' width='712' height='285' alt=''><img id='simLens8' src='amo/simulations/sim_sona_jpg/sim_sona_8.jpg' width='712' height='285' alt=''><img id='simLens9' src='amo/simulations/sim_sona_jpg/sim_sona_9.jpg' width='712' height='285' alt=''><img id='simLens10' src='amo/simulations/sim_sona_jpg/sim_sona_10.jpg' width='712' height='285' alt=''><img id='simLens11' src='amo/simulations/sim_sona_jpg/sim_sona_11.jpg' width='712' height='285' alt=''><img id='simLens12' src='amo/simulations/sim_sona_jpg/sim_sona_12.jpg' width='712' height='285' alt=''><img id='simLens13' src='amo/simulations/sim_sona_jpg/sim_sona_13.jpg' width='712' height='285' alt=''><img id='simLens14' src='amo/simulations/sim_sona_jpg/sim_sona_14.jpg' width='712' height='285' alt=''><img id='simLens15' src='amo/simulations/sim_sona_jpg/sim_sona_15.jpg' width='712' height='285' alt=''><img id='simLens16' src='amo/simulations/sim_sona_jpg/sim_sona_16.jpg' width='712' height='285' alt=''><img id='simLens17' src='amo/simulations/sim_sona_jpg/sim_sona_17.jpg' width='712' height='285' alt=''><img id='simLens18' src='amo/simulations/sim_sona_jpg/sim_sona_18.jpg' width='712' height='285' alt=''><img id='simLens19' src='amo/simulations/sim_sona_jpg/sim_sona_19.jpg' width='712' height='285' alt=''><img id='simLens20' src='amo/simulations/sim_sona_jpg/sim_sona_20.jpg' width='712' height='285' alt=''><img id='simLens21' src='amo/simulations/sim_sona_jpg/sim_sona_21.jpg' width='712' height='285' alt=''><img id='simLens22' src='amo/simulations/sim_sona_jpg/sim_sona_22.jpg' width='712' height='285' alt=''><img id='simLens23' src='amo/simulations/sim_sona_jpg/sim_sona_23.jpg' width='712' height='285' alt=''><img id='simLens24' src='amo/simulations/sim_sona_jpg/sim_sona_24.jpg' width='712' height='285' alt=''><img id='simLens25' src='amo/simulations/sim_sona_jpg/sim_sona_25.jpg' width='712' height='285' alt=''><img id='simLens26' src='amo/simulations/sim_sona_jpg/sim_sona_26.jpg' width='712' height='285' alt=''><img id='simLens27' src='amo/simulations/sim_sona_jpg/sim_sona_27.jpg' width='712' height='285' alt=''><img id='simLens28' src='amo/simulations/sim_sona_jpg/sim_sona_28.jpg' width='712' height='285' alt=''><img id='simLens29' src='amo/simulations/sim_sona_jpg/sim_sona_29.jpg' width='712' height='285' alt=''><img id='simLens30' src='amo/simulations/sim_sona_jpg/sim_sona_30.jpg' width='712' height='285' alt=''><img id='simLens31' src='amo/simulations/sim_sona_jpg/sim_sona_31.jpg' width='712' height='285' alt=''><img id='simLens32' src='amo/simulations/sim_sona_jpg/sim_sona_32.jpg' width='712' height='285' alt=''><img id='simLens33' src='amo/simulations/sim_sona_jpg/sim_sona_33.jpg' width='712' height='285' alt=''><img id='simLens34' src='amo/simulations/sim_sona_jpg/sim_sona_34.jpg' width='712' height='285' alt=''><img id='simLens35' src='amo/simulations/sim_sona_jpg/sim_sona_35.jpg' width='712' height='285' alt=''><img id='simLens36' src='amo/simulations/sim_sona_jpg/sim_sona_36.jpg' width='712' height='285' alt=''><img id='simLens37' src='amo/simulations/sim_sona_jpg/sim_sona_37.jpg' width='712' height='285' alt=''><img id='simLens38' src='amo/simulations/sim_sona_jpg/sim_sona_38.jpg' width='712' height='285' alt=''><img id='simLens39' src='amo/simulations/sim_sona_jpg/sim_sona_39.jpg' width='712' height='285' alt=''><img id='simLens40' src='amo/simulations/sim_sona_jpg/sim_sona_40.jpg' width='712' height='285' alt=''><img id='simLens41' src='amo/simulations/sim_sona_jpg/sim_sona_41.jpg' width='712' height='285' alt=''><img id='simLens42' src='amo/simulations/sim_sona_jpg/sim_sona_42.jpg' width='712' height='285' alt=''><img id='simLens43' src='amo/simulations/sim_sona_jpg/sim_sona_43.jpg' width='712' height='285' alt=''><img id='simLens44' src='amo/simulations/sim_sona_jpg/sim_sona_44.jpg' width='712' height='285' alt=''><img id='simLens45' src='amo/simulations/sim_sona_jpg/sim_sona_45.jpg' width='712' height='285' alt=''><img id='simLens46' src='amo/simulations/sim_sona_jpg/sim_sona_46.jpg' width='712' height='285' alt=''><img id='simLens47' src='amo/simulations/sim_sona_jpg/sim_sona_47.jpg' width='712' height='285' alt=''><img id='simLens48' src='amo/simulations/sim_sona_jpg/sim_sona_48.jpg' width='712' height='285' alt=''><img id='simLens49' src='amo/simulations/sim_sona_jpg/sim_sona_49.jpg' width='712' height='285' alt=''><img id='simLens50' src='amo/simulations/sim_sona_jpg/sim_sona_50.jpg' width='712' height='285' alt=''>",
			"eyehance": "<img id='simLens1' src='amo/simulations/sim_eyhc_jpg/sim_eyhc_1.jpg' width='712' height='285' alt='' style='visibility:visible;'><img id='simLens2' src='amo/simulations/sim_eyhc_jpg/sim_eyhc_2.jpg' width='712' height='285' alt=''><img id='simLens3' src='amo/simulations/sim_eyhc_jpg/sim_eyhc_3.jpg' width='712' height='285' alt=''><img id='simLens4' src='amo/simulations/sim_eyhc_jpg/sim_eyhc_4.jpg' width='712' height='285' alt=''><img id='simLens5' src='amo/simulations/sim_eyhc_jpg/sim_eyhc_5.jpg' width='712' height='285' alt=''><img id='simLens6' src='amo/simulations/sim_eyhc_jpg/sim_eyhc_6.jpg' width='712' height='285' alt=''><img id='simLens7' src='amo/simulations/sim_eyhc_jpg/sim_eyhc_7.jpg' width='712' height='285' alt=''><img id='simLens8' src='amo/simulations/sim_eyhc_jpg/sim_eyhc_8.jpg' width='712' height='285' alt=''><img id='simLens9' src='amo/simulations/sim_eyhc_jpg/sim_eyhc_9.jpg' width='712' height='285' alt=''><img id='simLens10' src='amo/simulations/sim_eyhc_jpg/sim_eyhc_10.jpg' width='712' height='285' alt=''><img id='simLens11' src='amo/simulations/sim_eyhc_jpg/sim_eyhc_11.jpg' width='712' height='285' alt=''><img id='simLens12' src='amo/simulations/sim_eyhc_jpg/sim_eyhc_12.jpg' width='712' height='285' alt=''><img id='simLens13' src='amo/simulations/sim_eyhc_jpg/sim_eyhc_13.jpg' width='712' height='285' alt=''><img id='simLens14' src='amo/simulations/sim_eyhc_jpg/sim_eyhc_14.jpg' width='712' height='285' alt=''><img id='simLens15' src='amo/simulations/sim_eyhc_jpg/sim_eyhc_15.jpg' width='712' height='285' alt=''><img id='simLens16' src='amo/simulations/sim_eyhc_jpg/sim_eyhc_16.jpg' width='712' height='285' alt=''><img id='simLens17' src='amo/simulations/sim_eyhc_jpg/sim_eyhc_17.jpg' width='712' height='285' alt=''><img id='simLens18' src='amo/simulations/sim_eyhc_jpg/sim_eyhc_18.jpg' width='712' height='285' alt=''><img id='simLens19' src='amo/simulations/sim_eyhc_jpg/sim_eyhc_19.jpg' width='712' height='285' alt=''><img id='simLens20' src='amo/simulations/sim_eyhc_jpg/sim_eyhc_20.jpg' width='712' height='285' alt=''><img id='simLens21' src='amo/simulations/sim_eyhc_jpg/sim_eyhc_21.jpg' width='712' height='285' alt=''><img id='simLens22' src='amo/simulations/sim_eyhc_jpg/sim_eyhc_22.jpg' width='712' height='285' alt=''><img id='simLens23' src='amo/simulations/sim_eyhc_jpg/sim_eyhc_23.jpg' width='712' height='285' alt=''><img id='simLens24' src='amo/simulations/sim_eyhc_jpg/sim_eyhc_24.jpg' width='712' height='285' alt=''><img id='simLens25' src='amo/simulations/sim_eyhc_jpg/sim_eyhc_25.jpg' width='712' height='285' alt=''><img id='simLens26' src='amo/simulations/sim_eyhc_jpg/sim_eyhc_26.jpg' width='712' height='285' alt=''><img id='simLens27' src='amo/simulations/sim_eyhc_jpg/sim_eyhc_27.jpg' width='712' height='285' alt=''><img id='simLens28' src='amo/simulations/sim_eyhc_jpg/sim_eyhc_28.jpg' width='712' height='285' alt=''><img id='simLens29' src='amo/simulations/sim_eyhc_jpg/sim_eyhc_29.jpg' width='712' height='285' alt=''><img id='simLens30' src='amo/simulations/sim_eyhc_jpg/sim_eyhc_30.jpg' width='712' height='285' alt=''><img id='simLens31' src='amo/simulations/sim_eyhc_jpg/sim_eyhc_31.jpg' width='712' height='285' alt=''><img id='simLens32' src='amo/simulations/sim_eyhc_jpg/sim_eyhc_32.jpg' width='712' height='285' alt=''><img id='simLens33' src='amo/simulations/sim_eyhc_jpg/sim_eyhc_33.jpg' width='712' height='285' alt=''><img id='simLens34' src='amo/simulations/sim_eyhc_jpg/sim_eyhc_34.jpg' width='712' height='285' alt=''><img id='simLens35' src='amo/simulations/sim_eyhc_jpg/sim_eyhc_35.jpg' width='712' height='285' alt=''><img id='simLens36' src='amo/simulations/sim_eyhc_jpg/sim_eyhc_36.jpg' width='712' height='285' alt=''><img id='simLens37' src='amo/simulations/sim_eyhc_jpg/sim_eyhc_37.jpg' width='712' height='285' alt=''><img id='simLens38' src='amo/simulations/sim_eyhc_jpg/sim_eyhc_38.jpg' width='712' height='285' alt=''><img id='simLens39' src='amo/simulations/sim_eyhc_jpg/sim_eyhc_39.jpg' width='712' height='285' alt=''><img id='simLens40' src='amo/simulations/sim_eyhc_jpg/sim_eyhc_40.jpg' width='712' height='285' alt=''><img id='simLens41' src='amo/simulations/sim_eyhc_jpg/sim_eyhc_41.jpg' width='712' height='285' alt=''><img id='simLens42' src='amo/simulations/sim_eyhc_jpg/sim_eyhc_42.jpg' width='712' height='285' alt=''><img id='simLens43' src='amo/simulations/sim_eyhc_jpg/sim_eyhc_43.jpg' width='712' height='285' alt=''><img id='simLens44' src='amo/simulations/sim_eyhc_jpg/sim_eyhc_44.jpg' width='712' height='285' alt=''><img id='simLens45' src='amo/simulations/sim_eyhc_jpg/sim_eyhc_45.jpg' width='712' height='285' alt=''><img id='simLens46' src='amo/simulations/sim_eyhc_jpg/sim_eyhc_46.jpg' width='712' height='285' alt=''><img id='simLens47' src='amo/simulations/sim_eyhc_jpg/sim_eyhc_47.jpg' width='712' height='285' alt=''><img id='simLens48' src='amo/simulations/sim_eyhc_jpg/sim_eyhc_48.jpg' width='712' height='285' alt=''><img id='simLens49' src='amo/simulations/sim_eyhc_jpg/sim_eyhc_49.jpg' width='712' height='285' alt=''><img id='simLens50' src='amo/simulations/sim_eyhc_jpg/sim_eyhc_50.jpg' width='712' height='285' alt=''>"
			};
		

		//!steal-remove-start
		/*this.simImages = {
			"normalvision": "<img id='simVision1' src='simulations/sim_norm_jpg/sim_norm_1.jpg' width='712' height='285' alt='' style='visibility:visible;'><img id='simVision2' src='simulations/sim_norm_jpg/sim_norm_2.jpg' width='712' height='285' alt=''><img id='simVision3' src='simulations/sim_norm_jpg/sim_norm_3.jpg' width='712' height='285' alt=''><img id='simVision4' src='simulations/sim_norm_jpg/sim_norm_4.jpg' width='712' height='285' alt=''><img id='simVision5' src='simulations/sim_norm_jpg/sim_norm_5.jpg' width='712' height='285' alt=''><img id='simVision6' src='simulations/sim_norm_jpg/sim_norm_6.jpg' width='712' height='285' alt=''><img id='simVision7' src='simulations/sim_norm_jpg/sim_norm_7.jpg' width='712' height='285' alt=''><img id='simVision8' src='simulations/sim_norm_jpg/sim_norm_8.jpg' width='712' height='285' alt=''><img id='simVision9' src='simulations/sim_norm_jpg/sim_norm_9.jpg' width='712' height='285' alt=''><img id='simVision10' src='simulations/sim_norm_jpg/sim_norm_10.jpg' width='712' height='285' alt=''><img id='simVision11' src='simulations/sim_norm_jpg/sim_norm_11.jpg' width='712' height='285' alt=''><img id='simVision12' src='simulations/sim_norm_jpg/sim_norm_12.jpg' width='712' height='285' alt=''><img id='simVision13' src='simulations/sim_norm_jpg/sim_norm_13.jpg' width='712' height='285' alt=''><img id='simVision14' src='simulations/sim_norm_jpg/sim_norm_14.jpg' width='712' height='285' alt=''><img id='simVision15' src='simulations/sim_norm_jpg/sim_norm_15.jpg' width='712' height='285' alt=''><img id='simVision16' src='simulations/sim_norm_jpg/sim_norm_16.jpg' width='712' height='285' alt=''><img id='simVision17' src='simulations/sim_norm_jpg/sim_norm_17.jpg' width='712' height='285' alt=''><img id='simVision18' src='simulations/sim_norm_jpg/sim_norm_18.jpg' width='712' height='285' alt=''><img id='simVision19' src='simulations/sim_norm_jpg/sim_norm_19.jpg' width='712' height='285' alt=''><img id='simVision20' src='simulations/sim_norm_jpg/sim_norm_20.jpg' width='712' height='285' alt=''><img id='simVision21' src='simulations/sim_norm_jpg/sim_norm_21.jpg' width='712' height='285' alt=''><img id='simVision22' src='simulations/sim_norm_jpg/sim_norm_22.jpg' width='712' height='285' alt=''><img id='simVision23' src='simulations/sim_norm_jpg/sim_norm_23.jpg' width='712' height='285' alt=''><img id='simVision24' src='simulations/sim_norm_jpg/sim_norm_24.jpg' width='712' height='285' alt=''><img id='simVision25' src='simulations/sim_norm_jpg/sim_norm_25.jpg' width='712' height='285' alt=''><img id='simVision26' src='simulations/sim_norm_jpg/sim_norm_26.jpg' width='712' height='285' alt=''><img id='simVision27' src='simulations/sim_norm_jpg/sim_norm_27.jpg' width='712' height='285' alt=''><img id='simVision28' src='simulations/sim_norm_jpg/sim_norm_28.jpg' width='712' height='285' alt=''><img id='simVision29' src='simulations/sim_norm_jpg/sim_norm_29.jpg' width='712' height='285' alt=''><img id='simVision30' src='simulations/sim_norm_jpg/sim_norm_30.jpg' width='712' height='285' alt=''><img id='simVision31' src='simulations/sim_norm_jpg/sim_norm_31.jpg' width='712' height='285' alt=''><img id='simVision32' src='simulations/sim_norm_jpg/sim_norm_32.jpg' width='712' height='285' alt=''><img id='simVision33' src='simulations/sim_norm_jpg/sim_norm_33.jpg' width='712' height='285' alt=''><img id='simVision34' src='simulations/sim_norm_jpg/sim_norm_34.jpg' width='712' height='285' alt=''><img id='simVision35' src='simulations/sim_norm_jpg/sim_norm_35.jpg' width='712' height='285' alt=''><img id='simVision36' src='simulations/sim_norm_jpg/sim_norm_36.jpg' width='712' height='285' alt=''><img id='simVision37' src='simulations/sim_norm_jpg/sim_norm_37.jpg' width='712' height='285' alt=''><img id='simVision38' src='simulations/sim_norm_jpg/sim_norm_38.jpg' width='712' height='285' alt=''><img id='simVision39' src='simulations/sim_norm_jpg/sim_norm_39.jpg' width='712' height='285' alt=''><img id='simVision40' src='simulations/sim_norm_jpg/sim_norm_40.jpg' width='712' height='285' alt=''><img id='simVision41' src='simulations/sim_norm_jpg/sim_norm_41.jpg' width='712' height='285' alt=''><img id='simVision42' src='simulations/sim_norm_jpg/sim_norm_42.jpg' width='712' height='285' alt=''><img id='simVision43' src='simulations/sim_norm_jpg/sim_norm_43.jpg' width='712' height='285' alt=''><img id='simVision44' src='simulations/sim_norm_jpg/sim_norm_44.jpg' width='712' height='285' alt=''><img id='simVision45' src='simulations/sim_norm_jpg/sim_norm_45.jpg' width='712' height='285' alt=''><img id='simVision46' src='simulations/sim_norm_jpg/sim_norm_46.jpg' width='712' height='285' alt=''><img id='simVision47' src='simulations/sim_norm_jpg/sim_norm_47.jpg' width='712' height='285' alt=''><img id='simVision48' src='simulations/sim_norm_jpg/sim_norm_48.jpg' width='712' height='285' alt=''><img id='simVision49' src='simulations/sim_norm_jpg/sim_norm_49.jpg' width='712' height='285' alt=''><img id='simVision50' src='simulations/sim_norm_jpg/sim_norm_50.jpg' width='712' height='285' alt=''>",
			"astigmatism":"<img id='simVision1' src='simulations/sim_astg_jpg/sim_astg_1.jpg' width='712' height='285' alt='' style='visibility:visible;'><img id='simVision2' src='simulations/sim_astg_jpg/sim_astg_2.jpg' width='712' height='285' alt=''><img id='simVision3' src='simulations/sim_astg_jpg/sim_astg_3.jpg' width='712' height='285' alt=''><img id='simVision4' src='simulations/sim_astg_jpg/sim_astg_4.jpg' width='712' height='285' alt=''><img id='simVision5' src='simulations/sim_astg_jpg/sim_astg_5.jpg' width='712' height='285' alt=''><img id='simVision6' src='simulations/sim_astg_jpg/sim_astg_6.jpg' width='712' height='285' alt=''><img id='simVision7' src='simulations/sim_astg_jpg/sim_astg_7.jpg' width='712' height='285' alt=''><img id='simVision8' src='simulations/sim_astg_jpg/sim_astg_8.jpg' width='712' height='285' alt=''><img id='simVision9' src='simulations/sim_astg_jpg/sim_astg_9.jpg' width='712' height='285' alt=''><img id='simVision10' src='simulations/sim_astg_jpg/sim_astg_10.jpg' width='712' height='285' alt=''><img id='simVision11' src='simulations/sim_astg_jpg/sim_astg_11.jpg' width='712' height='285' alt=''><img id='simVision12' src='simulations/sim_astg_jpg/sim_astg_12.jpg' width='712' height='285' alt=''><img id='simVision13' src='simulations/sim_astg_jpg/sim_astg_13.jpg' width='712' height='285' alt=''><img id='simVision14' src='simulations/sim_astg_jpg/sim_astg_14.jpg' width='712' height='285' alt=''><img id='simVision15' src='simulations/sim_astg_jpg/sim_astg_15.jpg' width='712' height='285' alt=''><img id='simVision16' src='simulations/sim_astg_jpg/sim_astg_16.jpg' width='712' height='285' alt=''><img id='simVision17' src='simulations/sim_astg_jpg/sim_astg_17.jpg' width='712' height='285' alt=''><img id='simVision18' src='simulations/sim_astg_jpg/sim_astg_18.jpg' width='712' height='285' alt=''><img id='simVision19' src='simulations/sim_astg_jpg/sim_astg_19.jpg' width='712' height='285' alt=''><img id='simVision20' src='simulations/sim_astg_jpg/sim_astg_20.jpg' width='712' height='285' alt=''><img id='simVision21' src='simulations/sim_astg_jpg/sim_astg_21.jpg' width='712' height='285' alt=''><img id='simVision22' src='simulations/sim_astg_jpg/sim_astg_22.jpg' width='712' height='285' alt=''><img id='simVision23' src='simulations/sim_astg_jpg/sim_astg_23.jpg' width='712' height='285' alt=''><img id='simVision24' src='simulations/sim_astg_jpg/sim_astg_24.jpg' width='712' height='285' alt=''><img id='simVision25' src='simulations/sim_astg_jpg/sim_astg_25.jpg' width='712' height='285' alt=''><img id='simVision26' src='simulations/sim_astg_jpg/sim_astg_26.jpg' width='712' height='285' alt=''><img id='simVision27' src='simulations/sim_astg_jpg/sim_astg_27.jpg' width='712' height='285' alt=''><img id='simVision28' src='simulations/sim_astg_jpg/sim_astg_28.jpg' width='712' height='285' alt=''><img id='simVision29' src='simulations/sim_astg_jpg/sim_astg_29.jpg' width='712' height='285' alt=''><img id='simVision30' src='simulations/sim_astg_jpg/sim_astg_30.jpg' width='712' height='285' alt=''><img id='simVision31' src='simulations/sim_astg_jpg/sim_astg_31.jpg' width='712' height='285' alt=''><img id='simVision32' src='simulations/sim_astg_jpg/sim_astg_32.jpg' width='712' height='285' alt=''><img id='simVision33' src='simulations/sim_astg_jpg/sim_astg_33.jpg' width='712' height='285' alt=''><img id='simVision34' src='simulations/sim_astg_jpg/sim_astg_34.jpg' width='712' height='285' alt=''><img id='simVision35' src='simulations/sim_astg_jpg/sim_astg_35.jpg' width='712' height='285' alt=''><img id='simVision36' src='simulations/sim_astg_jpg/sim_astg_36.jpg' width='712' height='285' alt=''><img id='simVision37' src='simulations/sim_astg_jpg/sim_astg_37.jpg' width='712' height='285' alt=''><img id='simVision38' src='simulations/sim_astg_jpg/sim_astg_38.jpg' width='712' height='285' alt=''><img id='simVision39' src='simulations/sim_astg_jpg/sim_astg_39.jpg' width='712' height='285' alt=''><img id='simVision40' src='simulations/sim_astg_jpg/sim_astg_40.jpg' width='712' height='285' alt=''><img id='simVision41' src='simulations/sim_astg_jpg/sim_astg_41.jpg' width='712' height='285' alt=''><img id='simVision42' src='simulations/sim_astg_jpg/sim_astg_42.jpg' width='712' height='285' alt=''><img id='simVision43' src='simulations/sim_astg_jpg/sim_astg_43.jpg' width='712' height='285' alt=''><img id='simVision44' src='simulations/sim_astg_jpg/sim_astg_44.jpg' width='712' height='285' alt=''><img id='simVision45' src='simulations/sim_astg_jpg/sim_astg_45.jpg' width='712' height='285' alt=''><img id='simVision46' src='simulations/sim_astg_jpg/sim_astg_46.jpg' width='712' height='285' alt=''><img id='simVision47' src='simulations/sim_astg_jpg/sim_astg_47.jpg' width='712' height='285' alt=''><img id='simVision48' src='simulations/sim_astg_jpg/sim_astg_48.jpg' width='712' height='285' alt=''><img id='simVision49' src='simulations/sim_astg_jpg/sim_astg_49.jpg' width='712' height='285' alt=''><img id='simVision50' src='simulations/sim_astg_jpg/sim_astg_50.jpg' width='712' height='285' alt=''>",
			"presbyopia": "<img id='simVision1' src='simulations/sim_pres_jpg/sim_pres_1.jpg' width='712' height='285' alt='' style='visibility:visible;'><img id='simVision2' src='simulations/sim_pres_jpg/sim_pres_2.jpg' width='712' height='285' alt=''><img id='simVision3' src='simulations/sim_pres_jpg/sim_pres_3.jpg' width='712' height='285' alt=''><img id='simVision4' src='simulations/sim_pres_jpg/sim_pres_4.jpg' width='712' height='285' alt=''><img id='simVision5' src='simulations/sim_pres_jpg/sim_pres_5.jpg' width='712' height='285' alt=''><img id='simVision6' src='simulations/sim_pres_jpg/sim_pres_6.jpg' width='712' height='285' alt=''><img id='simVision7' src='simulations/sim_pres_jpg/sim_pres_7.jpg' width='712' height='285' alt=''><img id='simVision8' src='simulations/sim_pres_jpg/sim_pres_8.jpg' width='712' height='285' alt=''><img id='simVision9' src='simulations/sim_pres_jpg/sim_pres_9.jpg' width='712' height='285' alt=''><img id='simVision10' src='simulations/sim_pres_jpg/sim_pres_10.jpg' width='712' height='285' alt=''><img id='simVision11' src='simulations/sim_pres_jpg/sim_pres_11.jpg' width='712' height='285' alt=''><img id='simVision12' src='simulations/sim_pres_jpg/sim_pres_12.jpg' width='712' height='285' alt=''><img id='simVision13' src='simulations/sim_pres_jpg/sim_pres_13.jpg' width='712' height='285' alt=''><img id='simVision14' src='simulations/sim_pres_jpg/sim_pres_14.jpg' width='712' height='285' alt=''><img id='simVision15' src='simulations/sim_pres_jpg/sim_pres_15.jpg' width='712' height='285' alt=''><img id='simVision16' src='simulations/sim_pres_jpg/sim_pres_16.jpg' width='712' height='285' alt=''><img id='simVision17' src='simulations/sim_pres_jpg/sim_pres_17.jpg' width='712' height='285' alt=''><img id='simVision18' src='simulations/sim_pres_jpg/sim_pres_18.jpg' width='712' height='285' alt=''><img id='simVision19' src='simulations/sim_pres_jpg/sim_pres_19.jpg' width='712' height='285' alt=''><img id='simVision20' src='simulations/sim_pres_jpg/sim_pres_20.jpg' width='712' height='285' alt=''><img id='simVision21' src='simulations/sim_pres_jpg/sim_pres_21.jpg' width='712' height='285' alt=''><img id='simVision22' src='simulations/sim_pres_jpg/sim_pres_22.jpg' width='712' height='285' alt=''><img id='simVision23' src='simulations/sim_pres_jpg/sim_pres_23.jpg' width='712' height='285' alt=''><img id='simVision24' src='simulations/sim_pres_jpg/sim_pres_24.jpg' width='712' height='285' alt=''><img id='simVision25' src='simulations/sim_pres_jpg/sim_pres_25.jpg' width='712' height='285' alt=''><img id='simVision26' src='simulations/sim_pres_jpg/sim_pres_26.jpg' width='712' height='285' alt=''><img id='simVision27' src='simulations/sim_pres_jpg/sim_pres_27.jpg' width='712' height='285' alt=''><img id='simVision28' src='simulations/sim_pres_jpg/sim_pres_28.jpg' width='712' height='285' alt=''><img id='simVision29' src='simulations/sim_pres_jpg/sim_pres_29.jpg' width='712' height='285' alt=''><img id='simVision30' src='simulations/sim_pres_jpg/sim_pres_30.jpg' width='712' height='285' alt=''><img id='simVision31' src='simulations/sim_pres_jpg/sim_pres_31.jpg' width='712' height='285' alt=''><img id='simVision32' src='simulations/sim_pres_jpg/sim_pres_32.jpg' width='712' height='285' alt=''><img id='simVision33' src='simulations/sim_pres_jpg/sim_pres_33.jpg' width='712' height='285' alt=''><img id='simVision34' src='simulations/sim_pres_jpg/sim_pres_34.jpg' width='712' height='285' alt=''><img id='simVision35' src='simulations/sim_pres_jpg/sim_pres_35.jpg' width='712' height='285' alt=''><img id='simVision36' src='simulations/sim_pres_jpg/sim_pres_36.jpg' width='712' height='285' alt=''><img id='simVision37' src='simulations/sim_pres_jpg/sim_pres_37.jpg' width='712' height='285' alt=''><img id='simVision38' src='simulations/sim_pres_jpg/sim_pres_38.jpg' width='712' height='285' alt=''><img id='simVision39' src='simulations/sim_pres_jpg/sim_pres_39.jpg' width='712' height='285' alt=''><img id='simVision40' src='simulations/sim_pres_jpg/sim_pres_40.jpg' width='712' height='285' alt=''><img id='simVision41' src='simulations/sim_pres_jpg/sim_pres_41.jpg' width='712' height='285' alt=''><img id='simVision42' src='simulations/sim_pres_jpg/sim_pres_42.jpg' width='712' height='285' alt=''><img id='simVision43' src='simulations/sim_pres_jpg/sim_pres_43.jpg' width='712' height='285' alt=''><img id='simVision44' src='simulations/sim_pres_jpg/sim_pres_44.jpg' width='712' height='285' alt=''><img id='simVision45' src='simulations/sim_pres_jpg/sim_pres_45.jpg' width='712' height='285' alt=''><img id='simVision46' src='simulations/sim_pres_jpg/sim_pres_46.jpg' width='712' height='285' alt=''><img id='simVision47' src='simulations/sim_pres_jpg/sim_pres_47.jpg' width='712' height='285' alt=''><img id='simVision48' src='simulations/sim_pres_jpg/sim_pres_48.jpg' width='712' height='285' alt=''><img id='simVision49' src='simulations/sim_pres_jpg/sim_pres_49.jpg' width='712' height='285' alt=''><img id='simVision50' src='simulations/sim_pres_jpg/sim_pres_50.jpg' width='712' height='285' alt=''>",
			"cataract": "<img id='simVision1' src='simulations/sim_cata_jpg/sim_cata_1.jpg' width='712' height='285' alt='' style='visibility:visible;'><img id='simVision2' src='simulations/sim_cata_jpg/sim_cata_2.jpg' width='712' height='285' alt=''><img id='simVision3' src='simulations/sim_cata_jpg/sim_cata_3.jpg' width='712' height='285' alt=''><img id='simVision4' src='simulations/sim_cata_jpg/sim_cata_4.jpg' width='712' height='285' alt=''><img id='simVision5' src='simulations/sim_cata_jpg/sim_cata_5.jpg' width='712' height='285' alt=''><img id='simVision6' src='simulations/sim_cata_jpg/sim_cata_6.jpg' width='712' height='285' alt=''><img id='simVision7' src='simulations/sim_cata_jpg/sim_cata_7.jpg' width='712' height='285' alt=''><img id='simVision8' src='simulations/sim_cata_jpg/sim_cata_8.jpg' width='712' height='285' alt=''><img id='simVision9' src='simulations/sim_cata_jpg/sim_cata_9.jpg' width='712' height='285' alt=''><img id='simVision10' src='simulations/sim_cata_jpg/sim_cata_10.jpg' width='712' height='285' alt=''><img id='simVision11' src='simulations/sim_cata_jpg/sim_cata_11.jpg' width='712' height='285' alt=''><img id='simVision12' src='simulations/sim_cata_jpg/sim_cata_12.jpg' width='712' height='285' alt=''><img id='simVision13' src='simulations/sim_cata_jpg/sim_cata_13.jpg' width='712' height='285' alt=''><img id='simVision14' src='simulations/sim_cata_jpg/sim_cata_14.jpg' width='712' height='285' alt=''><img id='simVision15' src='simulations/sim_cata_jpg/sim_cata_15.jpg' width='712' height='285' alt=''><img id='simVision16' src='simulations/sim_cata_jpg/sim_cata_16.jpg' width='712' height='285' alt=''><img id='simVision17' src='simulations/sim_cata_jpg/sim_cata_17.jpg' width='712' height='285' alt=''><img id='simVision18' src='simulations/sim_cata_jpg/sim_cata_18.jpg' width='712' height='285' alt=''><img id='simVision19' src='simulations/sim_cata_jpg/sim_cata_19.jpg' width='712' height='285' alt=''><img id='simVision20' src='simulations/sim_cata_jpg/sim_cata_20.jpg' width='712' height='285' alt=''><img id='simVision21' src='simulations/sim_cata_jpg/sim_cata_21.jpg' width='712' height='285' alt=''><img id='simVision22' src='simulations/sim_cata_jpg/sim_cata_22.jpg' width='712' height='285' alt=''><img id='simVision23' src='simulations/sim_cata_jpg/sim_cata_23.jpg' width='712' height='285' alt=''><img id='simVision24' src='simulations/sim_cata_jpg/sim_cata_24.jpg' width='712' height='285' alt=''><img id='simVision25' src='simulations/sim_cata_jpg/sim_cata_25.jpg' width='712' height='285' alt=''><img id='simVision26' src='simulations/sim_cata_jpg/sim_cata_26.jpg' width='712' height='285' alt=''><img id='simVision27' src='simulations/sim_cata_jpg/sim_cata_27.jpg' width='712' height='285' alt=''><img id='simVision28' src='simulations/sim_cata_jpg/sim_cata_28.jpg' width='712' height='285' alt=''><img id='simVision29' src='simulations/sim_cata_jpg/sim_cata_29.jpg' width='712' height='285' alt=''><img id='simVision30' src='simulations/sim_cata_jpg/sim_cata_30.jpg' width='712' height='285' alt=''><img id='simVision31' src='simulations/sim_cata_jpg/sim_cata_31.jpg' width='712' height='285' alt=''><img id='simVision32' src='simulations/sim_cata_jpg/sim_cata_32.jpg' width='712' height='285' alt=''><img id='simVision33' src='simulations/sim_cata_jpg/sim_cata_33.jpg' width='712' height='285' alt=''><img id='simVision34' src='simulations/sim_cata_jpg/sim_cata_34.jpg' width='712' height='285' alt=''><img id='simVision35' src='simulations/sim_cata_jpg/sim_cata_35.jpg' width='712' height='285' alt=''><img id='simVision36' src='simulations/sim_cata_jpg/sim_cata_36.jpg' width='712' height='285' alt=''><img id='simVision37' src='simulations/sim_cata_jpg/sim_cata_37.jpg' width='712' height='285' alt=''><img id='simVision38' src='simulations/sim_cata_jpg/sim_cata_38.jpg' width='712' height='285' alt=''><img id='simVision39' src='simulations/sim_cata_jpg/sim_cata_39.jpg' width='712' height='285' alt=''><img id='simVision40' src='simulations/sim_cata_jpg/sim_cata_40.jpg' width='712' height='285' alt=''><img id='simVision41' src='simulations/sim_cata_jpg/sim_cata_41.jpg' width='712' height='285' alt=''><img id='simVision42' src='simulations/sim_cata_jpg/sim_cata_42.jpg' width='712' height='285' alt=''><img id='simVision43' src='simulations/sim_cata_jpg/sim_cata_43.jpg' width='712' height='285' alt=''><img id='simVision44' src='simulations/sim_cata_jpg/sim_cata_44.jpg' width='712' height='285' alt=''><img id='simVision45' src='simulations/sim_cata_jpg/sim_cata_45.jpg' width='712' height='285' alt=''><img id='simVision46' src='simulations/sim_cata_jpg/sim_cata_46.jpg' width='712' height='285' alt=''><img id='simVision47' src='simulations/sim_cata_jpg/sim_cata_47.jpg' width='712' height='285' alt=''><img id='simVision48' src='simulations/sim_cata_jpg/sim_cata_48.jpg' width='712' height='285' alt=''><img id='simVision49' src='simulations/sim_cata_jpg/sim_cata_49.jpg' width='712' height='285' alt=''><img id='simVision50' src='simulations/sim_cata_jpg/sim_cata_50.jpg' width='712' height='285' alt=''>",
			"cataractsurgery": "<img id='simVision1' src='simulations/sim_surg_jpg/sim_surg_1.jpg' width='712' height='285' alt='' style='visibility:visible;'><img id='simVision2' src='simulations/sim_surg_jpg/sim_surg_2.jpg' width='712' height='285' alt=''><img id='simVision3' src='simulations/sim_surg_jpg/sim_surg_3.jpg' width='712' height='285' alt=''><img id='simVision4' src='simulations/sim_surg_jpg/sim_surg_4.jpg' width='712' height='285' alt=''><img id='simVision5' src='simulations/sim_surg_jpg/sim_surg_5.jpg' width='712' height='285' alt=''><img id='simVision6' src='simulations/sim_surg_jpg/sim_surg_6.jpg' width='712' height='285' alt=''><img id='simVision7' src='simulations/sim_surg_jpg/sim_surg_7.jpg' width='712' height='285' alt=''><img id='simVision8' src='simulations/sim_surg_jpg/sim_surg_8.jpg' width='712' height='285' alt=''><img id='simVision9' src='simulations/sim_surg_jpg/sim_surg_9.jpg' width='712' height='285' alt=''><img id='simVision10' src='simulations/sim_surg_jpg/sim_surg_10.jpg' width='712' height='285' alt=''><img id='simVision11' src='simulations/sim_surg_jpg/sim_surg_11.jpg' width='712' height='285' alt=''><img id='simVision12' src='simulations/sim_surg_jpg/sim_surg_12.jpg' width='712' height='285' alt=''><img id='simVision13' src='simulations/sim_surg_jpg/sim_surg_13.jpg' width='712' height='285' alt=''><img id='simVision14' src='simulations/sim_surg_jpg/sim_surg_14.jpg' width='712' height='285' alt=''><img id='simVision15' src='simulations/sim_surg_jpg/sim_surg_15.jpg' width='712' height='285' alt=''><img id='simVision16' src='simulations/sim_surg_jpg/sim_surg_16.jpg' width='712' height='285' alt=''><img id='simVision17' src='simulations/sim_surg_jpg/sim_surg_17.jpg' width='712' height='285' alt=''><img id='simVision18' src='simulations/sim_surg_jpg/sim_surg_18.jpg' width='712' height='285' alt=''><img id='simVision19' src='simulations/sim_surg_jpg/sim_surg_19.jpg' width='712' height='285' alt=''><img id='simVision20' src='simulations/sim_surg_jpg/sim_surg_20.jpg' width='712' height='285' alt=''><img id='simVision21' src='simulations/sim_surg_jpg/sim_surg_21.jpg' width='712' height='285' alt=''><img id='simVision22' src='simulations/sim_surg_jpg/sim_surg_22.jpg' width='712' height='285' alt=''><img id='simVision23' src='simulations/sim_surg_jpg/sim_surg_23.jpg' width='712' height='285' alt=''><img id='simVision24' src='simulations/sim_surg_jpg/sim_surg_24.jpg' width='712' height='285' alt=''><img id='simVision25' src='simulations/sim_surg_jpg/sim_surg_25.jpg' width='712' height='285' alt=''><img id='simVision26' src='simulations/sim_surg_jpg/sim_surg_26.jpg' width='712' height='285' alt=''><img id='simVision27' src='simulations/sim_surg_jpg/sim_surg_27.jpg' width='712' height='285' alt=''><img id='simVision28' src='simulations/sim_surg_jpg/sim_surg_28.jpg' width='712' height='285' alt=''><img id='simVision29' src='simulations/sim_surg_jpg/sim_surg_29.jpg' width='712' height='285' alt=''><img id='simVision30' src='simulations/sim_surg_jpg/sim_surg_30.jpg' width='712' height='285' alt=''><img id='simVision31' src='simulations/sim_surg_jpg/sim_surg_31.jpg' width='712' height='285' alt=''><img id='simVision32' src='simulations/sim_surg_jpg/sim_surg_32.jpg' width='712' height='285' alt=''><img id='simVision33' src='simulations/sim_surg_jpg/sim_surg_33.jpg' width='712' height='285' alt=''><img id='simVision34' src='simulations/sim_surg_jpg/sim_surg_34.jpg' width='712' height='285' alt=''><img id='simVision35' src='simulations/sim_surg_jpg/sim_surg_35.jpg' width='712' height='285' alt=''><img id='simVision36' src='simulations/sim_surg_jpg/sim_surg_36.jpg' width='712' height='285' alt=''><img id='simVision37' src='simulations/sim_surg_jpg/sim_surg_37.jpg' width='712' height='285' alt=''><img id='simVision38' src='simulations/sim_surg_jpg/sim_surg_38.jpg' width='712' height='285' alt=''><img id='simVision39' src='simulations/sim_surg_jpg/sim_surg_39.jpg' width='712' height='285' alt=''><img id='simVision40' src='simulations/sim_surg_jpg/sim_surg_40.jpg' width='712' height='285' alt=''><img id='simVision41' src='simulations/sim_surg_jpg/sim_surg_41.jpg' width='712' height='285' alt=''><img id='simVision42' src='simulations/sim_surg_jpg/sim_surg_42.jpg' width='712' height='285' alt=''><img id='simVision43' src='simulations/sim_surg_jpg/sim_surg_43.jpg' width='712' height='285' alt=''><img id='simVision44' src='simulations/sim_surg_jpg/sim_surg_44.jpg' width='712' height='285' alt=''><img id='simVision45' src='simulations/sim_surg_jpg/sim_surg_45.jpg' width='712' height='285' alt=''><img id='simVision46' src='simulations/sim_surg_jpg/sim_surg_46.jpg' width='712' height='285' alt=''><img id='simVision47' src='simulations/sim_surg_jpg/sim_surg_47.jpg' width='712' height='285' alt=''><img id='simVision48' src='simulations/sim_surg_jpg/sim_surg_48.jpg' width='712' height='285' alt=''><img id='simVision49' src='simulations/sim_surg_jpg/sim_surg_49.jpg' width='712' height='285' alt=''><img id='simVision50' src='simulations/sim_surg_jpg/sim_surg_50.jpg' width='712' height='285' alt=''>",
			"monolens": "<img id='simLens1' src='simulations/sim_mono_jpg/sim_mono_1.jpg' width='712' height='285' alt='' style='visibility:visible;'><img id='simLens2' src='simulations/sim_mono_jpg/sim_mono_2.jpg' width='712' height='285' alt=''><img id='simLens3' src='simulations/sim_mono_jpg/sim_mono_3.jpg' width='712' height='285' alt=''><img id='simLens4' src='simulations/sim_mono_jpg/sim_mono_4.jpg' width='712' height='285' alt=''><img id='simLens5' src='simulations/sim_mono_jpg/sim_mono_5.jpg' width='712' height='285' alt=''><img id='simLens6' src='simulations/sim_mono_jpg/sim_mono_6.jpg' width='712' height='285' alt=''><img id='simLens7' src='simulations/sim_mono_jpg/sim_mono_7.jpg' width='712' height='285' alt=''><img id='simLens8' src='simulations/sim_mono_jpg/sim_mono_8.jpg' width='712' height='285' alt=''><img id='simLens9' src='simulations/sim_mono_jpg/sim_mono_9.jpg' width='712' height='285' alt=''><img id='simLens10' src='simulations/sim_mono_jpg/sim_mono_10.jpg' width='712' height='285' alt=''><img id='simLens11' src='simulations/sim_mono_jpg/sim_mono_11.jpg' width='712' height='285' alt=''><img id='simLens12' src='simulations/sim_mono_jpg/sim_mono_12.jpg' width='712' height='285' alt=''><img id='simLens13' src='simulations/sim_mono_jpg/sim_mono_13.jpg' width='712' height='285' alt=''><img id='simLens14' src='simulations/sim_mono_jpg/sim_mono_14.jpg' width='712' height='285' alt=''><img id='simLens15' src='simulations/sim_mono_jpg/sim_mono_15.jpg' width='712' height='285' alt=''><img id='simLens16' src='simulations/sim_mono_jpg/sim_mono_16.jpg' width='712' height='285' alt=''><img id='simLens17' src='simulations/sim_mono_jpg/sim_mono_17.jpg' width='712' height='285' alt=''><img id='simLens18' src='simulations/sim_mono_jpg/sim_mono_18.jpg' width='712' height='285' alt=''><img id='simLens19' src='simulations/sim_mono_jpg/sim_mono_19.jpg' width='712' height='285' alt=''><img id='simLens20' src='simulations/sim_mono_jpg/sim_mono_20.jpg' width='712' height='285' alt=''><img id='simLens21' src='simulations/sim_mono_jpg/sim_mono_21.jpg' width='712' height='285' alt=''><img id='simLens22' src='simulations/sim_mono_jpg/sim_mono_22.jpg' width='712' height='285' alt=''><img id='simLens23' src='simulations/sim_mono_jpg/sim_mono_23.jpg' width='712' height='285' alt=''><img id='simLens24' src='simulations/sim_mono_jpg/sim_mono_24.jpg' width='712' height='285' alt=''><img id='simLens25' src='simulations/sim_mono_jpg/sim_mono_25.jpg' width='712' height='285' alt=''><img id='simLens26' src='simulations/sim_mono_jpg/sim_mono_26.jpg' width='712' height='285' alt=''><img id='simLens27' src='simulations/sim_mono_jpg/sim_mono_27.jpg' width='712' height='285' alt=''><img id='simLens28' src='simulations/sim_mono_jpg/sim_mono_28.jpg' width='712' height='285' alt=''><img id='simLens29' src='simulations/sim_mono_jpg/sim_mono_29.jpg' width='712' height='285' alt=''><img id='simLens30' src='simulations/sim_mono_jpg/sim_mono_30.jpg' width='712' height='285' alt=''><img id='simLens31' src='simulations/sim_mono_jpg/sim_mono_31.jpg' width='712' height='285' alt=''><img id='simLens32' src='simulations/sim_mono_jpg/sim_mono_32.jpg' width='712' height='285' alt=''><img id='simLens33' src='simulations/sim_mono_jpg/sim_mono_33.jpg' width='712' height='285' alt=''><img id='simLens34' src='simulations/sim_mono_jpg/sim_mono_34.jpg' width='712' height='285' alt=''><img id='simLens35' src='simulations/sim_mono_jpg/sim_mono_35.jpg' width='712' height='285' alt=''><img id='simLens36' src='simulations/sim_mono_jpg/sim_mono_36.jpg' width='712' height='285' alt=''><img id='simLens37' src='simulations/sim_mono_jpg/sim_mono_37.jpg' width='712' height='285' alt=''><img id='simLens38' src='simulations/sim_mono_jpg/sim_mono_38.jpg' width='712' height='285' alt=''><img id='simLens39' src='simulations/sim_mono_jpg/sim_mono_39.jpg' width='712' height='285' alt=''><img id='simLens40' src='simulations/sim_mono_jpg/sim_mono_40.jpg' width='712' height='285' alt=''><img id='simLens41' src='simulations/sim_mono_jpg/sim_mono_41.jpg' width='712' height='285' alt=''><img id='simLens42' src='simulations/sim_mono_jpg/sim_mono_42.jpg' width='712' height='285' alt=''><img id='simLens43' src='simulations/sim_mono_jpg/sim_mono_43.jpg' width='712' height='285' alt=''><img id='simLens44' src='simulations/sim_mono_jpg/sim_mono_44.jpg' width='712' height='285' alt=''><img id='simLens45' src='simulations/sim_mono_jpg/sim_mono_45.jpg' width='712' height='285' alt=''><img id='simLens46' src='simulations/sim_mono_jpg/sim_mono_46.jpg' width='712' height='285' alt=''><img id='simLens47' src='simulations/sim_mono_jpg/sim_mono_47.jpg' width='712' height='285' alt=''><img id='simLens48' src='simulations/sim_mono_jpg/sim_mono_48.jpg' width='712' height='285' alt=''><img id='simLens49' src='simulations/sim_mono_jpg/sim_mono_49.jpg' width='712' height='285' alt=''><img id='simLens50' src='simulations/sim_mono_jpg/sim_mono_50.jpg' width='712' height='285' alt=''>",
			"multilens": "<img id='simLens1' src='simulations/sim_mult_jpg/sim_mult_1.jpg' width='712' height='285' alt='' style='visibility:visible;'><img id='simLens2' src='simulations/sim_mult_jpg/sim_mult_2.jpg' width='712' height='285' alt=''><img id='simLens3' src='simulations/sim_mult_jpg/sim_mult_3.jpg' width='712' height='285' alt=''><img id='simLens4' src='simulations/sim_mult_jpg/sim_mult_4.jpg' width='712' height='285' alt=''><img id='simLens5' src='simulations/sim_mult_jpg/sim_mult_5.jpg' width='712' height='285' alt=''><img id='simLens6' src='simulations/sim_mult_jpg/sim_mult_6.jpg' width='712' height='285' alt=''><img id='simLens7' src='simulations/sim_mult_jpg/sim_mult_7.jpg' width='712' height='285' alt=''><img id='simLens8' src='simulations/sim_mult_jpg/sim_mult_8.jpg' width='712' height='285' alt=''><img id='simLens9' src='simulations/sim_mult_jpg/sim_mult_9.jpg' width='712' height='285' alt=''><img id='simLens10' src='simulations/sim_mult_jpg/sim_mult_10.jpg' width='712' height='285' alt=''><img id='simLens11' src='simulations/sim_mult_jpg/sim_mult_11.jpg' width='712' height='285' alt=''><img id='simLens12' src='simulations/sim_mult_jpg/sim_mult_12.jpg' width='712' height='285' alt=''><img id='simLens13' src='simulations/sim_mult_jpg/sim_mult_13.jpg' width='712' height='285' alt=''><img id='simLens14' src='simulations/sim_mult_jpg/sim_mult_14.jpg' width='712' height='285' alt=''><img id='simLens15' src='simulations/sim_mult_jpg/sim_mult_15.jpg' width='712' height='285' alt=''><img id='simLens16' src='simulations/sim_mult_jpg/sim_mult_16.jpg' width='712' height='285' alt=''><img id='simLens17' src='simulations/sim_mult_jpg/sim_mult_17.jpg' width='712' height='285' alt=''><img id='simLens18' src='simulations/sim_mult_jpg/sim_mult_18.jpg' width='712' height='285' alt=''><img id='simLens19' src='simulations/sim_mult_jpg/sim_mult_19.jpg' width='712' height='285' alt=''><img id='simLens20' src='simulations/sim_mult_jpg/sim_mult_20.jpg' width='712' height='285' alt=''><img id='simLens21' src='simulations/sim_mult_jpg/sim_mult_21.jpg' width='712' height='285' alt=''><img id='simLens22' src='simulations/sim_mult_jpg/sim_mult_22.jpg' width='712' height='285' alt=''><img id='simLens23' src='simulations/sim_mult_jpg/sim_mult_23.jpg' width='712' height='285' alt=''><img id='simLens24' src='simulations/sim_mult_jpg/sim_mult_24.jpg' width='712' height='285' alt=''><img id='simLens25' src='simulations/sim_mult_jpg/sim_mult_25.jpg' width='712' height='285' alt=''><img id='simLens26' src='simulations/sim_mult_jpg/sim_mult_26.jpg' width='712' height='285' alt=''><img id='simLens27' src='simulations/sim_mult_jpg/sim_mult_27.jpg' width='712' height='285' alt=''><img id='simLens28' src='simulations/sim_mult_jpg/sim_mult_28.jpg' width='712' height='285' alt=''><img id='simLens29' src='simulations/sim_mult_jpg/sim_mult_29.jpg' width='712' height='285' alt=''><img id='simLens30' src='simulations/sim_mult_jpg/sim_mult_30.jpg' width='712' height='285' alt=''><img id='simLens31' src='simulations/sim_mult_jpg/sim_mult_31.jpg' width='712' height='285' alt=''><img id='simLens32' src='simulations/sim_mult_jpg/sim_mult_32.jpg' width='712' height='285' alt=''><img id='simLens33' src='simulations/sim_mult_jpg/sim_mult_33.jpg' width='712' height='285' alt=''><img id='simLens34' src='simulations/sim_mult_jpg/sim_mult_34.jpg' width='712' height='285' alt=''><img id='simLens35' src='simulations/sim_mult_jpg/sim_mult_35.jpg' width='712' height='285' alt=''><img id='simLens36' src='simulations/sim_mult_jpg/sim_mult_36.jpg' width='712' height='285' alt=''><img id='simLens37' src='simulations/sim_mult_jpg/sim_mult_37.jpg' width='712' height='285' alt=''><img id='simLens38' src='simulations/sim_mult_jpg/sim_mult_38.jpg' width='712' height='285' alt=''><img id='simLens39' src='simulations/sim_mult_jpg/sim_mult_39.jpg' width='712' height='285' alt=''><img id='simLens40' src='simulations/sim_mult_jpg/sim_mult_40.jpg' width='712' height='285' alt=''><img id='simLens41' src='simulations/sim_mult_jpg/sim_mult_41.jpg' width='712' height='285' alt=''><img id='simLens42' src='simulations/sim_mult_jpg/sim_mult_42.jpg' width='712' height='285' alt=''><img id='simLens43' src='simulations/sim_mult_jpg/sim_mult_43.jpg' width='712' height='285' alt=''><img id='simLens44' src='simulations/sim_mult_jpg/sim_mult_44.jpg' width='712' height='285' alt=''><img id='simLens45' src='simulations/sim_mult_jpg/sim_mult_45.jpg' width='712' height='285' alt=''><img id='simLens46' src='simulations/sim_mult_jpg/sim_mult_46.jpg' width='712' height='285' alt=''><img id='simLens47' src='simulations/sim_mult_jpg/sim_mult_47.jpg' width='712' height='285' alt=''><img id='simLens48' src='simulations/sim_mult_jpg/sim_mult_48.jpg' width='712' height='285' alt=''><img id='simLens49' src='simulations/sim_mult_jpg/sim_mult_49.jpg' width='712' height='285' alt=''><img id='simLens50' src='simulations/sim_mult_jpg/sim_mult_50.jpg' width='712' height='285' alt=''>",
			"symfony": "<img id='simLens1' src='simulations/sim_symf_jpg/sim_symf_1.jpg' width='712' height='285' alt='' style='visibility:visible;'><img id='simLens2' src='simulations/sim_symf_jpg/sim_symf_2.jpg' width='712' height='285' alt=''><img id='simLens3' src='simulations/sim_symf_jpg/sim_symf_3.jpg' width='712' height='285' alt=''><img id='simLens4' src='simulations/sim_symf_jpg/sim_symf_4.jpg' width='712' height='285' alt=''><img id='simLens5' src='simulations/sim_symf_jpg/sim_symf_5.jpg' width='712' height='285' alt=''><img id='simLens6' src='simulations/sim_symf_jpg/sim_symf_6.jpg' width='712' height='285' alt=''><img id='simLens7' src='simulations/sim_symf_jpg/sim_symf_7.jpg' width='712' height='285' alt=''><img id='simLens8' src='simulations/sim_symf_jpg/sim_symf_8.jpg' width='712' height='285' alt=''><img id='simLens9' src='simulations/sim_symf_jpg/sim_symf_9.jpg' width='712' height='285' alt=''><img id='simLens10' src='simulations/sim_symf_jpg/sim_symf_10.jpg' width='712' height='285' alt=''><img id='simLens11' src='simulations/sim_symf_jpg/sim_symf_11.jpg' width='712' height='285' alt=''><img id='simLens12' src='simulations/sim_symf_jpg/sim_symf_12.jpg' width='712' height='285' alt=''><img id='simLens13' src='simulations/sim_symf_jpg/sim_symf_13.jpg' width='712' height='285' alt=''><img id='simLens14' src='simulations/sim_symf_jpg/sim_symf_14.jpg' width='712' height='285' alt=''><img id='simLens15' src='simulations/sim_symf_jpg/sim_symf_15.jpg' width='712' height='285' alt=''><img id='simLens16' src='simulations/sim_symf_jpg/sim_symf_16.jpg' width='712' height='285' alt=''><img id='simLens17' src='simulations/sim_symf_jpg/sim_symf_17.jpg' width='712' height='285' alt=''><img id='simLens18' src='simulations/sim_symf_jpg/sim_symf_18.jpg' width='712' height='285' alt=''><img id='simLens19' src='simulations/sim_symf_jpg/sim_symf_19.jpg' width='712' height='285' alt=''><img id='simLens20' src='simulations/sim_symf_jpg/sim_symf_20.jpg' width='712' height='285' alt=''><img id='simLens21' src='simulations/sim_symf_jpg/sim_symf_21.jpg' width='712' height='285' alt=''><img id='simLens22' src='simulations/sim_symf_jpg/sim_symf_22.jpg' width='712' height='285' alt=''><img id='simLens23' src='simulations/sim_symf_jpg/sim_symf_23.jpg' width='712' height='285' alt=''><img id='simLens24' src='simulations/sim_symf_jpg/sim_symf_24.jpg' width='712' height='285' alt=''><img id='simLens25' src='simulations/sim_symf_jpg/sim_symf_25.jpg' width='712' height='285' alt=''><img id='simLens26' src='simulations/sim_symf_jpg/sim_symf_26.jpg' width='712' height='285' alt=''><img id='simLens27' src='simulations/sim_symf_jpg/sim_symf_27.jpg' width='712' height='285' alt=''><img id='simLens28' src='simulations/sim_symf_jpg/sim_symf_28.jpg' width='712' height='285' alt=''><img id='simLens29' src='simulations/sim_symf_jpg/sim_symf_29.jpg' width='712' height='285' alt=''><img id='simLens30' src='simulations/sim_symf_jpg/sim_symf_30.jpg' width='712' height='285' alt=''><img id='simLens31' src='simulations/sim_symf_jpg/sim_symf_31.jpg' width='712' height='285' alt=''><img id='simLens32' src='simulations/sim_symf_jpg/sim_symf_32.jpg' width='712' height='285' alt=''><img id='simLens33' src='simulations/sim_symf_jpg/sim_symf_33.jpg' width='712' height='285' alt=''><img id='simLens34' src='simulations/sim_symf_jpg/sim_symf_34.jpg' width='712' height='285' alt=''><img id='simLens35' src='simulations/sim_symf_jpg/sim_symf_35.jpg' width='712' height='285' alt=''><img id='simLens36' src='simulations/sim_symf_jpg/sim_symf_36.jpg' width='712' height='285' alt=''><img id='simLens37' src='simulations/sim_symf_jpg/sim_symf_37.jpg' width='712' height='285' alt=''><img id='simLens38' src='simulations/sim_symf_jpg/sim_symf_38.jpg' width='712' height='285' alt=''><img id='simLens39' src='simulations/sim_symf_jpg/sim_symf_39.jpg' width='712' height='285' alt=''><img id='simLens40' src='simulations/sim_symf_jpg/sim_symf_40.jpg' width='712' height='285' alt=''><img id='simLens41' src='simulations/sim_symf_jpg/sim_symf_41.jpg' width='712' height='285' alt=''><img id='simLens42' src='simulations/sim_symf_jpg/sim_symf_42.jpg' width='712' height='285' alt=''><img id='simLens43' src='simulations/sim_symf_jpg/sim_symf_43.jpg' width='712' height='285' alt=''><img id='simLens44' src='simulations/sim_symf_jpg/sim_symf_44.jpg' width='712' height='285' alt=''><img id='simLens45' src='simulations/sim_symf_jpg/sim_symf_45.jpg' width='712' height='285' alt=''><img id='simLens46' src='simulations/sim_symf_jpg/sim_symf_46.jpg' width='712' height='285' alt=''><img id='simLens47' src='simulations/sim_symf_jpg/sim_symf_47.jpg' width='712' height='285' alt=''><img id='simLens48' src='simulations/sim_symf_jpg/sim_symf_48.jpg' width='712' height='285' alt=''><img id='simLens49' src='simulations/sim_symf_jpg/sim_symf_49.jpg' width='712' height='285' alt=''><img id='simLens50' src='simulations/sim_symf_jpg/sim_symf_50.jpg' width='712' height='285' alt=''>"
		};*/
		//!steal-remove-end

	} //Ende anlegenDaten


}); //Ende Controller

export default AmoDatenanbieter;