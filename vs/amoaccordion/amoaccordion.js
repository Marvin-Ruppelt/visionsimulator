import $ from "jquery";
import can from "can";


export const AmoAccordion = can.Control.extend("Amo.Accordion", {
	defaults: {
		accordionThema: ''
	}
},
{
	init: function( element, options ) {

	},
	'a.ui-collapsible-heading-toggle click': function( el, ev ) {
		var accordionSeiteEl = el.closest('.ui-collapsible');

		ev.preventDefault();
		ev.stopPropagation();

		steal.dev.log("Accordion-Click " + accordionSeiteEl.data('collapsbox'));

		//this.element.css("outline","4px dashed blue");

		//this.element.find('.amoAccordionOffen').removeClass('amoAccordionOffen');
		this.element.children('.amoAccordionOffen').removeClass('amoAccordionOffen');

		accordionSeiteEl.addClass('amoAccordionOffen');

		this.options.accordionThema( accordionSeiteEl.data('collapsbox') );
		//this.options.accordionThema.attr("status", accordionSeiteEl.data('collapsbox') );
	}
}); //Ende Control

export default AmoAccordion;