import $ from "jquery";
import can from "can";

//import './aminit.js'; // nicht mehr nötig??

import './resources/store';
import AMODatenanbieter from './datenanbieter/datenanbieter';
import AMOSteuerung from './steuerung/steuerung';
import AMOVorladen from './vorladen/vorladen';
import AMOFullscreen from './fullscreen/fullscreen';
import AMOBrowsertest from './browsertest/browsertest';
import AMOSeitenSteuerung from './seitensteuerung/seitensteuerung';
import AMOAccordion from './amoaccordion/amoaccordion'
import './slider/slider';
import './resources/jquerymobile/less/themes/default/jquery.mobile.less!';

var drechslerBrowsertest 
		, drechslerFullscreen
		, drechslerVorladen
		, amoSteuerung
		, amoDatenanbieter
		, AmoSzenenauswahl
		, amoSeitensteuerung
		, seitenContainer
		, seitenCompute
		, eyeWorksAccordion
		, eyeWorksCompute
		, eyeWorksObserve
		, iolAccordionHaupt
		, iolAccordionHauptCompute
		, iolLightAccordion
		, iolLightAccordionCompute
		, templateSlider
		, progress
		, sliderEingabe
		, templateSliderIOLOptions
		, progressioloptions
		, progressvision
		, templateSliderVision
		, progressastigmatism
		, templateSliderAstigmatism
		, skalierFaktor
		, bereit
		, derBody = $('body')
		, szenenAuswahlEl = $('#szenenAuswahl');

$(document).ready(function() {
	console.log("document ready bereit: " );

	if( ie8 !== true )
	{
		console.log("not ie8");

		if( appStateCounter > -1 )
		{
			appStateCounter += 1;
		}
	}


	if ( !bereitsFertig ) { 
		//bereit = true;
		derBody.trigger('bereit'); 
		bereitsFertig = true;
		console.log("document ready bereit: ");
	}
	
	console.log('document ready Event appStateCounter ' + appStateCounter);
	bereit = true;
});
	
	
$(window).load(function() {
	console.log("load bereit: " + bereit); 

	if ( ie8 === true )
	{

		if ( appStateCounter > -1 )
		{
			appStateCounter += 1;
		}

	}
	
	if ( !bereitsFertig ) { 
		derBody.trigger('bereit');
		bereitsFertig = true;
	}
	console.log('load Event appStateCounter ' + appStateCounter);

	bereit = true;
});

derBody.on("bereit", function(){
	//bereitsFertig = true;
	console.log("Body bereit ----- ");
});

//Computes --------------------------

progress = can.compute(1, function( newVal, oldVal) {

		return newVal;

});

/*sliderEingabe = can.compute(0, function( newVal, oldVal){

	if ( newVal < 1 )
	{
		return 0;
	}
	else 
	{
		return newVal;
	}
});*/

progressioloptions = can.compute(1, function( newVal, oldVal) {

		return newVal;

});

progressvision = can.compute(1, function( newVal, oldVal) {

		return newVal;

});


progressastigmatism = can.compute(1, function( newVal, oldVal) {

	if ( newVal < 1 )
	{
		return 1;
	}
	else 
	{
		return newVal;
	}
});

seitenCompute = can.compute("#startseite");
eyeWorksCompute = can.compute("normalvision");
//eyeWorksObserve = new can.Observe({status: "normalvision"});

iolAccordionHauptCompute = can.compute("eins");
iolLightAccordionCompute = can.compute("monolens");

if ( webApp === true && desktopApp === false )
{
	drechslerBrowsertest = new AMOBrowsertest( derBody );
	drechslerFullscreen = new AMOFullscreen( derBody );
	drechslerVorladen = new AMOVorladen( derBody );
}

/* üä?ß将棋（しょう */
amoDatenanbieter = new AMODatenanbieter( derBody, {
	seitenCompute: seitenCompute
});

amoSteuerung = new AMOSteuerung( derBody, {
	amoDatenAktuell: amoDatenanbieter.ausgebenDaten('amoDatenAktuell'),
	laenderVersionen: amoDatenanbieter.ausgebenDaten('laenderVersionen'),
	amoStatus: amoDatenanbieter.ausgebenDaten('amoStatus'),
	simulatorBilder: amoDatenanbieter.ausgebenDaten('simulatorBilder'),
	simImages: amoDatenanbieter.ausgebenDaten('simImages'),
	seitenCompute: seitenCompute,
	eyeWorksCompute: eyeWorksCompute,
	iolLightAccordionCompute: iolLightAccordionCompute,
	progress: progress,
	progressioloptions: progressioloptions,
	progressastigmatism: progressastigmatism,
	progressvision: progressvision
});

amoSteuerung.eingebenDaten("progress", progress);



if ( webApp === true)
{
	seitenContainer = '#webAppContainer';
}
else
{
	seitenContainer = '#pagecontainer';
}

//Seiten-Wechsel-Modul
amoSeitensteuerung = new AMOSeitenSteuerung({
	"seitenContainerSelector" : seitenContainer,
	"seiten": ["#startseite","#welcomeseite","#eyeworksseite","#ioloptionsseite","#visionsimulatorseite"],
	"seitenCompute": seitenCompute
});


//Eye-Works-Accordion
eyeWorksAccordion = new AMOAccordion( $('#eyeExamplesContainer .amoAccordion'),{
	accordionThema: eyeWorksCompute
});

//Iol-Options-Accordion
iolAccordionHaupt = new AMOAccordion( $('#iolAccordionHaupt'),{
	accordionThema: iolAccordionHauptCompute
});

iolLightAccordion = new AMOAccordion( $('#amoAccordion2'),{
	accordionThema: iolLightAccordionCompute
}); /**/



//Slider EyeWorks initialisieren --------------
skalierFaktor = 1;


/*progress.bind("change", function(ev, newVal, oldVal){
	//steal.dev.log("XXX---XXX " + newVal);
	amoSteuerung.eingebenDaten("progresseyeworks", newVal);

});*/



templateSlider = can.stache("<slider id='iolslider' {(sliderwert)}='~sliderwert' {slidereingabe}='slidereingabe' skalierfaktor='skalierfaktor' {schritte}='schritte' {sliderstrecke}='sliderStrecke'>{{{meinTemplate}}}</slider>");

$('#sliderContainer-1').html( templateSlider({
	sliderwert: progress,
	slidereingabe: sliderEingabe,
	skalierfaktor: skalierFaktor,
	sliderStrecke: 712,
	schritte: 50,
	//meinTemplate: ''
	meinTemplate: '<div class="ui-slider"><div role="application" class="ui-slider-track ui-btn-down-j ui-btn-corner-all scalebox"><span class="ui-slider-handle ui-btn ui-shadow ui-btn-corner-all ui-btn-up-j slidergriff" data-corners="true" data-shadow="true" data-iconshadow="true" data-wrapperels="span" data-theme="j"  aria-valuemax="50"  style="left: 0%;"></span></div></div>'
}) );

//Slider IOL-Options initialisieren --------------



/*progressioloptions.bind("change", function(ev, newVal, oldVal){
	//steal.dev.log("XXX---XXX " + newVal);
	amoSteuerung.eingebenDaten("progressioloptions", newVal);

});*/

templateSliderIOLOptions = can.stache("<slider {(sliderwert)}='~sliderwert' {slidereingabe}='slidereingabe' skalierfaktor='skalierfaktor' {schritte}='schritte' {sliderstrecke}='sliderStrecke'>{{{meinTemplate}}}</slider>");

$('#sliderContainer-2').html( templateSlider({
	sliderwert: progressioloptions,
	slidereingabe: sliderEingabe,
	skalierfaktor: skalierFaktor,
	sliderStrecke: 712,
	schritte: 50,
	//meinTemplate: ''
	meinTemplate: '<div class="ui-slider"><div role="application" class="ui-slider-track ui-btn-down-j ui-btn-corner-all scalebox"><span class="ui-slider-handle ui-btn ui-shadow ui-btn-corner-all ui-btn-up-j slidergriff" data-corners="true" data-shadow="true" data-iconshadow="true" data-wrapperels="span" data-theme="j"  aria-valuemax="50"  style="left: 0%;"></span></div></div>'
}) );


//Slider Vision-Simulator initialisieren --------------

/*progressvision.bind("change", function(ev, newVal, oldVal){
	//steal.dev.log("XXX---XXX " + newVal);
	amoSteuerung.eingebenDaten("progressvision", newVal);

});*/

templateSliderVision = can.stache("<slider {(sliderwert)}='~sliderwert' {slidereingabe}='slidereingabe' skalierfaktor='skalierfaktor' {schritte}='schritte' {sliderstrecke}='sliderStrecke'>{{{meinTemplate}}}</slider>");

$('#simSliderBlock').append( templateSliderVision({
	sliderwert: progressvision,
	slidereingabe: sliderEingabe,
	skalierfaktor: skalierFaktor,
	sliderStrecke: 958,
	schritte: 239,
	//meinTemplate: ''
	meinTemplate: '<div class="ui-slider"><div role="application" class="ui-slider-track ui-btn-down-j ui-btn-corner-all scalebox"><span class="ui-slider-handle ui-btn ui-shadow ui-btn-corner-all ui-btn-up-j slidergriff" data-corners="true" data-shadow="true" data-iconshadow="true" data-wrapperels="span" data-theme="j"  aria-valuemax="50"  style="left: 0%;"></span></div></div>'
}) );


//Slider Astigmatism initialisieren --------------

templateSliderAstigmatism = can.stache("<span id='astigcover'></span><slider {(sliderwert)}='~sliderwert' {slidereingabe}='slidereingabe' skalierfaktor='skalierfaktor' {schritte}='schritte' {sliderstrecke}='sliderStrecke'>{{{meinTemplate}}}</slider>");

$('#astigSliderBlock').html( templateSliderAstigmatism({
	sliderwert: progressastigmatism,
	slidereingabe: sliderEingabe,
	skalierfaktor: skalierFaktor,
	sliderStrecke: 190,
	schritte: 5,
	//meinTemplate: ''
	meinTemplate: '<div class="ui-slider"><div role="application" class="ui-slider-track ui-btn-down-j ui-btn-corner-all scalebox"><span class="ui-slider-handle ui-btn ui-shadow ui-btn-corner-all ui-btn-up-j slidergriff" data-corners="true" data-shadow="true" data-iconshadow="true" data-wrapperels="span" data-theme="j"  aria-valuemax="5"  style="left: 0%;"></span></div></div>'
}) );

//derBody.trigger('bereit'); 