steal(
	//'can/util/jquery/jquery.js', 
	'can/control/control.js'
	, 'can/util/jquery/jquery.js'
	, 'resources/fullscreen/jquery.fullscreen-min.js'
	, function() {

var DrechslerFullscreen = can.Control("Drechsler.Fullscreen", {
		defaults: {
			klasseFullScreenUnterstuetzung: "fullscreenNativ",
			klasseFullScreenEin: "fullscreenEin",
			idFullScreenButton: "fullscreenButton"
		}
	}, 
	{
		init: function( element , options ) {
			var self = this;
			this.dasHtmlEl = $('html');

			this.pruefenFullScreenUnterstuetzung();

		},
		pruefenFullScreenUnterstuetzung: function(){
			if ( $(document).fullScreen() != null )
			{
				this.dasHtmlEl.addClass( this.options.klasseFullScreenUnterstuetzung );
			}
		},
		'#fullscreenButton click': function( el, ev ){
			this.dasHtmlEl.toggleClass( this.options.klasseFullScreenEin );
			$(document).toggleFullScreen();
		}

	}); //Ende Controller

}); 