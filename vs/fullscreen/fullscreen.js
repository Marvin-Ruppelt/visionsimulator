import $ from "jquery";
import can from "can";

import 'vs/resources/fullscreen/jquery.fullscreen-min';


export const DrechslerFullscreen = can.Control.extend("Drechsler.Fullscreen", {
		defaults: {
			klasseFullScreenUnterstuetzung: "fullscreenNativ",
			klasseFullScreenEin: "fullscreenEin",
			idFullScreenButton: "fullscreenButton"
		}
	}, 
	{
		init: function( element , options ) {
			var self = this, nodeGui;
			this.dasHtmlEl = $('html');

			if ( desktopApp === true )
			{
				this.umschaltenFullScreen = this.umschaltenFullScreenDesktop;
				//nodeGui = global.window.nwDispatcher.requireNwGui();
				nodeGui = require('nw.gui');
				this.nodeWindow = nodeGui.Window.get();
			}
			else
			{
				this.umschaltenFullScreen = this.umschaltenFullScreenWeb;
			}

			this.pruefenFullScreenUnterstuetzung();

		},
		pruefenFullScreenUnterstuetzung: function(){
			if ( desktopApp === false )
			{

				if ( $(document).fullScreen() != null )
				{
					this.dasHtmlEl.addClass( this.options.klasseFullScreenUnterstuetzung );
				}
			}
			else
			{
				this.dasHtmlEl.addClass( this.options.klasseFullScreenUnterstuetzung );
			}
		},
		umschaltenFullScreen: function(){

		},
		umschaltenFullScreenWeb: function(){
			this.dasHtmlEl.toggleClass( this.options.klasseFullScreenEin );
			$(document).toggleFullScreen();
		},
		umschaltenFullScreenDesktop: function(){
			console.log("umschaltenFullScreenDesktop");
			this.dasHtmlEl.toggleClass( this.options.klasseFullScreenEin );
			this.nodeWindow.toggleFullscreen();
		},
		'#fullscreenButton click': function( el, ev ){
			this.umschaltenFullScreen();
		}

	}); //Ende Controller

export default DrechslerFullscreen;