import $ from "jquery";
import can from "can";

import 'vs/resources/jquery-browser-plugin/jquery.browser';
	

export const DrechslerBrowsertest = can.Control.extend("Drechsler.Browsertest", {
		defaults: {
			klasseAlterBrowser: "oldBrowser"
		}
	}, 
	{
		init: function( element , options ) {
			var self = this;
			this.dasHtmlEl = $('html');
			//this.itunesHinweisEl = $('#itunesHinweis');
			//this.itunesHinweisAppVerweisEl = $('#itunesHinweisAppVerweis');
			//this.itunesHinweisCancelBoxEl = $('#itunesHinweisCancelBox');
			this.alterBrowser = false;
			//JDconsole.log('browsertest');

			//browserKuerzel: msie, mozilla, chrome, safari
			this.browserKuerzel = '';

			if ( $.browser.chrome === true )
			{
				this.browserKuerzel = 'chrome';
			}
			else if ( $.browser.mozilla === true )
			{
				this.browserKuerzel = 'mozilla';
			}
			else if ( $.browser.msie === true )
			{
				this.browserKuerzel = 'msie';
			}
			else if ( $.browser.safari === true )
			{
				this.browserKuerzel = 'safari';
			}
			else if ( $.browser.opr === true )
			{
				this.browserKuerzel = 'opr';
			}
			else
			{
				this.browserKuerzel = 'unknown';
			}
			
			this.browserVersion = $.browser.version;

			this.pruefenIstAlterBrowser();
			this.setzenBrowserKlasse();

			if ( bereitsFertig === true )
			{
				this.pruefenIstIPad();
			}
			
		},
		pruefenIstAlterBrowser: function(){
			//browserVersionMin: minimale Version, die noch nicht als alt gilt

			switch ( this.browserKuerzel ) 
				{
					case 'chrome':
						if ( this.browserVersion < 27 ) { this.alterBrowser = true; }
					break;
					case 'mozilla':
						if ( this.browserVersion < 22 ) { this.alterBrowser = true; }
					break;
					case 'safari':
						if ( this.browserVersion < 5.1 ) { this.alterBrowser = true; }
					break;
					case 'opr':
						if ( this.browserVersion < 27 ) { this.alterBrowser = true; }
					break;
					case 'msie':
						if ( this.browserVersion < 10 ) 
							{
								altIE = true;
								this.alterBrowser = true;
							}
					break;
				}

		},
		setzenBrowserKlasse: function(){
			if ( this.alterBrowser === true ) 
			{
				this.dasHtmlEl.addClass( this.options.klasseAlterBrowser );
			}

			if ( $.browser.mobile || $.browser.android )
			{
				this.dasHtmlEl.addClass("mobile");
			}
		},
		pruefenIstIPad: function(){
			if ( $.browser.ipad === true )
			{
				//itunes-App-Hinweis anzeigen
				//this.itunesHinweisAppVerweisEl.html('<iframe src="https://banners.itunes.apple.com/banner.html?partnerId=&aId=&id=622926806&c=us&l=en-US&bt=catalog&t=catalog_white&w=300&h=250" frameborder=0 style="overflow-x:hidden;overflow-y:hidden;width:300px;height:250px;border:0px"></iframe>');
				$('#itunesHinweisAppVerweis').html('<iframe src="https://banners.itunes.apple.com/banner.html?partnerId=&aId=&id=622926806&c=us&l=en-US&bt=catalog&t=catalog_white&w=300&h=250" frameborder=0 style="overflow-x:hidden;overflow-y:hidden;width:300px;height:250px;border:0px"></iframe>');
				
				this.dasHtmlEl.addClass("iPad");
			}
		},
		' bereit': function( el, event ) {
			this.pruefenIstIPad();
		},
		'#itunesHinweisCancelBox click': function( el, event ) {

			this.dasHtmlEl.removeClass("iPad");
		}


	}); //Ende Controller

export default DrechslerBrowsertest;