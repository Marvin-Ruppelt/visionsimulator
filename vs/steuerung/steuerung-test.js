steal(
	//'can/util/jquery/jquery.js', 
	'can/control/control.js'
	, 'can/view'
  	, 'can/view/ejs'
  	, 'can/view/modifiers',
  	'can/observe/observe.js', 
'can/observe/list/list.js',
'can/observe/delegate/delegate.js'
	).then(
	'amo/steuerung/emptytemplate.ejs',
	function() {

	var AmoSteuerung, steuer, AmoDatenanbieter;

	AmoSteuerung = can.Control("Amo.Steuerung", {
		defaults: {
			amoDatenAktuell: false
		}
	}, 
	{
		init: function( element , options ) {
			steal.dev.log('Steuerung geladen - ' );
			var self = this;
			this.dasEl = $('.test');

			//die Länderauswahl für das Select-Menü zusammenstellen
			//this.aktualisierenLanderAuswahl();

			//this.dasEl.html('amo/steuerung/emptytemplate.ejs', { amoDatenAktuell: this.options.amoDatenAktuell });

			this.dasEl.each(function(){
				$(this).html('amo/steuerung/emptytemplate.ejs', { amoDatenAktuell: self.options.amoDatenAktuell });
			});
		},
		aktualisierenLanderAuswahl: function() {
			this.countrySelectEl.html('amo/steuerung/countryselect.ejs', {
				laenderVersionen: this.options.laenderVersionen
			});
		},
		'click': function(el, ev) {
			//steal.dev.log('Country change ' + el.val() );

			this.element.trigger('landgewaehlt', 'usa' );
		},
		'{amoDatenAktuell} sprachVariante': function( obj, ev, attr, how, newVal, oldVal ) {
			steal.dev.log('amoDatenAktuell-Sprachvariante aktualisiert');
		}
	}); //Ende Controller

AmoDatenanbieter = can.Control("Amo.Datenanbieter", {
	defaults: {
		test: 'test'
	}
}, {
	init: function( element , options ) {
		steal.dev.log('Amo.Datenanbieter geladen');
		var that = this; //Referenz auf Controller

		//Daten anlegen und verfügbar machen
		//noch statisch
		this.anlegenDaten();

		//Daten für die aktuelle Sprachversion laden
		//this.festlegenLaenderVersion( 'deutschland' );
		this.aktualisierenLaenderVersion( 'deutschland' );
	},
	ausgebenDaten: function( datenName ) {
		return this[datenName];
	},
	eingebenDaten: function( datenName, eingabeWert ){
		this[datenName] = eingabeWert;
	},
	aktualisierenLaenderVersion: function( dasLand ) {
		this.amoDatenAktuell.attr('laenderVersion', this.laenderVersionen[dasLand]);
		this.amoDatenAktuell.attr('sprachVariante', this.sprachVarianten[ this.laenderVersionen[dasLand].sprache ]);
		this.amoDatenAktuell.attr('counter', Math.random() );
	},
	'clack': function() {
		this.aktualisierenLaenderVersion( 'usa' );
	},
	' landgewaehlt': function( el, event, land ) {
		steal.dev.log('landgewaehlt ' + land);

		this.aktualisierenLaenderVersion( land );
		steal.dev.log( this.amoDatenAktuell.sprachVariante.isiLink);
	},
	anlegenDaten: function() {
		var self = this;

		this.amoDatenAktuell = new can.Observe({
			version: 12,
			counter: 1,
			laenderVersion: {ref: 'nichts'},
			sprachVariante: {ref: 'nichts'}
		});

		this.laenderVersionen = {
			'deutschland': {
				'ref': "deutschland",
				'sprache': "deutsch",
				'auswahl': "Deutschland"
				},
			'usa': {
				'ref': "usa",
				'sprache': "english",
				'auswahl': "U. S. (english)"
				}
		};

		this.sprachVarianten = {
			'deutsch': {
				'ref': "deutsch",
				'isiLink': "Wichtige Information zur Sicherheit"
				},
			'english': {
				'ref': "english",
				'isiLink': "Important Safety Information"
				}
		};
	}
	
}); //Ende Controller

	amoDatenanbieter = new Amo.Datenanbieter( $('body') );

	steuer = new Amo.Steuerung( $('body'), {
		amoDatenAktuell: amoDatenanbieter.ausgebenDaten('amoDatenAktuell') }
		);

	steal.dev.log('etwas geladen');
}); 