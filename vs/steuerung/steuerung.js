import $ from "jquery";
import can from "can";

//import 'can/control/control.js';
	//, 'can/view/view.js'
//import 'can/view/stache/stache.js';
  	//, 'can/view/modifiers/modifiers.js'

//import emptyTemplateStache from 'amo/steuerung/emptytemplate.stache!';
import emptyTemplateNeuStache from 'amo/steuerung/emptyTemplateNeu.stache!';
import countrySelectStache from 'amo/steuerung/countryselect.stache!';
import termsHeaderStache from 'amo/steuerung/termsHeader.stache!';
import termsStache from 'amo/steuerung/terms.stache!';
import updateBlockStache from 'amo/steuerung/updateblock.stache!';
import acceptButtonStache from 'amo/steuerung/acceptbutton.stache!';
import popupPasswortTextStache from 'amo/steuerung/popuppassworttext.stache!';
import popupPasswortButtonCancelStache from 'amo/steuerung/popuppasswortbuttoncancel.stache!';
import popupPasswortButtonEnterStache from 'amo/steuerung/popuppasswortbuttonenter.stache!';
import emptyTemplateIsiTopStache from 'amo/steuerung/emptytemplateIsiTop.stache!';
import emptyTemplateIsiBlockStache from 'amo/steuerung/emptytemplateIsiBlock.stache!';
import linsenContainerStache from 'amo/steuerung/linsencontainer.stache!';
import iolOptionsButtonContainerStache from 'amo/steuerung/iolOptionsButtonContainer.stache!';

import backToStartStache from 'amo/steuerung/backToStart.stache!';
import welcomeHeaderStache from 'amo/steuerung/welcomeHeader.stache!';
import menuStartTourStache from 'amo/steuerung/menuStartTour.stache!';
import welcomeTextStache from 'amo/steuerung/welcomeText.stache!';
import boxEyeWorksStache from 'amo/steuerung/boxEyeWorks.stache!';
import boxIOLOptionsStache from 'amo/steuerung/boxIOLOptions.stache!';
import boxVisionSimulatorStache from 'amo/steuerung/boxVisionSimulator.stache!';

import menuMainMenuStache from 'amo/steuerung/menuMainMenu.stache!';
import eyeWorksTitelStache from 'amo/steuerung/eyeworksTitel.stache!';
import menuIolOptionsStache from 'amo/steuerung/menuIolOptions.stache!';
import accordionEyeWorksStache from 'amo/steuerung/accordionEyeWorks.stache!';
import emptyTemplateAccordionHeaderStache from 'amo/steuerung/emptytemplateAccordionHeader.stache!';
import simSliderLabelBlockStache from 'amo/steuerung/simSliderLabelBlock.stache!';
import simCaptionStufeStache from 'amo/steuerung/simCaptionStufe.stache!'
//import szenenBoxStache from 'amo/steuerung/szenenBox.stache!';

can.stache.registerSimpleHelper('abfragenSprachWert', function( sprachObj, str ){
	var result;

	result = sprachObj.attr(str);
    return result;
});

can.stache.registerSimpleHelper('abfragenSprachWertSync', function( sprachObj, status ){
	var result,syncStatus;
	syncStatus = status.attr("status");
	//syncStatus = status;
	result = sprachObj.attr(syncStatus);
	//result = sprachObj.attr("syncConnected");
    return result;

});

can.stache.registerSimpleHelper('abfragenSprachWertAccHead', function( sprachObj, str ){
	var result;

	result = sprachObj.attr(str);
    return result;
});

can.stache.registerSimpleHelper('accordionLinsenHelper', function( das, produkte, treatmentTable, inhalt ){
	var result, produktName;
	produktName = produkte.attr(Number(das));
	//result = treatmentTable.attr(produktName).rowHeader;
	result = treatmentTable.attr(produktName)[inhalt];
    return result;
});

can.stache.registerSimpleHelper('abfragenProduktKennungHelper', function( nummer, produkte ){
	var result, produktName;

	produktName = produkte.attr(Number(nummer));

    return produktName;
});

can.stache.registerSimpleHelper('abfragenProduktBezeichnungHelper', function( nummer, produkte, sprachVariante ){
	var result, produktName;

	produktName = produkte.attr(Number(nummer));

	result = sprachVariante.attr(produktName);
    return result;
});

can.stache.registerSimpleHelper('prop', function(context) {
    var argLen = arguments.length - 1,
        result = context,
        childKey,
        i;

    for (i = 1; i < argLen && (result instanceof can.Map); i++) {
        childKey = arguments[i];

        if (childKey === undefined) {
            return;
        }

        result = result.attr(childKey);
    }

    return result;
});

export const AmoSteuerung = can.Control.extend("Amo.Steuerung", {
		defaults: {
			amoDatenAktuell: false,
			laenderVersionen: false,
			amoStatus: false,
			simulatorBilder: false,
			eyeExamplesContainerEl: $('#eyeExamplesContainer'),
			amoAccordion2El: $('#amoAccordion2'),
			slider1InputContainerEl: $('#sliderContainer-1'),
			seitenCompute: false,
			eyeWorksCompute: 0,
			iolLightAccordionCompute: 0,
			progress: 0,
			progresseyeworks: 1,
			progressioloptions: 2,
			progressastigmatism: 3,
			progressvision: 4
		}
	},
	{
		init: function( element , options ) {
			steal.dev.log('Amo.Steuerung geladen');
			var self = this; //Referenz auf Steuerung-Controller

			this.imgSlider = true; //Wenn true, wird mit mehreren Bildern (img) beim Slider gearbeitet
			/*this.serverPfad = 'amo/';
				//!steal-remove-start
					this.serverPfad = '';
				//!steal-remove-end	*/

			this.serverPfad = '';

			//this.options.seitenCompute
			//this.options.accordionThema


			this.derBodyEl = $('body');
			//this.uiPageEls = $('.amoSeite');
			this.isiEls = $('.isi');
			this.countrySelectEl = $('#countrySelect');
			this.countrySelectAnzeigeEl = $('.countrySelectContainer span.amoButton');
			this.termsHeaderEl = $('#termsHeader');
			this.termsBlockEl = $('#termsBlock');
			this.updateBlockEl = $('#updateBlock');
			this.acceptButtonEl = $('#acceptButton');
			this.popupPasswortEl = $('#popupPasswort');
			//this.popupPasswortTextEl = $('#popupPasswortText');
			this.popupPasswortTextContainerEl = $('#popupPasswortTextContainer');
			this.popupPasswortEingabeEl = $('#popupPasswortEingabe');
			this.popupPasswortButtonCancelEl = $('#popupPasswortButtonCancel');
			this.popupPasswortButtonEnterEl = $('#popupPasswortButtonEnter');

			if ( webApp === true)
			{
				this.isiFooterEls = $('#festerFuss .footerIsi');
				this.derBodyEl.addClass('webApp');
			}
			else
			{
				//jetzt genau wie Web-App
				this.isiFooterEls = $('#festerFuss .footerIsi');
				//this.isiFooterEls = $('.footerIsi');
			}

			//Welcome-Seite
			this.backToStartEl = $('#backToStart');
			this.welcomeHeaderEl = $('#welcomeHeader');
			this.menuStartTourEl = $('#menuStartTour');
			this.welcomeTextEl = $('#welcomeText');

			this.boxEyeWorksEl = $('#boxEyeWorks');
			this.boxIOLOptionsEl = $('#boxIOLOptions');
			this.boxVisionSimulatorEl = $('#boxVisionSimulator');

			//How the Eye Works - Seite
			this.menuMainMenuEl = $('#menuMainMenu');
			this.eyeworksTitelEl = $('#eyeworksTitel');
			this.menuIolOptionsEl = $('#menuIolOptions');
			this.accordionEyeWorks1El = $('#accordionEyeWorks1');
			this.accordionEyeWorks1aEl = $('#accordionEyeWorks1a');
			this.accordionEyeWorks1bEl = $('#accordionEyeWorks1b');
			this.accordionEyeWorks2El = $('#accordionEyeWorks2');
			this.accordionEyeWorks3El = $('#accordionEyeWorks3');
			this.accordionEyeWorks4El = $('#accordionEyeWorks4');
			this.accordionEyeWorks5El = $('#accordionEyeWorks5');

			//IOL-Options-Seite
			this.menuEyeWorksEl = $('#menuEyeWorks');
			this.iolOptionsTitelEl = $('#iolOptionsTitel');
			this.menuVisionSimulatorEl = $('#menuVisionSimulator');
			this.iolAccordionHeader1El = $('#iolAccordionHeader1');
			this.iolAccordionHeader2El = $('#iolAccordionHeader2');
			this.linsenAccordionContainerEl = $('#linsenAccordion');
			this.accordionIol1El = $('#accordionIol1');
			this.accordionIol2El = $('#accordionIol2');
			this.accordionIol3El = $('#accordionIol3');
			this.accordionIol4El = $('#accordionIol4');
			this.accordionIol5El = $('#accordionIol5');
			this.accordionIol6El = $('#accordionIol6');

			//Vision-Simulator-Seite
			this.VisionSimulatorSeiteEl = $('#visionsimulatorseite');
			this.iolOptionsButtonContainerEl = $('#iolOptionsButtonContainer');
			this.menuIolOptions2El = $('#menuIolOptions2');
			this.astigmCheckBoxEl = $('#astigmCheckBox');
			this.availableOptionsEl = $('#availableOptions');
			this.simulationLabelEl = $('#simulationLabel');
			this.simSliderLabelBlockEl = $('#simSliderLabelBlock');
			this.simCaptionStufeEl = $('#simCaptionStufe');





			//Slider - Simulator
			this.slider1InputEl = $('#slider-1');
			this.simOptionsEl = $('#simVision'); //erstes Slider-Bild
			this.slider1Str = this.serverPfad + 'amo/simulations/sim_norm_jpg/sim_norm_';

			this.simVisionContainerEl = $('#simVisionContainer');
			this.simVisionImgEl1 = $('#simVision1');
			this.simVisionImgEl2 = $('#simVision50');


			this.slider2InputEl = $('#slider-2');
			this.simIolEl = $('#simIol'); //zweites Slider-Bild
			this.slider2Str = this.serverPfad + 'amo/simulations/sim_mono_jpg/sim_mono_';

			this.simLensesContainerEl = $('#simLensesContainer');
			this.simLensesImgEl1 = $('#simLens1');
			this.simLensesImgEl2 = $('#simLens50');


			this.slider4InputEl = $('#slider-4');

			this.simSliderBlockEl = $('#simSliderBlock');
			this.simCaptionEl = $('#simCaption');
			this.simCaptionStufeEl = $('#simCaptionStufe');

			this.slider3InputEl = $('#slider-3');

			this.szenenBoxEl = $('#simVisBox');

			this.zuweisenSliderSzenen();

			//Werte_________________
			this.fertigLayout = false;
			this.fertigEyeWorksSeite = false;
			this.fertigIoloptionsSeite = false;
			this.fertigVisionSimulatorSeite = false;
			this.sliderStufe = 1; //um zu steuern welche Bilder überhaupt sichtbar sein dürfen
			this.simProduktNr = 1; //Produkt-Nummer : 1-6;
			this.simBilderProduktSerienNr = 0; //Bilder-Serien-Index für den Bildaustausch : 0-3
			this.verfuegbareProdukte = [1,2,5,6];
			this.simAstigmatism = false;
			this.simBilderSerienNr = 0; //der Array-index für den Austausch der Simulator-Bilder; 0 ist ohne Astigmatismus
			this.astigmatismSliderWert = 1; //der Array-index, wenn Astigmatismus gewählt ist
			this.simSliderWert = 0;
			this.simSliderStufe = 1; //die Sektionen auf dem Slider : 1-7 ; bestimmt auch den Caption-Text
			this.aktuelleSprache = "usa";


			this.simVisionImgValue = 'normalvision'; //Vision-Lnses
			this.sliderWertSpeicherVision = 1;

			this.simVisionImgValue2 = 'monolens';
			this.sliderWertSpeicherLenses = 1;

			this.festlegenSzenenObjekt();

			//die Länderauswahl für das Select-Menü zusammenstellen
			this.aktualisierenLaenderAuswahl();

			//alle Texte zu Land und Sprache anpassen
			this.aktualisierenTexte();

			/*$('[data-role=collapsible]').bind('expand', function (event) {
			   $(this).find('h3').data('aufzu', 'auf');
			});
			$('[data-role=collapsible]').bind('collapse', function (event) {
			   $(this).find('h3').data('aufzu', 'zu');
			});*/
//this.slider4InputEl.css({display:'block',outline: '5px dotted red'});
			//$.event.special.expand = {}; //bringt nichts?

			//Bilder für den SimSlider einfügen
			if ( this.imgSlider )
			{
				steal.dev.log('init imgSlider updateSim1ImagesContainer');
				this.updateSim1ImagesContainer();
				this.updateSim2ImagesContainer();
			}

			/*steal.dev.log( this.options.eyeWorksCompute );
			this.options.progresseyeworks.bind('change', function(ev, neu, alt) {
				steal.dev.log('CCC ' + neu + alt);
			});*/
		},
		eingebenDaten: function( datenName, eingabeWert ){
			//this.options[datenName] = eingabeWert;

			switch ( datenName )
			{
				case 'progresseyeworks':
					this.changeProgresseyeworks( eingabeWert );
				break;
				case 'progressioloptions':
					this.changeProgressioloptions( eingabeWert );
				break;
				case 'progressvision':
					this.changeProgressvision( eingabeWert );
				break;
				case 'progressastigmatism':
					this.changeProgressastigmatism( eingabeWert );
				break;
				case 'progress':
					this.options.progress = eingabeWert;
				break;
			}

			//steal.dev.log("eingebenDaten " + datenName + ' ' + eingabeWert );
		},
		zuweisenSliderSzenen: function() {
			this.simVis1El = $('#simVis1');
			this.simVis2El = $('#simVis2');
			this.simVis3El = $('#simVis3');
			this.simVis4El = $('#simVis4');
			this.simVis5El = $('#simVis5');
			this.simVis6El = $('#simVis6');
			this.simVis7El = $('#simVis7');
			this.simVis8El = $('#simVis8');

			this.simVisAlleEls = $('.simVis');
			this.simVis2klEls = $('.simVis:nth-child(-n+2)');
			this.simVis2grEls = $('.simVis:nth-child(n+2)');
			this.simVis3klEls = $('.simVis:nth-child(-n+3)');
			this.simVis3grEls = $('.simVis:nth-child(n+3)');
			this.simVis4klEls = $('.simVis:nth-child(-n+4)');
			this.simVis4grEls = $('.simVis:nth-child(n+4)');
			this.simVis5klEls = $('.simVis:nth-child(-n+5)');
			this.simVis5grEls = $('.simVis:nth-child(n+5)');
			this.simVis6klEls = $('.simVis:nth-child(-n+6)');
			this.simVis6grEls = $('.simVis:nth-child(n+6)');
			this.simVis7klEls = $('.simVis:nth-child(-n+7)');
			this.simVis7grEls = $('.simVis:nth-child(n+7)');
		},
		festlegenSzenenObjekt: function() {
			var jetztDieSzene = 0,
				jetztSzenenOrdner = 'sim';

			if( this.options.amoStatus.dieSzene === 1 )
			{
				jetztDieSzene = 1;
				jetztSzenenOrdner = 'sim1';
			}
			else if( this.options.amoStatus.dieSzene === 2 )
			{
				jetztDieSzene = 2;
				jetztSzenenOrdner = 'sim2';
			}

			this.gewaehlteSzene = new can.Observe({
				dieSzene: jetztDieSzene,
				szenenOrdner: jetztSzenenOrdner
			});
		},
		aktualisierenLaenderAuswahl: function() {
			//steal.dev.log("ref " + this.options.laenderVersionen.argentinia.ref);
			//JDconsole.log('Länderauswahl aktualisiert ' + this.options.amoDatenAktuell.laenderVersion.ref);
			this.countrySelectEl.html( countrySelectStache( {
				laenderVersionen: this.options.laenderVersionen,
				laenderKuerzel: this.options.amoDatenAktuell.laenderVersion.ref
			}) );
		},
		' neuedaten': function( el, event, neueDaten ) {
			this.options.laenderVersionen = neueDaten.laenderVersionen;
			this.aktualisierenLaenderAuswahl();
			//alert(this.serverPfad);
			//JDconsole.log("neuedaten");
		},
		aktualisierenTexte: function() {
			var self = this;
//console.log("status da? " + this.options.amoStatus.status);
//console.log("status Sprache " + this.options.amoDatenAktuell.attr('sprachVariante').attr( 'ref' ) );
//console.log("status Sprache syncNotConnected  " + this.options.amoDatenAktuell.attr('sprachVariante').attr( 'syncNotConnected' ) );
//console.log("status Sprache " + this.options.amoDatenAktuell.attr('sprachVariante').attr(  this.options.amoStatus.attr('status')) );
			//Terms and Conditions
			this.termsHeaderEl.html( termsHeaderStache({ amoDatenAktuell: self.options.amoDatenAktuell }) );

			this.termsBlockEl.html( termsStache({ amoDatenAktuell: self.options.amoDatenAktuell }) );
/**/
			this.updateBlockEl.html( updateBlockStache({ amoDatenAktuell: self.options.amoDatenAktuell,
				amoStatus: self.options.amoStatus }) );

			this.acceptButtonEl.html(acceptButtonStache({ amoDatenAktuell: self.options.amoDatenAktuell }) );

			//Passwort-Popup
			this.popupPasswortTextContainerEl.html(popupPasswortTextStache({ amoDatenAktuell: self.options.amoDatenAktuell }) );

			this.popupPasswortButtonCancelEl.html(popupPasswortButtonCancelStache({ amoDatenAktuell: self.options.amoDatenAktuell }) );

			this.popupPasswortButtonEnterEl.html(popupPasswortButtonEnterStache({ amoDatenAktuell: self.options.amoDatenAktuell }) );

			//Welcome-Seite
			this.backToStartEl.html( backToStartStache({ amoDatenAktuell: self.options.amoDatenAktuell }) );

			this.welcomeHeaderEl.html( welcomeHeaderStache({ amoDatenAktuell: self.options.amoDatenAktuell }) );

			this.menuStartTourEl.html( menuStartTourStache({ amoDatenAktuell: self.options.amoDatenAktuell }) );

			this.welcomeTextEl.html( welcomeTextStache({ amoDatenAktuell: self.options.amoDatenAktuell }) );

			this.boxEyeWorksEl.html( boxEyeWorksStache({ amoDatenAktuell: self.options.amoDatenAktuell }) );

			this.boxIOLOptionsEl.html( boxIOLOptionsStache({ amoDatenAktuell: self.options.amoDatenAktuell }) );

			this.boxVisionSimulatorEl.html( boxVisionSimulatorStache({ amoDatenAktuell: self.options.amoDatenAktuell }) );

			//How the Eye Works - Seite
			this.menuMainMenuEl.html( menuMainMenuStache({ amoDatenAktuell: self.options.amoDatenAktuell }) );

			this.eyeworksTitelEl.html( eyeWorksTitelStache({ amoDatenAktuell: self.options.amoDatenAktuell }) );

			this.menuIolOptionsEl.html( menuIolOptionsStache({ amoDatenAktuell: self.options.amoDatenAktuell }) );

			this.accordionEyeWorks1El.replaceWith(accordionEyeWorksStache({ amoDatenAktuell: self.options.amoDatenAktuell, header: "accordionH1", block: "accordionB1", id: "accordionEyeWorks1", thema: "normalvision", collapsed: false }) );

			this.accordionEyeWorks1aEl.replaceWith(accordionEyeWorksStache({ amoDatenAktuell: self.options.amoDatenAktuell, header: "accordionH1a", block: "accordionB1a", id: "accordionEyeWorks1a", thema: "hyperopia", collapsed: "true" }) );

			this.accordionEyeWorks1bEl.replaceWith(accordionEyeWorksStache({ amoDatenAktuell: self.options.amoDatenAktuell, header: "accordionH1b", block: "accordionB1b", id: "accordionEyeWorks1b", thema: "myopia", collapsed: "true" }) );

			this.accordionEyeWorks2El.replaceWith(accordionEyeWorksStache({ amoDatenAktuell: self.options.amoDatenAktuell, header: "accordionH2", block: "accordionB2", id: "accordionEyeWorks2", thema: "astigmatism", collapsed: "true" }) );

			this.accordionEyeWorks3El.replaceWith(accordionEyeWorksStache( { amoDatenAktuell: self.options.amoDatenAktuell, header: "accordionH3", block: "accordionB3", id: "accordionEyeWorks3", thema: "presbyopia", collapsed: "true" }) );

			this.accordionEyeWorks4El.replaceWith(accordionEyeWorksStache( { amoDatenAktuell: self.options.amoDatenAktuell, header: "accordionH4", block: "accordionB4", id: "accordionEyeWorks4", thema: "cataract", collapsed: "true" }) );

			this.accordionEyeWorks5El.replaceWith(accordionEyeWorksStache( { amoDatenAktuell: self.options.amoDatenAktuell, header: "accordionH5", block: "accordionB5", id: "accordionEyeWorks5", thema: "cataractsurgery", collapsed: "true" }) );

			//IOL-Options-Seite
			this.linsenAccordionContainerEl.html(linsenContainerStache({ amoDatenAktuell: self.options.amoDatenAktuell }) );

			this.menuEyeWorksEl.html( emptyTemplateNeuStache({ amoDatenAktuell: self.options.amoDatenAktuell, datenname: "menuEyeWorks" }) );
			//this.menuEyeWorksEl.html( emptyTemplateNeuStache({ amoDatenAktuell: self.options.amoDatenAktuell,				inhalt: "menuEyeWorks"}) );

			this.iolOptionsTitelEl.html( emptyTemplateNeuStache({ amoDatenAktuell: self.options.amoDatenAktuell, datenname: "understandingIolHeader" }) );

			this.menuVisionSimulatorEl.html( emptyTemplateNeuStache({ amoDatenAktuell: self.options.amoDatenAktuell, datenname: "menuVisionSimulator" }) );

			this.iolAccordionHeader1El.html(emptyTemplateAccordionHeaderStache({ amoDatenAktuell: self.options.amoDatenAktuell, datenname: "understandingIolTableHeader1" }) );

			this.iolAccordionHeader2El.html( emptyTemplateAccordionHeaderStache({ amoDatenAktuell: self.options.amoDatenAktuell, datenname: "understandingIolSliderHeader" }) );


			this.accordionIol1El.replaceWith(accordionEyeWorksStache({ amoDatenAktuell: self.options.amoDatenAktuell, header: "UnderstandingIolAccordionH1", block: "UnderstandingIolAccordionB1", id: "accordionIol1", thema: "monolens", collapsed: false }) );

			this.accordionIol2El.replaceWith(accordionEyeWorksStache({ amoDatenAktuell: self.options.amoDatenAktuell, header: "UnderstandingIolAccordionH2", block: "UnderstandingIolAccordionB2", id: "accordionIol2", thema: "multilens", collapsed: "true" }) );

			this.accordionIol3El.replaceWith(accordionEyeWorksStache({ amoDatenAktuell: self.options.amoDatenAktuell, header: "UnderstandingIolAccordionH3", block: "UnderstandingIolAccordionB3", id: "accordionIol3", thema: "symfony", collapsed: "true" }) );

			this.accordionIol4El.replaceWith(accordionEyeWorksStache({ amoDatenAktuell: self.options.amoDatenAktuell, header: "UnderstandingIolAccordionH6", block: "UnderstandingIolAccordionB6", id: "accordionIol6", thema: "eyehance", collapsed: "true" }) );

			this.accordionIol5El.replaceWith(accordionEyeWorksStache({ amoDatenAktuell: self.options.amoDatenAktuell, header: "UnderstandingIolAccordionH4", block: "UnderstandingIolAccordionB4", id: "accordionIol4", thema: "synergy", collapsed: "true" }) );

			this.accordionIol6El.replaceWith(accordionEyeWorksStache({ amoDatenAktuell: self.options.amoDatenAktuell, header: "UnderstandingIolAccordionH5", block: "UnderstandingIolAccordionB5", id: "accordionIol5", thema: "sonata", collapsed: "true" }) );

			


			//Vision-Simulator-Seite
			this.iolOptionsButtonContainerEl.html( iolOptionsButtonContainerStache({ amoDatenAktuell: self.options.amoDatenAktuell }) );
			this.menuIolOptions2El.html(emptyTemplateNeuStache({ amoDatenAktuell: self.options.amoDatenAktuell, datenname: "menuIolOptions" }) );

			this.astigmCheckBoxEl.html(emptyTemplateNeuStache({ amoDatenAktuell: self.options.amoDatenAktuell, datenname: "iHaveAstigmatism" }) );

			this.availableOptionsEl.html(emptyTemplateNeuStache({ amoDatenAktuell: self.options.amoDatenAktuell, datenname: "AvailableIOLOptions" }) );

			this.simulationLabelEl.html(emptyTemplateNeuStache({ amoDatenAktuell: self.options.amoDatenAktuell, datenname: "Simulation" }) );

			this.simSliderLabelBlockEl.html(simSliderLabelBlockStache({ amoDatenAktuell: self.options.amoDatenAktuell}) );

			this.simCaptionStufeEl.html(simCaptionStufeStache({ amoDatenAktuell: self.options.amoDatenAktuell}) );

			//this.szenenBoxEl.html('amo/steuerung/szenenBox.ejs', { gewaehlteSzene: self.gewaehlteSzene});

			//Footer - Important Safety Information
			this.isiEls.each(function(){
				$(this).html(emptyTemplateIsiTopStache({ amoDatenAktuell: self.options.amoDatenAktuell }) );
			});

			this.isiFooterEls.each(function(){
				$(this).html(emptyTemplateIsiBlockStache({ amoDatenAktuell: self.options.amoDatenAktuell }) );
			});
/**/

			//this.zuweisenSliderSzenen();
		},
		austauschenBilderSimulator: function() {
			var self = this, bildPfad;

			steal.dev.log('serverPfad serverPfad ' + this.serverPfad);


			this.simVisAlleEls.each( function( index, element ) {
				bildPfad = self.serverPfad + 'amo/simulations/' + self.gewaehlteSzene.szenenOrdner + '/' + self.options.simulatorBilder[ self.simBilderSerienNr ][ self.simBilderProduktSerienNr ][ index ];

				$( element ).attr('src', bildPfad );
			});

         //Anpassung für Canada - DrJo
         if ( (this.aktuelleSprache === "englishcanada" || this.aktuelleSprache === "frenchcanada") && self.simBilderProduktSerienNr == 3)
         {
            this.simVis5El.attr('src', self.serverPfad + 'amo/simulations/' + self.gewaehlteSzene.szenenOrdner + '/' + 'tvs0_n_tcmono.jpg');
         }
		},
		'#countrySelect change': function(el, ev) {
			steal.dev.log('Country change ' + el.val() );
			ev.preventDefault();
			ev.stopPropagation();
			this.element.trigger('landgewaehlt', el.val() );
		},
		' landgewaehlt': function( el, event, land ) {
			this.anpassenFuerSprache( land );
		},
		anpassenFuerSprache: function( land ) {

				this.derBodyEl.removeClass( "sprache-" + this.aktuelleSprache );

				this.derBodyEl.addClass( "sprache-" + land );

				this.aktuelleSprache = land;

		},
		'#acceptButton click': function(el, ev) {
			if ( this.options.amoDatenAktuell.laenderVersion.passwort === false || this.options.amoStatus.passwortKontrolle === false )
			{
				//$.mobile.changePage('#welcomeseite');
				this.options.seitenCompute("#welcomeseite");
			}
			else
			{
				//Passwort-Abfrage
				//this.popupPasswortEl.popup( "open" );
				this.derBodyEl.toggleClass("passwortpopup", true);
				this.popupPasswortEingabeEl.val('');
				this.popupPasswortEingabeEl.focus();
			}
		},
		'.navLink, .navBox click': function(el, ev) {
			ev.preventDefault();
			this.options.seitenCompute( el.attr("href") );
		},
		'#popupPasswort popupafteropen': function(el, ev) {
			steal.dev.log('popup offen');
		},
		'#popupPasswortButtonCancel click': function(el, ev) {
			//this.popupPasswortEl.popup( "close" );
			this.derBodyEl.toggleClass("passwortpopup", false );
		},
		'#popupPasswortEingabe keyup': function(el, ev) {

			if ( ev.keyCode === 13 )
			{
				//steal.dev.log('enter');
				this.ueberpruefenPasswort(el, ev);
			}
		},
		'#popupPasswortButtonEnter click': function(el, ev) {
			this.ueberpruefenPasswort(el, ev);
		},
		ueberpruefenPasswort: function(el, ev) {
			//Überprüfung Passwort
			var pass = this.popupPasswortEingabeEl.val();

			if ( this.options.amoDatenAktuell.laenderVersion.passwort === pass )
			{
				//this.popupPasswortEl.popup( "close" );
				this.derBodyEl.toggleClass("passwortpopup", false );
				//$.mobile.changePage('#welcomeseite');
				this.options.seitenCompute("#welcomeseite");
				this.popupPasswortTextContainerEl.toggleClass('fehler', false);

				//Passwort-Kontrolle bleibt auf true
				//Es wird also immer das Passwort abgefragt
				//this.options.amoStatus.attr("passwortKontrolle", false);
			}
			else
			{
				this.popupPasswortTextContainerEl.toggleClass('fehler',true);
				this.popupPasswortEingabeEl.val('');
				this.popupPasswortEingabeEl.focus();
			}
		},
		'#slider-1 slidestart': function(el, ev) {
			this.derBodyEl.toggleClass('footerAus', true);
		},
		changeProgresseyeworks: function( newVal ) {
			var sliderWert = newVal;

			if ( sliderWert < 1 )
			{
				sliderWert = 1;
			}

			if( sliderWert !== this.sliderWertSpeicherVision )
			{
				//console.log('sliderWert: ' + sliderWert);

				if ( this.imgSlider )
				{
					//das neue Bild einblenden
					this.simVisionImgEl1 = $('#simVision' + sliderWert).css('visibility','visible');

					//das alte Bild ausblenden
					this.simVisionImgEl2.css('visibility','hidden');

					//das Neue Bild für das spätere Ausblenden merken
					this.simVisionImgEl2 = this.simVisionImgEl1;
				}
				else
				{
					this.simOptionsEl.attr('src', this.slider1Str + sliderWert + '.jpg');
				}

				this.sliderWertSpeicherVision = sliderWert;
			}

		},
		updateSim1Img: function( sliderWert ) {
//Achtung !!!!!!!!!!!!!!!!!
			if ( this.imgSlider )
			{
				this.updateSim1ImagesContainer();
				this.options.progress(0);
			}
			else
			{

				this.simOptionsEl.attr('src', this.slider1Str + sliderWert + '.jpg');
				this.options.progress(0);

				//console.log('sliderWert: ' + sliderWert);
			}
		},
		updateSim1ImagesContainer: function() {
			this.simVisionContainerEl.html( this.options.simImages[ this.simVisionImgValue] );

		},
		'#slider-2 slidestart': function(el, ev) {
			this.derBodyEl.toggleClass('footerAus', true);
		},
		changeProgressioloptions: function( neuWert ) {
			var sliderWert = neuWert;

			if ( sliderWert < 1 )
			{
				sliderWert = 1;
			}

			if( sliderWert !== this.sliderWertSpeicherLenses )
			{
				if ( this.imgSlider )
				{
					//das neue Bild einblenden
					this.simLensesImgEl1 = $('#simLens' + sliderWert).css('visibility','visible');

					//das alte Bild ausblenden
					this.simLensesImgEl2.css('visibility','hidden');

					//das Neue Bild für das spätere Ausblenden merken
					this.simLensesImgEl2 = this.simLensesImgEl1;
				}
				else
				{
					this.simIolEl.attr('src', this.slider2Str + sliderWert + '.jpg');
				}

				this.sliderWertSpeicherLenses = sliderWert;
			}
		},
		'updateSim2Img': function( sliderWert ) {

			if ( this.imgSlider )
			{
				this.updateSim2ImagesContainer();
				this.options.progressioloptions(0);
			}
			else
			{
				this.simIolEl.attr('src', this.slider2Str + sliderWert + '.jpg');
				this.options.progressioloptions(0);

				//console.log('sliderWert: ' + sliderWert);
			}
		},
		updateSim2ImagesContainer: function() {
			this.simLensesContainerEl.html( this.options.simImages[ this.simVisionImgValue2 ] );

		},
		'#slider-3 slidestart': function(el, ev) {
			steal.dev.log('slidestart');
			this.derBodyEl.toggleClass('footerAus', true);
		},
		changeProgressvision: function( progressV ) { //Hauptslider - Simulator
			if ( progressV < 1 )
			{
				progressV = 1;
			}

			this.simSliderWert = progressV;


			//simSliderStufe für die Caption der ersten drei setzen
			if ( this.simSliderWert < 28 ) //Stufe 1
			{
				this.simSliderStufe = 1;
			}
			else if ( this.simSliderWert < 63 ) //Stufe 2
			{
				this.simSliderStufe = 2;
			}
			else if ( this.simSliderWert < 100 ) //Stufe 3
			{
				this.simSliderStufe = 3;
			}

			if ( this.simSliderWert < 20 ) //Stufe 1
			{
				this.simVis2grEls.css('opacity', 0);
				//this.simSliderStufe = 1;
				//this.simCaptionEl.toggleClass('pfeil', false);
			}
			else if ( this.simSliderWert < 55 ) //Stufe 2 Presbyopia: 20-55
			{
				//von 50 an 100%
				this.simVis2El.css('opacity', ( this.simSliderWert - 19 ) / ( 49 - 19 ) );
				this.simVis3grEls.css('opacity', 0);
				//this.simSliderStufe = 2;
				//this.simCaptionEl.toggleClass('pfeil', false);
			}
			else if ( this.simSliderWert < 98 ) //Stufe 3 Cataract: 55-97
			{
				//von 85 an 100%
				this.simVis3El.css('opacity', ( this.simSliderWert - 54 ) / ( 85 - 54 ) );
				this.simVis2klEls.css('opacity', 1);
				this.simVis4grEls.css('opacity', 0);
				//this.simSliderStufe = 3;
				//this.simCaptionEl.toggleClass('pfeil', false);
			}
			else if ( this.simSliderWert < 100 ) //Stufe 4 Übergang
			{
				if ( this.simSliderWert == 98 )
				{
					this.simVis4El.css('opacity',  0.4 );
				}
				else
				{
					this.simVis4El.css('opacity',  0.7 );
				}
				this.simVis3klEls.css('opacity', 1 );
				this.simVis5grEls.css('opacity', 0 );
				//this.simSliderStufe = 3;
				//this.simCaptionEl.toggleClass('pfeil', false);
			}
			else if ( this.simSliderWert < 136 ) //Stufe 4
			{
				//this.simVis4El.css('opacity',  1 );
				this.simVis4klEls.css('opacity', 1);
				this.simVis5grEls.css('opacity', 0);
				this.simSliderStufe = 4;
				//this.simCaptionEl.toggleClass('pfeil', false);
			}
			else if ( this.simSliderWert < 173 ) //Stufe 5
			{
				this.simVis5klEls.css('opacity', 1);
				this.simVis6grEls.css('opacity', 0);
				this.simSliderStufe = 5;
				//this.simCaptionEl.toggleClass('pfeil', false);
			}
			else if ( this.simSliderWert < 175 ) //Stufe 6 Übergang
			{
				if ( this.simSliderWert == 173 )
				{
					this.simVis6El.css('opacity',  0.4 );
				}
				else
				{
					this.simVis6El.css('opacity',  0.7 );
				}

				this.simVis5klEls.css('opacity', 1);
				this.simVis7grEls.css('opacity', 0);
				this.simSliderStufe = 5;
				//this.simCaptionEl.toggleClass('pfeil', false);
			}
			else if ( this.simSliderWert < 211 ) //Stufe 6
			{
				this.simVis6klEls.css('opacity', 1);
				this.simVis7grEls.css('opacity', 0);
				this.simSliderStufe = 6;
				//this.simCaptionEl.toggleClass('pfeil', false);
			}
			else //Stufe 7
			{
				/*if ( this.simAstigmatism ) //Astigmatism
				{
					//Astigmatism
				}else{}*/

				if ( this.simBilderProduktSerienNr == 1 || this.simBilderProduktSerienNr == 3 )
				{
					//von 232 an 100%
					this.simVis8El.css('opacity', ( this.simSliderWert - 210 ) / ( 232 - 210 ) );
					this.simVis7El.css('opacity', 1);
					//this.simCaptionEl.toggleClass('pfeil', true);
				}
				else
				{
					this.simVis7klEls.css('opacity', 1);
					this.simVis8El.css('opacity', 0 );
					//this.simCaptionEl.toggleClass('pfeil', false);
				}

				this.simSliderStufe = 7;
			}

			//Pfeil ein- oder ausblenden
			this.einblendenPfeil();

			//Texte auf der Slider-Legende aktualisieren
			this.aendernSimCaptionStufe();
		},//Astigmatismus-Slider
		changeProgressastigmatism: function( astigWert ) {
			var sliderWert = astigWert;

			this.astigmatismSliderWert = sliderWert;
			this.simBilderSerienNr = sliderWert;

			this.austauschenBilderSimulator(); //XXXXXXXXXXXXXXXXX
		},
		'.isi click': function(el, ev) {
			//$.mobile.silentScroll(720);
			$(window).scrollTop(720);
			this.derBodyEl.toggleClass('footerAus', false);
		},
		'.scrollDown click': function(el, ev) {
			steal.dev.log("scrollDown click");
			this.derBodyEl.toggleClass('footerAus', false);
			//$.mobile.silentScroll(720);
			$(window).scrollTop(720);
		},
		'.scrollUp click': function(el, ev) {
			//$.mobile.silentScroll();
			$(window).scrollTop(0);
			this.derBodyEl.toggleClass('footerAus', true);
		},
		' vmousedown': function(el, ev) {
			if ( ev.pageY > 100 )
			{
				steal.dev.log('vmoudown triggered X: ' + ev.screenX + ' Y: ' + ev.screenY );
				this.swipeStart = ev.screenY;
			}

		},
		' vmousemove': function(el, ev) {
			if ( this.swipeStart - ev.screenY > 30 )
			{
				this.derBodyEl.toggleClass('footerAus', false);
				steal.dev.log('vmousemove triggered X: ' + ev.screenY + ' Y: ' + ev.screenY );
			}
		},
		' vmouseup': function(el, ev) {
			this.swipeStart = -1;
		},
		'{eyeExamplesContainerEl} expand': function(el, ev) {
			if( this.fertigEyeWorksSeite ) //nur Ausführen, wenn Layout fertig
			{

				var collapsBox = $(ev.target).data('collapsbox');
				steal.dev.log('collaps' + collapsBox);

				switch ( collapsBox )
				{
					case 'normalvision':
						this.slider1Str = this.serverPfad + 'amo/simulations/sim_norm_jpg/sim_norm_';
						//this.updateSim1Img( this.slider1InputEl.val() );
						this.simVisionImgValue = 'normalvision';
						//this.updateSim1Img( this.slider1InputEl.val() );
						//steal.dev.log('sliderwert ausgelesen' + this.slider1InputEl.val() + ' ' + this.slider1InputEl.attr('value') );
						//this.slider1InputEl.css('display','block');
					break;
					case 'hyperopia':
						this.slider1Str = this.serverPfad + 'amo/simulations/sim_fars_jpg/sim_fars_';
						this.simVisionImgValue = 'hyperopia';
						this.updateSim1Img( this.slider1InputEl.val() );
					break;
					case 'myopia':
						this.slider1Str = this.serverPfad + 'amo/simulations/sim_near_jpg/sim_near_';
						this.simVisionImgValue = 'myopia';
						this.updateSim1Img( this.slider1InputEl.val() );
					break;
					case 'astigmatism':
						this.slider1Str = this.serverPfad + 'amo/simulations/sim_astg_jpg/sim_astg_';
						this.simVisionImgValue = 'astigmatism';
						this.updateSim1Img( this.slider1InputEl.val() );
					break;
					case 'presbyopia':
						this.slider1Str = this.serverPfad + 'amo/simulations/sim_pres_jpg/sim_pres_';
						this.simVisionImgValue = 'presbyopia';
						this.updateSim1Img( this.slider1InputEl.val() );
					break;
					case 'cataract':
						this.slider1Str = this.serverPfad + 'amo/simulations/sim_cata_jpg/sim_cata_';
						this.simVisionImgValue = 'cataract';
						this.updateSim1Img( this.slider1InputEl.val() );
					break;
					case 'cataractsurgery':
						this.slider1Str = this.serverPfad + 'amo/simulations/sim_surg_jpg/sim_surg_';
						this.simVisionImgValue = 'cataractsurgery';
						this.updateSim1Img( this.slider1InputEl.val() );
					break;
				}

				//Das passende Bild zur Slider-Position laden
				this.updateSim1Img( this.slider1InputEl.val() );
			}
		},
		'{eyeWorksCompute} change': function( compute, ev, newVal, oldVal ) {
			steal.dev.log("eyeWorksCompute" + ev + newVal + oldVal);

			var collapsBox = newVal;

				switch ( collapsBox )
				{
					case 'normalvision':
						this.slider1Str = this.serverPfad + 'amo/simulations/sim_norm_jpg/sim_norm_';
						//this.updateSim1Img( this.slider1InputEl.val() );
						this.simVisionImgValue = 'normalvision';
						this.updateSim1Img( 0 );
						//steal.dev.log('sliderwert ausgelesen' + this.slider1InputEl.val() + ' ' + this.slider1InputEl.attr('value') );
						//this.slider1InputEl.css('display','block');

					break;
					case 'hyperopia':
						this.slider1Str = this.serverPfad + 'amo/simulations/sim_fars_jpg/sim_fars_';
						this.simVisionImgValue = 'hyperopia';
						this.updateSim1Img( 0 );
					break;
					case 'myopia':
						this.slider1Str = this.serverPfad + 'amo/simulations/sim_near_jpg/sim_near_';
						this.simVisionImgValue = 'myopia';
						this.updateSim1Img( 0 );
					break;
					case 'astigmatism':
						this.slider1Str = this.serverPfad + 'amo/simulations/sim_astg_jpg/sim_astg_';
						this.simVisionImgValue = 'astigmatism';
						this.updateSim1Img( 0 );
					break;
					case 'presbyopia':
						this.slider1Str = this.serverPfad + 'amo/simulations/sim_pres_jpg/sim_pres_';
						this.simVisionImgValue = 'presbyopia';
						this.updateSim1Img( 0 );
					break;
					case 'cataract':
						this.slider1Str = this.serverPfad + 'amo/simulations/sim_cata_jpg/sim_cata_';
						this.simVisionImgValue = 'cataract';
						this.updateSim1Img( 0 );
					break;
					case 'cataractsurgery':
						this.slider1Str = this.serverPfad + 'amo/simulations/sim_surg_jpg/sim_surg_';
						this.simVisionImgValue = 'cataractsurgery';
						this.updateSim1Img( 0 );
					break;
				}

				//Das passende Bild zur Slider-Position laden
				//!!!!!!!!!???? this.updateSim1Img( this.slider1InputEl.val() );
		},
		'{iolLightAccordionCompute} change': function( compute, ev, newVal, oldVal ) {
			steal.dev.log("iolLightAccordionCompute" + ev + newVal + oldVal);

			var collapsBox = newVal;

			switch ( collapsBox )
				{
					case 'monolens':
						this.slider2Str = this.serverPfad + 'amo/simulations/sim_mono_jpg/sim_mono_';
						this.simVisionImgValue2 = 'monolens';
						this.updateSim2Img( 1 );

					break;
					case 'multilens':
						this.slider2Str = this.serverPfad + 'amo/simulations/sim_mult_jpg/sim_mult_';
						this.simVisionImgValue2 = 'multilens';
						this.updateSim2Img( 1 );
					break;
					case 'symfony':
						this.slider2Str = this.serverPfad + 'amo/simulations/sim_symf_jpg/sim_symf_';
						this.simVisionImgValue2 = 'symfony';
						this.updateSim2Img( 1 );
					break;
					case 'synergy':
						this.slider2Str = this.serverPfad + 'amo/simulations/sim_syne_jpg/sim_syne_';
						this.simVisionImgValue2 = 'synergy';
						this.updateSim2Img( 1 );
					break;
					case 'sonata':
						this.slider2Str = this.serverPfad + 'amo/simulations/sim_sona_jpg/sim_sona_';
						this.simVisionImgValue2 = 'sonata';
						this.updateSim2Img( 1 );
					break;
					case 'eyehance':
						this.slider2Str = this.serverPfad + 'amo/simulations/sim_eyhc_jpg/sim_eyhc_';
						this.simVisionImgValue2 = 'eyehance';
						this.updateSim2Img( 1 );
					break;
				}

		},
		'{amoAccordion2El} expand': function( el, ev ) {
			steal.dev.log('Accordion2 expanded');

			if( this.fertigIoloptionsSeite ) //nur Ausführen, wenn Layout fertig
			{

				var collapsBox = $(ev.target).data('collapsbox');
				steal.dev.log('collaps' + collapsBox);

				switch ( collapsBox )
				{
					case 'monolens':
						this.slider2Str = this.serverPfad + 'amo/simulations/sim_mono_jpg/sim_mono_';
						this.simVisionImgValue2 = 'monolens';
						this.updateSim2Img( this.slider2InputEl.val() );
					break;
					case 'multilens':
						this.slider2Str = this.serverPfad + 'amo/simulations/sim_mult_jpg/sim_mult_';
						this.simVisionImgValue2 = 'multilens';
						this.updateSim2Img( this.slider2InputEl.val() );
					break;
					case 'symfony':
						this.slider2Str = this.serverPfad + 'amo/simulations/sim_symf_jpg/sim_symf_';
						this.simVisionImgValue2 = 'symfony';
						this.updateSim2Img( this.slider2InputEl.val() );
					break;
					case 'synergy':
						this.slider2Str = this.serverPfad + 'amo/simulations/sim_syne_jpg/sim_syne_';
						this.simVisionImgValue2 = 'synergy';
						this.updateSim2Img( this.slider2InputEl.val() );
					break;
					case 'sonata':
						this.slider2Str = this.serverPfad + 'amo/simulations/sim_sona_jpg/sim_sona_';
						this.simVisionImgValue2 = 'sonata';
						this.updateSim2Img( this.slider2InputEl.val() );
					break;
					case 'eyehance':
						this.slider2Str = this.serverPfad + 'amo/simulations/sim_eyhc_jpg/sim_eyhc_';
						this.simVisionImgValue2 = 'eyehance';
						this.updateSim2Img( this.slider2InputEl.val() );
					break;
				}
			}
		},
		einblendenPfeil: function() {
			if ( this.simSliderWert > 210 && (this.simBilderProduktSerienNr == 1 || this.simBilderProduktSerienNr == 3) )
			{
				this.simCaptionEl.toggleClass('pfeil', true);
			}
			else
			{
				this.simCaptionEl.toggleClass('pfeil', false);
			}
		},
		aendernSimCaptionStufe: function() {
			this.simCaptionStufeEl.removeClass();

			switch ( this.simSliderStufe )
			{
				case 1:
					if ( this.simAstigmatism )
					{
						//Astigmatismus eingeschaltet
						this.simCaptionStufeEl.addClass('simCapSliderStufe1astig');
					}
					else
					{
						//ohne Astigmatismus
						this.simCaptionStufeEl.addClass('simCapSliderStufe1');
					}
				break;
				case 2:
					this.simCaptionStufeEl.addClass('simCapSliderStufe2');
				break;
				case 3:
					this.simCaptionStufeEl.addClass('simCapSliderStufe3');
				break;
				case 4:
					this.simCaptionStufeEl.addClass('simCapSliderStufe4');
				break;
				case 5:
					this.simCaptionStufeEl.addClass('simCapSliderStufe5');
				break;
				case 6:
					this.simCaptionStufeEl.addClass('simCapSliderStufe6');
				break;
				case 7:
					this.simCaptionStufeEl.addClass('simCapSliderStufe7');
				break;
			}
		},
		auswaehlenSimLegende: function() {
			this.simSliderBlockEl.removeClass('prod1 prod2 prod3 prod4 prod5 prod6 prod7 prod8 prod9 prod10 prod11 prod12 prod13 prod14');
			this.simCaptionEl.removeClass();

			switch ( this.simProduktNr )
			{
				case 1:
					this.simSliderBlockEl.addClass('prod1');
					this.simCaptionEl.addClass('prod1');
				break;
				case 2:
					this.simSliderBlockEl.addClass('prod2');
					this.simCaptionEl.addClass('prod2');
				break;
				case 3:
					this.simSliderBlockEl.addClass('prod3');
					this.simCaptionEl.addClass('prod3');
				break;
				case 4:
					this.simSliderBlockEl.addClass('prod4');
					this.simCaptionEl.addClass('prod4');
				break;
				case 5:
					this.simSliderBlockEl.addClass('prod5');
					this.simCaptionEl.addClass('prod5');
				break;
				case 6:
					this.simSliderBlockEl.addClass('prod6');
					this.simCaptionEl.addClass('prod6');
				break;
				case 7:
					this.simSliderBlockEl.addClass('prod7');
					this.simCaptionEl.addClass('prod7');
				break;
				case 8:
					this.simSliderBlockEl.addClass('prod8');
					this.simCaptionEl.addClass('prod8');
				break;
				case 9:
					this.simSliderBlockEl.addClass('prod9');
					this.simCaptionEl.addClass('prod9');
				break;
				case 10:
					this.simSliderBlockEl.addClass('prod10');
					this.simCaptionEl.addClass('prod10');
				break;
				case 11:
					this.simSliderBlockEl.addClass('prod11');
					this.simCaptionEl.addClass('prod11');
				break;
				case 12:
					this.simSliderBlockEl.addClass('prod12');
					this.simCaptionEl.addClass('prod12');
				break;
				case 13:
					this.simSliderBlockEl.addClass('prod13');
					this.simCaptionEl.addClass('prod13');
				break;
				case 14:
					this.simSliderBlockEl.addClass('prod14');
					this.simCaptionEl.addClass('prod14');
				break;
			}

		},//<sup class=\“eur\“>,2</sup>
		auswaehlenAstigSimLegende: function() {

			if ( this.simAstigmatism )
			{
				this.simSliderBlockEl.addClass('mitAstig');
			}
			else
			{
				this.simSliderBlockEl.removeClass('mitAstig');
			}

		},
		'#iolOptionsButtonContainer click': function(el, ev) {
			var gewaehlterButtonEl;

			//nur reagieren, wenn ein Button getroffen wurde
			gewaehlterButtonEl = $(ev.target).closest('.iolOptionsButton');
			if ( gewaehlterButtonEl.length )
			{
				//Button-Zustand anzeigen
				el.find('.iolOptionsButton').toggleClass('down', false);
				gewaehlterButtonEl.toggleClass('down', true);

				//gewaehltes Produkt speichern
				this.simProduktNr = Number( gewaehlterButtonEl.data('iolbutton') );

				//gegebenenfalls Produktnummer in BilderSerienNr umwandeln
				if ( this.simProduktNr < 5 )
				{
					//für das Array auf Null ausrichten
					this.simBilderProduktSerienNr = this.simProduktNr - 1;
				}
				else
				{
					//die Sonderfälle auf die vier Bilder korregieren
					this.simBilderProduktSerienNr = this.simProduktNr - 3;
				}

				steal.dev.log('gewählte BilderProduktSerienNummer: ' + this.simBilderProduktSerienNr + ' simProduktNr ' + this.simProduktNr );

				this.austauschenBilderSimulator();

				this.auswaehlenSimLegende();

				//Pfeil ein- oder ausblenden
				this.einblendenPfeil();

			}
		},
		'#astigmCheckBox click': function(el, ev) {
			if ( this.simAstigmatism )
			{ //Ausschalten Astigmatismus
				this.simAstigmatism = false;
				el.toggleClass('on', false);
				//this.slider4InputEl.slider('disable');

				//BilderSerie auf Ausgangswert 0 zurücksetzen
				this.simBilderSerienNr = 0;
				this.austauschenBilderSimulator();
			}
			else
			{ //Einschalten Astigmatismus
				this.simAstigmatism = true;
				el.toggleClass('on', true);
				//this.slider4InputEl.slider('enable');

				//BilderSerienNummer vom Slider nehmen
				this.simBilderSerienNr = this.astigmatismSliderWert;
				this.austauschenBilderSimulator();
			}

			this.auswaehlenAstigSimLegende();

			//Texte auf der Slider-Legende aktualisieren
			this.aendernSimCaptionStufe();
		},
		'#eyeworksseite pageinit': function(el, ev) {
			steal.dev.log('Page init');

			this.fertigEyeWorksSeite = true;
			this.slider1InputEl = $('#slider-1');
		},
		'#ioloptionsseite pageinit': function(el, ev) {
			steal.dev.log('Page init');

			this.fertigIoloptionsSeite = true;
			this.slider2InputEl = $('#slider-2');
		},
		'#visionsimulatorseite pageinit': function(el, ev) {
			steal.dev.log('Page init');

			this.fertigVisionSimulatorSeite = true;
			this.slider4InputEl = $('#slider-4');

			if ( ie8 !== true )
			{
				this.slider4InputEl.slider('disable');
			}
			else
			{
				setTimeout(function(){
					this.slider4InputEl.slider('disable');
				}, 2000);
			}
		},
		' pagebeforechange': function( el, ev, info ) {
			steal.dev.log('pagebeforechange ' + info.fromPage + " " + info.toPage);
			this.derBodyEl.toggleClass('footerAus', true);
		},
		'{progress} change': function( frage, ev, value ) {
			this.changeProgresseyeworks(value);
		},
		'{progressioloptions} change': function( frage, ev, value ) {
			this.changeProgressioloptions(value);
		},
		'{progressvision} change': function( frage, ev, value ) {
			this.changeProgressvision(value);
		},
		'{progressastigmatism} change': function( frage, ev, value ) {
			this.changeProgressastigmatism(value);
		},
		'{seitenCompute} change': function( frage, ev, value ) {
			var self = this;

			if ( value === '#welcomeseite' )
			{
				console.log('Page change Welcome-Seite');
				setTimeout( function(){
					//info.toPage.addClass("ui-page-active");
					self.options.progress(0);

					self.options.progressioloptions(0);

					self.refreshVisionSimulatorSeite();

					//Accordions zurücksetzen
					$('#accordionSeite1:not(.amoAccorionOffen) > .ui-collapsible-heading .ui-collapsible-heading-toggle').trigger('click');
					$('#accordionIol1:not(.amoAccorionOffen) > .ui-collapsible-heading .ui-collapsible-heading-toggle').trigger('click');
					$('#accordionEyeWorks1:not(.amoAccorionOffen) > .ui-collapsible-heading .ui-collapsible-heading-toggle').trigger('click');
				}, 500 );
			}

			if ( value === 'visionsimulatorseite' )
			{
				$('#szenenAuswahl.ausgefahren').toggleClass('ausgefahren', false);
			}

			/*setTimeout( function(){
				info.toPage.addClass("ui-page-active");
			}, 2000 );*/

		},
		refreshVisionSimulatorSeite: function() {
			if ( this.simAstigmatism )
			{
				this.options.progressastigmatism(0);
				$('#astigmCheckBox').trigger('click');
			}

			$('#iolOptionsButtonContainer > .iolOptionsRow:first-child > .iolOptionsButton:first-child').trigger('click');

			this.options.progressvision(0);

		},
		/*' swipeup': function( el, ev, info ) {
			steal.dev.log('swipeup');
			//var self = this;
			this.derBodyEl.toggleClass( 'footerAus', false );
			//var warten = setTimeout( function(){
			//}, 2000 );

		},*/
		'{amoDatenAktuell} sprachVariante': function( obj, ev, attr, how, newVal, oldVal ) {
			this.anpassenReferenzVersion();
			this.anzeigenLogo();

			setTimeout(function() {
				$('#eyeExamplesContainer .amoAccordion').trigger('create');

				$('#amoAccordion2').trigger('create');

			}, 200);
			steal.dev.log('amoDatenAktuell-Sprachvariante aktualisiert');
		},
		anzeigenLogo: function() {
			//wenn Japan gewählt Logo ausblenden
			if ( this.options.amoDatenAktuell.laenderVersion.logo === true )
			{
				this.derBodyEl.toggleClass('logoaus', false);
			}
			else
			{
				this.derBodyEl.toggleClass('logoaus', true);
			}
		},
		anpassenReferenzVersion: function() {
			//Referenznummer für US/ANZ oder EUR anpassen
			if ( this.options.amoDatenAktuell.laenderVersion.referenzVersion === 'eur' )
			{
				this.VisionSimulatorSeiteEl.toggleClass('eur', true);
			}
			else
			{
				this.VisionSimulatorSeiteEl.toggleClass('eur', false);
			}

		},
		'{document} mobileinit': function( el, ev ) {

			//auf alle Seiten jQuerymobile-Layout anwenden
			//this.uiPageEls.not('#startseite').trigger('create');
			this.fertigLayout = true;

			//$('.amoAccordion').trigger('create');
			steal.dev.log('mobileinit in Steuerung');
		},
		'{document} uiRefresh': function( el, ev ) {
			//Buttons und andere Widgets aktualisieren
			//this.acceptButtonEl.button('refresh');

			steal.dev.log('uiRefresh');
		},
		' szenenwechsel': function( el, ev, szenenWahl ) {
			this.gewaehlteSzene.dieSzene = szenenWahl;

			if( szenenWahl === 2 )
			{
				this.gewaehlteSzene.attr('szenenOrdner', 'sim2');
			}
			else if ( szenenWahl === 1 )
			{
				this.gewaehlteSzene.attr('szenenOrdner', 'sim1');
			}
			else
			{
				this.gewaehlteSzene.attr('szenenOrdner', 'sim');
			}
			//console.log('szenenWechsel' + szenenWahl);

			this.austauschenBilderSimulator();

			//this.refreshVisionSimulatorSeite();
			//this.zuweisenSliderSzenen();
		}
	}); //Ende Controller

export default AmoSteuerung;
