import $ from "jquery";
import can from "can";

console.log( "seitensteuerung" );
export const AmoSeitensteuerung = can.Construct.extend("Amo.SeitenSteuerung",{
	    seitenContainerEl: "",
	    alteSeiteEl: "",
	    neueSeiteEl: ""
	}, {
	    init: function( eingangsWerte ) {
	        var self = this;
console.log("Seteinsteuerung " + 1 );
	        this.seitenContainerEl = $( eingangsWerte.seitenContainerSelector );
console.log("Seteinsteuerung " + 2 );

	        this.seitenCompute = eingangsWerte.seitenCompute;

	        //Klassen dem SeitenContainerhinzufügen


	        //Seitenreferenzen setzen
	        // eingangsWerte.seiten muss ein Array sein
	        $.each( eingangsWerte.seiten, function( indeX, seitenId) {
	        	self[seitenId] = $(seitenId);
	        });

console.log("Seteinsteuerung " + 3 );
	        //erste Seite aktiv setzen
	        this[eingangsWerte.seiten[0]].addClass("ui-page-active");
	        this.neueSeiteEl = $( eingangsWerte.seiten[0] );

console.log("Seteinsteuerung " + 4 );
	        this.seitenCompute.bind('change', function(ev, newVal, oldVal) {
			    steal.dev.log("Seitencompute: " + newVal );

			    self.wechselnSeite( newVal );
			});


	    },
	    wechselnSeite: function( neueSeite ) {
	        var self = this;

	        this.alteSeiteEl = this.neueSeiteEl;

	        //die neueSeite muss als ID-Selektor übergeben werden
	        this.neueSeiteEl = this[neueSeite];
	        this.neueSeiteEl.css("opacity",0).addClass("ui-page-active");

	        this.alteSeiteEl.animate({
			  opacity: 0
			}, {
			  duration: 500,
			  queue: false,
			  always: function(){
			  	self.alteSeiteEl.removeClass("ui-page-active");
			  }
			});

			this.neueSeiteEl.animate({
				opacity: 1
			}, {
				duration: 500,
				queue: false
			});
	    }
	});


export default AmoSeitensteuerung;