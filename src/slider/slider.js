import $ from "jquery";
import can from "can";
import "src/resources/jquerypp_custom";
//import "jquerypp";
	//, 'jquerypp/dom'
	//, 'jquerypp/event/drag'
	//, './slider.mustache'

export default can.Component.extend({
        tag: "slider",
        viewModel: {
            sliderwert: 0, //ist ein can.compute von 0 bis 1 bezogen auf die Sliderstecke
            sliderWert: 0, //lokal berechneter Wert
            slidereingabe: 0, //ist ein can.compute von 0 bis 1 - soll den Slider setzen (Setter)
            griffEl: '', //Slidergriff
            scaleboxEl: '', //Begrenzung Slider für die Skalierung
            dasElement: '', //Referenz auf das Hauptelement
            containerOffset: 0,
            sliderWidth: 20,
            containerWidth: 500,
            sliderstrecke: 1350,
            sliderRandOffset: 1000,
            schritte: 50,
            skalierfaktor: 1, //erwartet ein can.compute von 0 bis 1 - wichtig für die Berechnung der Sliderposition bei Skalierung,

            anlegenGrundeinstellung: function( element ){
                var self = this;

                can.each( this.parameter,
                    function( wert, param ) {
                        self.attr( param, wert );
                    }
                );
    steal.dev.log("init " + this.schritte );
                this.griffEl = element.find(".slidergriff").eq(0);
                this.scaleboxEl = element.find(".scalebox");
                //this.containerOffset = element.offset().left + parseInt( element.css("paddingLeft") ) + parseInt( element.css("borderLeftWidth") );
steal.dev.log("SliderStrecke " + this.sliderstrecke);
                this.berechnenAbmessungen();
            },
            berechnenAbmessungen: function(){
                var marginLeft;
                this.containerOffset = this.dasElement.offset().left;

                //this.containerOffset = this.dasElement.offset().left + parseInt( this.dasElement.css("paddingLeft") ) + parseInt( this.dasElement.css("borderLeftWidth") );
                
                this.sliderWidth = this.griffEl.outerWidth();

                //this.containerWidth = this.dasElement.width() * this.skalierfaktor;
                this.containerWidth = this.sliderstrecke;
                //this.sliderRandOffset = this.containerOffset + this.containerWidth - this.sliderWidth;

                //this.sliderWidth = this.griffEl.outerWidth();
                //this.containerWidth = this.dasElement.width();
                //this.sliderRandOffset = this.containerOffset + (this.containerWidth - this.sliderWidth) * this.skalierfaktor;

                //this.scaleboxEl.css({"width": this.skalierfaktor * this.sliderStrecke });

                marginLeft = this.sliderWidth / -2 + "px";
                this.scaleboxEl.css({
                    "width":  this.sliderstrecke + this.sliderWidth,
                    "marginLeft": marginLeft
                });
                
                //steal.dev.log("X >< X " + this.containerOffset + ' ' + this.sliderRandOffset + ' ' + this.containerWidth );
            },
            updatePosition: function( context, el, ev ){
                
                //steal.dev.log("updatePosition");
                //this.sliderEl.css("outline","2px dotted green");

                var value = this.attr("sliderwert"),
                    container = this.sliderEl,
                    containerOffset = container.offset().left,
                    sliderWidth = this.griffEl.outerWidth(),
                    containerWidth = container.width();
                
                containerOffset +=
                    parseInt( container.css("paddingLeft") ) +
                    parseInt( container.css("borderLeftWidth") );
                
                var sliderOffset = containerOffset +
                    (   (containerWidth - sliderWidth) * 
                     (value) );
                
                this.griffEl.offset({
                        left: sliderOffset
                    });/**/
            },
            updatePositionProzent: function( value ) {
                steal.dev.log("updateversion " + value );
                    var prozentWert, offsetWert;

                //prozentWert = value * 100;

                offsetWert = this.containerOffset + this.containerWidth * value / this.skalierfaktor - this.sliderWidth / 2 / this.skalierfaktor;

                if ( offsetWert < this.containerOffset )
                {
                    offsetWert = this.containerOffset;
                }
                else if ( offsetWert > this.sliderRandOffset )
                {
                    offsetWert = this.sliderRandOffset;
                }

//steal.dev.log("containerOffset " + this.containerOffset + " plus " + (this.containerWidth * value / this.skalierfaktor) + " sliderW " + (this.sliderWidth / 2 / this.skalierfaktor) + " sliderRandOffset" + this.sliderRandOffset + " offsetWert " + offsetWert);

//steal.dev.log("offsetWert " + offsetWert + " value " + value + " containerOffset " + this.containerOffset + " containerWidth " + this.containerWidth + " sliderwert " + this.sliderwert + " slidderRandOffset " + this.sliderRandOffset);

                this.griffEl.offset({
                        left: offsetWert
                    });

                //this.griffEl.css("left", prozentWert + "%");
            },
            updatePositionPageX: function( pageXWert ) {
                var leftWert;

                //leftWert = ( pageXWert - this.containerOffset - this.sliderWidth / 2 ) / this.skalierfaktor;
                leftWert = ( pageXWert - this.containerOffset - this.sliderWidth / 2 ) / this.skalierfaktor;

                this.griffEl.css("left", leftWert );
            },
            setzenAufNull: function() {
                this.griffEl.css("left", 0 );
            },
            setzenSlider: function( wert ) {
                steal.dev.log(wert);
                wert = this.sliderstrecke / parseInt(this.schritte) * wert;
                steal.dev.log(wert);
                this.griffEl.css("left", wert + "px" );

            }
        },
        //template: "<div class='scalebox'><div class='slidergriff'></div></div>",
        template: can.stache("<content/>"),
        events: {
            "inserted": function( el, ev ) {
                this.viewModel.dasElement = el;

                this.viewModel.anlegenGrundeinstellung( el );

            },
            ".slidergriff draginit": function( el, ev, drag){
                //steal.dev.log("draginit");
                //this.element.css("background-color", "red");
                drag.limit( this.viewModel.scaleboxEl );
                drag.horizontal();
                el.addClass("dragging");
            },
            ".slidergriff dragend": function( el, ev, drag){
                el.removeClass("dragging");
            },
            ".slidergriff dragmove": function(el, ev, drag){
                
                var sliderLeft;

                sliderLeft = Math.round ( el.position().left / this.viewModel.sliderstrecke * parseInt(this.viewModel.schritte) + 0.5 );
                
                if ( sliderLeft > this.viewModel.schritte )
                {
                    sliderLeft = this.viewModel.schritte;
                }

                this.viewModel.attr("sliderwert", sliderLeft);

            },
            "{viewModel} slidereingabe": function( attr, ev, value ) {
                steal.dev.log("slidereingabe " + value);
                //this.viewModel.updatePosition();
                //this.viewModel.updatePositionProzent( value );

                if ( value < 0.015 )
                {
                    this.viewModel.setzenAufNull();
                }
                else
                {
                    this.viewModel.setzenSlider(value);
                }
            },
            "{viewModel} skalierfaktor": function( attr, ev, value ) {

                //neu berechnen der relativen Sliderwerte für die Skalierung
                this.viewModel.berechnenAbmessungen();
            },
            "{viewModel} sliderwert": function( attr, ev, value ) {

               //steal.dev.log("viewModel :" + value );
               if ( this.viewModel.sliderLeft !== value )
               {    
                    this.viewModel.setzenSlider( value );
               }
            },
            "click": function( el, ev ) {
                var positionXinBox,sliderLeft;
//steal.dev.log("click erfolgt " + ev.pageX + ' ' + this.viewModel.dasElement.offset().left);

                positionXinBox = ev.pageX - this.viewModel.dasElement.offset().left + parseInt( this.viewModel.dasElement.css("paddingLeft") ) + parseInt( this.viewModel.dasElement.css("borderLeftWidth") );
                if ( positionXinBox < 0 )
                {
                    positionXinBox = 0;
                }

                //this.viewModel.griffEl.css("left", positionXinBox + "px" );

                sliderLeft = Math.round ( positionXinBox / this.viewModel.sliderstrecke * parseInt(this.viewModel.schritte) );

                if ( sliderLeft > this.viewModel.schritte )
                {
                    sliderLeft = this.viewModel.schritte;
                }

                this.viewModel.attr("sliderwert", sliderLeft);
            },
            "clicker": function( el, ev ) {
                var anteiligePosition,
                    positionXinBox;
steal.dev.log("click");
                positionXinBox = ev.pageX - this.viewModel.containerOffset;
                if ( positionXinBox < 0 )
                {
                    positionXinBox = 0;
                }

                anteiligePosition = positionXinBox / this.viewModel.containerWidth;

                if ( Math.round( Math.abs((anteiligePosition - this.viewModel.sliderwert)) * 25 ) > 0 )
                {
                    steal.dev.log("größer");
                    //this.viewModel.updatePositionProzent( anteiligePosition );
                    //this.viewModel.attr("slidereingabe", anteiligePosition );
                    this.viewModel.attr("sliderwert", anteiligePosition );

                    this.viewModel.updatePositionPageX(ev.pageX)

                }
                
                //$("#rechterfuss").html("pageX " + ev.pageX + " containerOffset " + this.viewModel.containerOffset + " containerWidth " + this.viewModel.containerWidth + " % " + anteiligePosition + " sliderwert " + this.viewModel.sliderwert );
            }
        }
    });
