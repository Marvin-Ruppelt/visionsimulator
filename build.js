var stealTools = require("steal-tools");

var buildPromise = stealTools.build({
//	main: "amo",
  config: __dirname + "/package.json!npm"
}, {
  bundleAssets: false,
  bundleSteal: true,
  debug: true,
  minify: false
});
